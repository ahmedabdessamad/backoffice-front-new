import React, { useEffect, useState } from "react";
import {
  Modal,
  Checkbox,
  Form,
  Input,
  Space,
  Select,
  Button,
  message,
} from "antd";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/react-hooks";
import * as PropTypes from "prop-types";
import classes from "./Permission.module.scss";
import { SUBTABS_NAMES, TAB_NAMES } from "../../configs/constant";
import "./style.css";
import { editPermissionMutation as EDIT_PERMISSION_MUTATION } from "./mutations";
import { GET_PERMISSIONS } from "./queries";

const { Option } = Select;
const PARENTS_DISPLAY_NAME = Object.keys(SUBTABS_NAMES);

function EditPermission({ permission, visibleEdit, setVisibleEdit, updaterCallBack }) {
  const [form] = Form.useForm();
  const [tabsOptions, setTabsOptions] = useState(PARENTS_DISPLAY_NAME);
  const [fieldsTabs, setFieldsTabs] = useState({});
  const [subTabsPermission, setSubTabsPermission] = useState([]);
  const [thereIsEdit, setThereIsEdit] = useState(false);
  const [statePermission, setStatePermission] = useState(permission);

  const tabSelectHandler = (fieldName, tabName) => {
    // map field to its tab
    setFieldsTabs((prevState) => ({ ...prevState, [fieldName]: tabName }));
    // add tab to permission state
    const newTabName = Object.keys(TAB_NAMES).find(
      (key) => TAB_NAMES[key] === tabName
    );
    const newTab = { name: newTabName, parent: null };
    setStatePermission((prevState) => ({
      ...prevState,
      tabs: [...prevState.tabs, newTab],
    }));
  };

  // add/remove selected tab from tabs options on fieldsTabs update
  useEffect(() => {
    const newOptions = PARENTS_DISPLAY_NAME.filter(
      (t) => !Object.values(fieldsTabs).includes(t)
    );
    setTabsOptions(newOptions);
  }, [fieldsTabs]);

  const setInitFieldsTabs = () => {
    const { tabs } = permission;
    let initState = {};

    tabs
      .filter((tab) => tab.parent === null)
      .forEach(({ name }, index) => {
        initState = {
          ...initState,
          [index]: TAB_NAMES[name],
        };
      });

    setFieldsTabs(initState);
  };

  const permissionType = (perm) => {
    const permissionTypeTab = [];
    if ((perm & 4) === 4) permissionTypeTab.push("Admin");
    if ((perm & 2) === 2) permissionTypeTab.push("Editor");
    if ((perm & 1) === 1) permissionTypeTab.push("Reader");
    return permissionTypeTab;
  };

  const getChildren = (tabs, parentName) => {
    const children = {};
    tabs
      .filter(({ parent }) => parent === parentName)
      .forEach((child) => {
        const childName = SUBTABS_NAMES[TAB_NAMES[parentName]][child.name];
        children[childName] = permissionType(child.permission);
      });
    return children;
  };

  const getDefaultTabs = () => {
    if (!statePermission || !statePermission.tabs) return [];

    const { tabs } = statePermission;
    return tabs
      .filter((tab) => {
        if (thereIsEdit) {
          return (
            tab.parent === null &&
            Object.values(fieldsTabs).includes(TAB_NAMES[tab.name])
          );
        }
        return tab.parent === null;
      })
      .map(({ name, permission: parentPermission }) => {
        const children = getChildren(tabs, name);
        if (Object.keys(children).length) {
          // has children
          return {
            tabName: TAB_NAMES[name],
            ...children,
          };
        }
        // has no children
        return {
          tabName: TAB_NAMES[name],
          [TAB_NAMES[name]]: permissionType(parentPermission),
        };
      });
  };

  const [editPermission] = useMutation(EDIT_PERMISSION_MUTATION, {
    onCompleted: () => {
      message.success("Permission Successfully Updated!");
      // stop loading
      updaterCallBack(false);
    },
    onError: () => {
      message.error("Oops Something went wrong");
      updaterCallBack(false);
    },
    refetchQueries: [{ query: GET_PERMISSIONS }],
  });
  const exist = (tabsPermission, subTname) => {
    let i = 0;
    for (i = 0; i < tabsPermission.length; i += 1) {
      if (subTname === tabsPermission[i].name) {
        return i;
      }
    }
    return -1;
  };

  // prepare permission to be sent in mutatuion if the tab has subtabs
  const permissionToBinary = async (checkedValues, subtab, tab) => {
    let perm = 0;
    checkedValues.map((val) => {
      if (val === "Reader") perm += 1;
      if (val === "Editor") perm += 2;
      if (val === "Admin") perm += 4;
    });
    // changing the subtab name to its forma in our code exemple: "Missed Deal" to "MISSEDDEAL"
    const subTname = Object.keys(TAB_NAMES).find(
      (key) => TAB_NAMES[key] === subtab
    );

    const tname = Object.keys(TAB_NAMES).find((key) => TAB_NAMES[key] === tab);

    const ob = {
      name: subTname,
      permission: perm,
      parent: tname,
    };

    // setSubTabsPermission((prevState) => [...prevState, ob]);
    const isExist = exist(subTabsPermission, subTname);
    const isExist2 = exist(subTabsPermission, tname);

    if (subTabsPermission.length === 0 || isExist === -1) {
      setSubTabsPermission([...subTabsPermission, ob]);
      if (isExist2 === -1 || subTabsPermission.length === 0) {
        const obParent = {
          name: tname,
          permission: perm,
        };
        setSubTabsPermission([...subTabsPermission, obParent, ob]);
      } else {
        setSubTabsPermission([...subTabsPermission, ob]);
      }
      // if the sub tab is unchekecked after bin checked => delete it from subTabsPermission
    } else if (perm === 0) {
      const newArr1 = [...subTabsPermission];
      newArr1.splice(isExist, 1);
      setSubTabsPermission(newArr1);
    } else {
      const newArr = [...subTabsPermission];
      newArr[isExist] = ob;
      setSubTabsPermission(newArr);
    }
  };
  // prepare permission to be sent in mutatuion if the tab has no subtabs
  const permissionToBinary2 = (checkedValues, tab) => {
    let perm2 = 0;
    checkedValues.map((val) => {
      if (val === "Reader") perm2 += 1;
      if (val === "Editor") perm2 += 2;
      if (val === "Admin") perm2 += 4;
    });
    // changing the subtab name to its forma in our code exemple: "Missed Deal" to "MISSEDDEAL"
    const tname = Object.keys(TAB_NAMES).find((key) => TAB_NAMES[key] === tab);
    const ob = {
      name: tname,
      permission: perm2,
    };
    // setSubTabsPermission((prevState) => [...prevState, ob]);
    const isExist = exist(subTabsPermission, tname);
    if (subTabsPermission.length === 0 || isExist === -1) {
      setSubTabsPermission([...subTabsPermission, ob]);
    } else {
      const newArr = [...subTabsPermission];
      newArr[isExist] = ob;
      setSubTabsPermission(newArr);
    }
  };

  const renderSubTabs = (tab, key) => {
    const options = [
      { label: "Admin", value: "Admin" },
      { label: "Editor", value: "Editor" },
      { label: "Reader", value: "Reader" },
    ];

    if (Object.keys(SUBTABS_NAMES[tab]).length !== 0) {
      return Object.values(SUBTABS_NAMES[tab]).map((subtab) => {
        const onChange = (checkedValue) => {
          setThereIsEdit(true);
          permissionToBinary(checkedValue, subtab, tab);
        };
        return (
          <div style={{ display: "flex" }} key={subtab}>
            <div className="labelCheckboxEdit">
              <Form.Item name={[key, subtab]} label={subtab}>
                <Checkbox.Group
                  onChange={onChange}
                  className="ant-checkbox + span"
                  options={options}
                />
              </Form.Item>
            </div>
          </div>
        );
      });
    }
    const onChange2 = (checkedValue) => {
      setThereIsEdit(true);
      permissionToBinary2(checkedValue, tab);
    };
    return (
      <div className="labelCheckboxEdit" key={tab}>
        <Form.Item name={[key, tab]}>
          <Checkbox.Group
            onChange={onChange2}
            className="ant-checkbox + span"
            options={options}
          />
        </Form.Item>
      </div>
    );
  };

  useEffect(() => {
    if (permission && thereIsEdit === false) {
      form.resetFields();
      // set initial fields
      if (Array.isArray(permission.tabs)) setInitFieldsTabs();
      // set initial subTabs
      setSubTabsPermission(permission.tabs);
    }

    if (subTabsPermission) {
      console.log("subTabsPermission edit =====>", subTabsPermission);
    }
  }, [permission, subTabsPermission, thereIsEdit]);

  useEffect(() => {
    // update state on new permission prop
    setStatePermission(permission);
  }, [permission]);

  const removeFieldHandler = (fieldName) => {
    setThereIsEdit(true);
    const tab = fieldsTabs[fieldName];
    // remove subtabs and tab from subTabsPermission
    const tabName = Object.keys(TAB_NAMES).find(
      (key) => TAB_NAMES[key] === tab
    );
    const newsubTbs = subTabsPermission.filter((p) => {
      return p.name !== tabName && p.parent !== tabName;
    });
    setSubTabsPermission(newsubTbs);
    // remove field from fieldsTabs state
    setFieldsTabs((prevState) => {
      const { [fieldName]: fieldToDelete, ...rest } = prevState;
      return rest;
    });
  };

  return (
    <div>
      <Modal
        title=" Edit Permission"
        visible={visibleEdit}
        // onOk={handleOk}
        onOk={() => {
          form.validateFields().then((values) => {
            // start loading for the mutation
            updaterCallBack(true)
            editPermission({
              variables: {
                id: permission.id,
                input: {
                  ...values,
                  tabs: subTabsPermission,
                },
              },
            });
            form.resetFields();
            // reset state too
            setFieldsTabs({});
            setTabsOptions(Object.keys(SUBTABS_NAMES));
            setVisibleEdit(false);
            setTimeout(() => {
              setThereIsEdit(false);
            });
          });
        }}
        okText="Update"
        onClose={() => {
          setVisibleEdit(false);
          setThereIsEdit(false);
        }}
        onCancel={() => {
          setVisibleEdit(false);
          setThereIsEdit(false);
        }}
        width="60%"
      >
        <Form
          form={form}
          initialValues={{
            name: permission.name,
            tabs: getDefaultTabs(permission.tabs), // tabs: [{ tabName: "User management" , User: ["Admin"] , Permission: ["Admin", "Reader"] }]
          }}
        >
          <div className={classes.field}>
            <Form.Item
              label="Name"
              name="name"
              rules={[
                {
                  required: true,
                  message: "please enter a name",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </div>
          <Form.List name="tabs">
            {(fields, { add, remove }) => {
              return (
                <div>
                  {fields.map((field) => {
                    return (
                      <Space
                        key={field.key}
                        style={{
                          display: "flex",
                          marginBottom: 8,
                          width: "100%",
                        }}
                        align="start"
                        className="permissions-space"
                      >
                        <div>
                          <Form.Item
                            {...field}
                            label="Tab"
                            name={[field.name, "tabName"]}
                            fieldKey={field.fieldKey}
                            className="permission_add_label edit"
                          >
                            <Select
                              style={{ width: 200 }}
                              onSelect={(value) =>
                                tabSelectHandler(field.key, value)
                              }
                              placeholder="Select tab..."
                            >
                              {tabsOptions.map((t) => (
                                <Option key={t} value={t}>
                                  {t}
                                </Option>
                              ))}
                            </Select>
                          </Form.Item>
                          {fieldsTabs[field.key]
                            ? renderSubTabs(fieldsTabs[field.key], field.name)
                            : null}
                        </div>
                        <MinusCircleOutlined
                          onClick={() => {
                            remove(field.name);
                            removeFieldHandler(field.key);
                          }}
                        />
                      </Space>
                    );
                  })}
                  <Form.Item>
                    <Button
                      type="dashed"
                      onClick={() => {
                        add("", fields.length);
                      }}
                      block
                    >
                      <PlusOutlined /> Add Tab
                    </Button>
                  </Form.Item>
                </div>
              );
            }}
          </Form.List>
        </Form>
      </Modal>
    </div>
  );
}
EditPermission.propTypes = {
  permission: PropTypes.objectOf({
    name: PropTypes.string,
    tabs: PropTypes.array,
  }),
  visibleEdit: PropTypes.bool,
  setVisibleEdit: PropTypes.func,
  updaterCallBack: PropTypes.func.isRequired, // state setter from the parent to force parent re render
};
export default EditPermission;
