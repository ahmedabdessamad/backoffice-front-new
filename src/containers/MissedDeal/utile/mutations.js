import { gql } from "graphql.macro";

const UPDATE_MISSEDDEAL_MUTATION = gql`
  mutation editMissedDeal(
    $missedDealID: ID!
    $input: MissedDealInput!
    $toUpdate: [toUpdateInput]
  ) {
    editMissedDeal(
      missedDealID: $missedDealID
      input: $input
      toUpdate: $toUpdate
    ) {
      missedDealID
      companyId
      companyName
      ITConsultingFirm
      intermedianryAgency
      missionStart
      missionEnd
      skills {
        id
        name
      }
      jobTitle
      dailyRate
      city
      missionProjectDescription
      creator {
        email
      }
      candidate
    }
  }
`;

const CREATE_MISSEDDEAL_MUTATION = gql`
  mutation addMissedDeal($input: MissedDealInput!, $toUpdate: [toUpdateInput]) {
    addMissedDeal(input: $input, toUpdate: $toUpdate) {
      missedDealID
      companyId
      companyName
      ITConsultingFirm
      intermedianryAgency
      missionStart
      missionEnd
      skills {
        name
      }
      jobTitle
      dailyRate
      city
      missionProjectDescription
      creator {
        email
      }
      candidate
    }
  }
`;
export { UPDATE_MISSEDDEAL_MUTATION, CREATE_MISSEDDEAL_MUTATION };
