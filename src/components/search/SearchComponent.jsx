import React, { useState, useEffect } from "react";
import { Input } from "antd";

const SearchComponent = (props) => {
  const { Search } = Input;
  const { loading, data, error, searchHandler, filter, children } = props;
  // const [filter, setFilter] = useState("");
  const [filterdResults, setFilterdResults] = useState([]);
  const [isDisplayDataSet, setIsDisplayDataSet] = useState(false);

  useEffect(() => {
    if (loading && isDisplayDataSet) {
      setFilterdResults(data);
      setIsDisplayDataSet(true);
    }
  }, [filterdResults]);

  if (error) {
    console.error(error);
    return <h1>There was an error</h1>;
  }
  const onSearch = () => searchHandler;
  return (
    <div>
      {isDisplayDataSet && (
        <div>
          <Search
            placeholder="Search..."
            value={filter}
            onSearch={onSearch}
            enterButton
          />
          {children}
        </div>
      )}
    </div>
  );
};
export default SearchComponent;
