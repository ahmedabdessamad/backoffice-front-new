import React from "react";
import {
  Table,
  Tag,
  message,
  Popconfirm,
  Modal,
  Form,
  Input,
  Badge,
} from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { useQuery, useMutation } from "@apollo/react-hooks";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import Spinner from "../../components/Spinner/Spinner";
import GetUsersNumber from "../User/GetUsersNumber";
import { GET_GROUPS } from "./queries";
import { deleteGroupMutation, editGroupMutation } from "./mutations";
import classes from "../User/user.module.scss";
import AddGroup from "./AddGroup";

export default function Groups() {
  const { Column } = Table;
  const [form] = Form.useForm();
  const [group, setGroup] = React.useState("");
  const [editVisible, setEditVisible] = React.useState(false);

  const [deleteGroup] = useMutation(deleteGroupMutation, {
    onCompleted: () => message.success("Group deleted"),
    refetchQueries: [{ query: GET_GROUPS }],
  });

  const [editGroup] = useMutation(editGroupMutation, {
    onCompleted: () => message.success("Group updated"),
    refetchQueries: [{ query: GET_GROUPS }],
  });

  const { loading, error, data } = useQuery(GET_GROUPS);

  if (loading) return <Spinner />;
  if (error) return <p>Error</p>;

  const groupData = data ? data.groups : [];

  return (
    <div>
      <ContentWrapper style={{ height: "100vh" }}>
        <ContentContainer>
          <h1>Groups </h1>
          <div className={classes.header}>
            <AddGroup />
          </div>

          <Table dataSource={groupData} pagination={false} scroll={{ y: 240 }}>
            <Column title="groupID" dataIndex="id" key="id" />,
            <Column
              title="groupName"
              dataIndex="name"
              key="name"
              render={(text) => <Tag color="blue">{text}</Tag>}
            />
            ,
            <Column
              title="Number of Users"
              key="Number of Users"
              render={(record) => (
                <Badge count={GetUsersNumber(record.id, "GROUP")} />
              )}
            />
            <Column
              title="Action"
              dataIndex="Action"
              key="Action"
              render={(text, record) => (
                <span>
                  <div>
                    <EditOutlined
                      style={{ color: "rgb(26, 192, 198)", marginRight: 16 }}
                      onClick={() => setEditVisible(true)}
                    />
                    <Modal
                      title="Edit Group"
                      visible={editVisible}
                      onOk={() => {
                        setEditVisible(false);
                        editGroup({
                          variables: {
                            id: record.id,
                            input: { group },
                          },
                        });
                      }}
                      onCancel={() => setEditVisible(false)}
                    >
                      <Form form={form}>
                        <Form.Item label="Group Name">
                          <Input
                            placeholder="change group Name"
                            onChange={(e) => setGroup(e.target.value)}
                          />
                        </Form.Item>
                      </Form>
                    </Modal>
                  </div>

                  <Popconfirm
                    title="Are you sure delete this Group?"
                    onCancel={() => message.error("reset Delete")}
                    okText="Yes"
                    cancelText="No"
                    onConfirm={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                      deleteGroup({
                        variables: { id: record.id },
                      });
                    }}
                  >
                    <DeleteOutlined style={{ color: "rgb(178,34,34)" }} />
                  </Popconfirm>
                </span>
              )}
            />
          </Table>
        </ContentContainer>
      </ContentWrapper>
    </div>
  );
}
