import React from "react";
import { Checkbox, Row, Col } from "antd";

function CheckboxComponent(props) {
  const { choices } = props;
 // console.log("iko", choices);
  // console.log("xxxxx", x.firstName );
  function onChange(checkedValues) {
   // console.log("checked = ", checkedValues);
  }
  return (
    <div>
      <Checkbox.Group style={{ width: "100%" }} onChange={onChange}>
        <Row>
          {/* {choices.foreach((a) => (
            <Col span={8}>
              <Checkbox value={`${a}`}>ddddddd</Checkbox>
            </Col>
          ))} */}
          {choices &&
            choices.map((choice) => {
              return (
                <div>
                  <Col span={8}>
                    <Checkbox value={choice}>{choice}</Checkbox>
                  </Col>
                </div>
              );
            })}
        </Row>
      </Checkbox.Group>
    </div>
  );
}
export default CheckboxComponent;
