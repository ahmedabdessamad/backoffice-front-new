import React from "react";
import * as PropTypes from "prop-types";
import { Icon } from "cf-neo-ui";
import classes from "./TitleSubInfo.module.scss";

function TitleSubInfo(props) {
  const { id, location, name, email } = props;
  return (
    <div className={classes.container}>
      <div className={classes.container}>
        <span className={classes.item}>
          <span className={classes.hashtag}>#{id}</span>
        </span>
        <span className={classes.item}>
          <Icon type="map-marker" width={10} height={10} color="#838383" />
          <span className={classes.text}>{location}</span>
        </span>
      </div>
      <div className={classes.container}>
        <span className={classes.item}>
          <Icon type="user" width={10} height={10} color="#838383" />
          <span className={classes.text}>{name}</span>
        </span>
        <span className={classes.item}>
          <Icon type="mail" width={10} height={10} color="#838383" />
          <span className={classes.text}>{email}</span>
        </span>
      </div>
    </div>
  );
}

TitleSubInfo.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  location: PropTypes.string,
  name: PropTypes.string,
  email: PropTypes.string,
};

TitleSubInfo.defaultProps = {
  location: "",
  name: "",
  email: "",
};

export default TitleSubInfo;
