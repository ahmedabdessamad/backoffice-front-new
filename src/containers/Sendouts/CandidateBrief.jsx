import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Input } from "antd";
import sendoutAction from "../../redux/sendout/actions";

const { TextArea } = Input;
const { setTextField, setComment } = sendoutAction;

export default function CandidateBrief() {
  const { sendout } = useSelector((state) => state.SendoutReducer);
  const dispatch = useDispatch();

  const commentChangeHandler = ({ target: { value } }) => {
    dispatch(setComment(value));
  };

  return (
    <TextArea
      defaultValue={sendout.comments}
      value={sendout.comments}
      onChange={commentChangeHandler}
      placeholder="Profile Brief"
      autoSize={{ minRows: 3, maxRows: 5 }}
    />
  );
}
