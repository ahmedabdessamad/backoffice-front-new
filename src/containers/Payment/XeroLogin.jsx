import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { Result, Button } from "antd";
import { BUILD_CONSENT_URL } from "./mutations";

function XeroLogin() {
  const [buildConsentUrl] = useMutation(BUILD_CONSENT_URL, {
    onCompleted: (data) => {
      return window.open(data.buildConsentUrl, "_self");
    },
  });
  function handleClick() {
    buildConsentUrl();
  }
  return (
    <Result
      title="Please login with your xero account "
      extra={
        <Button type="primary" key="console" onClick={handleClick}>
          Login with Xero
        </Button>
      }
    />
  );
}
export default XeroLogin;
