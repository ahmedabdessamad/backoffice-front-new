import { gql } from "graphql.macro";

export const REQUEST_RESET = gql`
  mutation requestReset($email: String!) {
    requestReset(email: $email)
  }
`;
export const RESET_PASSWORD = gql`
  mutation resetPassword(
    $userId: String!
    $password: String!
    $confirmPassword: String!
    $resetToken: String!
  ) {
    resetPassword(
      userId: $userId
      password: $password
      confirmPassword: $confirmPassword
      resetToken: $resetToken
    ) {
      userID
      username
      email
      permission {
        _id
        role {
          roleID
          name
        }
        groupe {
          groupID
          name
        }
      }
      bhID
    }
  }
`;
