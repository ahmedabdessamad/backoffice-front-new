import React, { useState } from "react";
import * as PropTypes from "prop-types";
import { IconCircle, H4 } from "cf-neo-ui";
import { Link, withRouter } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import classes from "../Timesheets.module.scss";
import ContentContainer from "../../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../../components/ContentWrapper/ContentWrapper";
import TitleSubInfo from "./title-sub-info/TitleSubInfo";
import TimeSheetCalendar from "./calendar/TimeSheetCalendar";
import allClasses from "./Timesheet.module.scss";
import Messenger from "./messenger";
import PlusHoures from "./plus-houres/PlusHoures";
import ConclusionSection from "./conclusion-section/ConclusionSection";
import { SINGLE_TIMESHEET_QUERY } from "../queries";


function SingleTimesheet(props) {
  const { match } = props;
  const [timesheetState, setTimesheetState] = useState(null);
  const [specialHoursState, setSpecialHoursState] = useState(null);
  const [totalSpecialState, setTotalSpecialState] = useState(null);
  const [totalState, setTotalState] = useState(null);


  const { loading, data, error } = useQuery(SINGLE_TIMESHEET_QUERY, {
    variables: {
      id: match.params.id,
    },
    fetchPolicy: "network-only",
  });

  if (loading) return <div>loading</div>;
  if (error) return <div>Error</div>;
  if (data) {
    const { timesheet } = data;
    if (timesheet) {
      if (!timesheetState) {
        setTimesheetState(timesheet);
      }

      if (!specialHoursState) {
        for (let i = 0; i < timesheet.specialHours.length; i += 1) {
          timesheet.specialHours[i].id = i + 1;
        }
        setSpecialHoursState(timesheet.specialHours);
      }

      if (!totalSpecialState) {
        if (specialHoursState) {
          const temp = specialHoursState.slice();
          let totalSpecial = temp.reduce(
            (a, i) => ({
              hours: (a.hours || 0) + (i.hours || 0),
              minutes: (a.minutes || 0) + (i.minutes || 0),
            }),
            {
              hours: 0,
              minutes: 0,
            }
          );
          const resultDiv = totalSpecial.minutes / 60;
          const resultMode = totalSpecial.minutes % 60;
          totalSpecial = {
            hours: totalSpecial.hours + Math.floor(resultDiv),
            minutes: resultMode,
          };
          setTotalSpecialState(totalSpecial);
        }
      }

      if (totalState === null) {
        if (timesheetState) {
          const temp1 = timesheetState.selectedDays.slice();
          const total = temp1.reduce((a, i) => a + i.hours, 0);
          setTotalState(total);
        }
      }
    }
  }

  const getMonth = (m) => {
    const month = [
      "",
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    return month[m];
  };

  return (
    <ContentWrapper>
      <ContentContainer style={{ backgroundColor: "#f0b2b2" }}>
        {timesheetState ? (
          <div className={classes.header}>
            <div>
              <div className={allClasses.topHeader}>
                <Link to="/home/timesheets" className={allClasses.backButton}>
                  <IconCircle
                    type="chevron-left"
                    color="#8D0417"
                    color2="#D3354A"
                    dimension={30}
                    innerColor="#fff"
                    iconWidth={8.33}
                    iconHeight={8.33}
                  />
                  <span>Timesheets</span>
                </Link>
                {/* <Button */}
                {/*  variant="tertiary" */}
                {/*  size="small" */}
                {/*  role="button" */}
                {/*  type="button" */}
                {/*  className={allClasses.exportButton} */}
                {/*  onClick={() => { */}
                {/*    console.log("bonjour"); */}
                {/*  }} */}
                {/* > */}
                {/*  <Icon */}
                {/*    type="download" */}
                {/*    width={10} */}
                {/*    height={10} */}
                {/*    color="#979797" */}
                {/*  /> */}
                {/*  <span> Exporter en CSV</span> */}
                {/* </Button> */}
              </div>
              <div className={allClasses.title}>
                <H4>
                  Timesheet {getMonth(timesheetState.month)}{" "}
                  {timesheetState.year}
                </H4>
                <span className={allClasses.timesheetStatus}>
                  {timesheetState.status}
                </span>
              </div>
              <TitleSubInfo
                id={timesheetState.id}
                location={timesheetState.info && timesheetState.info.city}
                name={`candidate: ${
                  timesheetState.candidate && timesheetState.candidate.name
                }, client: ${
                  timesheetState.client && timesheetState.client.name
                }`}
                email={
                  timesheetState.candidate && timesheetState.candidate.email
                }
              />
              <div className={allClasses.mainContent}>
                <TimeSheetCalendar
                  month={timesheetState.month}
                  year={timesheetState.year}
                  selectedDays={timesheetState.selectedDays}
                />
                <aside className={allClasses.rightSide}>
                  <div>
                    <Messenger conversation={timesheetState.conversation} />
                  </div>
                  <PlusHoures
                    specialHours={specialHoursState}
                    month={timesheetState.month}
                    year={timesheetState.year}
                  />
                  <ConclusionSection
                    totalSpecial={totalSpecialState}
                    total={totalState}
                    status={timesheetState.status}
                  />
                </aside>
              </div>
            </div>
          </div>
        ) : null}
      </ContentContainer>
    </ContentWrapper>
  );
}

SingleTimesheet.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
};

export default withRouter(SingleTimesheet);
