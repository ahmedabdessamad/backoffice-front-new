import React from "react";
import { Modal, Descriptions } from "antd";

function ModalMarketDetails(marketInfo) {
  Modal.info({
    title: "Market Info",
    content: (
      <Descriptions size="small" column={1}>
        <br />
        <br />
        <Descriptions.Item label="Company">
          {marketInfo.company}
        </Descriptions.Item>
        <Descriptions.Item label="Candidate">
          {marketInfo.contactName}
        </Descriptions.Item>
        <Descriptions.Item label="Job title">
          {marketInfo.jobTitle}
        </Descriptions.Item>
        <Descriptions.Item label="Email">{marketInfo.email}</Descriptions.Item>
        <Descriptions.Item label="Phone">{marketInfo.phone}</Descriptions.Item>
        <Descriptions.Item label="Industry">
          {marketInfo.industry.map(({ name }) => name)}
        </Descriptions.Item>
        <Descriptions.Item label="Skills">
          {marketInfo.skills}
        </Descriptions.Item>
        <Descriptions.Item label="Creator">
          {marketInfo.creator}
        </Descriptions.Item>
        <Descriptions.Item label="Description">
          {marketInfo.description}
        </Descriptions.Item>
      </Descriptions>
    ),

    width: "40%",
    onOk() {},
  });
}
export default ModalMarketDetails;
