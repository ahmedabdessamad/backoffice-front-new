/* eslint import/prefer-default-export: 0 */
import { gql } from "graphql.macro";

export const SENDOUTS = gql`
  query Sendouts($validationStatus: [String], $limit: Int, $skip: Int) {
    sendouts(validationStatus: $validationStatus, limit: $limit, skip: $skip) {
      id
      jobOrderID
      jobOrderTitle
      candidateID
      candidateFirstName
      candidateLastName
      total
    }
  }
`;

export const SENDOUT_DETAILS = gql`
  query Sendout($id: ID!) {
    sendout(id: $id) {
      id
      jobOrderID
      jobOrderTitle
      candidateID
      candidateFirstName
      candidateLastName
      candidateOccupation
      candidateAvailableDate
      candidateAddress {
        address1
        address2
        city
        state
        zip
        countryID
      }
      candidateFiles {
        id
        url
        name
        type
      }
      comments
      payRate
      reference {
        id
        referenceFirstName
        referenceLastName
        referenceTitle
        outcome
      }
      status
    }
  }
`;
