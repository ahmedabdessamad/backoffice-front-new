import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ApolloProvider } from "@apollo/react-hooks";
import store from "./redux/store";
import "./index.scss";
import client from "./apolloClient";

// import App from "./App";
import Routes from "./router";

// import Layout from "./containers/Layout/Layout";
import * as serviceWorker from "./serviceWorker";

console.log("STARTING:", process.env.NODE_ENV);
console.log("STARTING 2:", process.env.REACT_APP_GRAPHQL_EP);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ApolloProvider client={client}>
        {
          // IsError
        }
        <Routes />
      </ApolloProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
