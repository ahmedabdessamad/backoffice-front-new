import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Form, Input, Button } from "antd";
import { useMutation } from "@apollo/react-hooks";
import { RESET_PASSWORD } from "./mutations";

export default function ResetPassword() {
  const { userId, resetToken } = useParams();
  console.log("userId", userId);
  console.log("token", resetToken);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [resetPassword, { called, loading, error }] = useMutation(
    RESET_PASSWORD
  );

  // const [submitted, setSubmitted] = useState(false);
  if (called && loading) return <p>...Loading</p>;
  if (error) return <p>Error...</p>;

  return (
    <div>
      <h3>Reset your password</h3>
      <Form
        name="basic"
        onFinish={() => {
          resetPassword({
            variables: {
              userId,
              resetToken,
              password,
              confirmPassword,
            },
          });
        }}
      >
        <Form.Item
          label="New Password"
          value={password}
          rules={[{ required: true, message: "Please input your email!" }]}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Confirm Password"
          value={confirmPassword}
          rules={[{ required: true, message: "Please input your email!" }]}
          onChange={(e) => {
            setConfirmPassword(e.target.value);
          }}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Update Password
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
