import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_ROLES } from "../containers/Role/queries";
import { GET_GROUPS } from "../containers/Group/queries";

const Utils = () => {
  const { loading: rloading, error: rerror, data: rdata } = useQuery(GET_ROLES);
  const { loading: gloading, error: gerror, data: gdata } = useQuery(
    GET_GROUPS
  );
  if (rloading) return <p> LoadingRole...</p>;
  if (rerror) return <p>RoleError</p>;
  const roleList = rdata.roles.map((role) => role.name);

  if (gloading) return <p> LoadingGroup...</p>;
  if (gerror) return <p>Error</p>;
  const groupList = gdata.groups.map((group) => group.name);

  return { roleList, groupList };
};
export default Utils;
