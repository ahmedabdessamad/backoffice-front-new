/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import { Modal, Typography, Table, message } from "antd";
import React, { useState } from "react";
import * as PropTypes from "prop-types";
import { ArrowRightOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/react-hooks";
import {
  CREATE_MISSEDDEAL_MUTATION,
  UPDATE_MISSEDDEAL_MUTATION,
} from "../../containers/MissedDeal/utile/mutations";
import { FETCH_MISSEDDEAL_QUERY } from "../../containers/MissedDeal/utile/queries";

const { Title } = Typography;

function ModalUpdateCandidate(props) {
  const { missedDeal, visible, setVisible, history, id, MutationType } = props;

  const [toUpdate, setToUpdate] = useState([]);
  const [errors, setErrors] = useState({});

  const success = () => {
    message.success("Missed Deal added successfully");
  };
  const success2 = () => {
    message.success("Candidate record updated successfully");
  };
  const [createMissedDeal] = useMutation(CREATE_MISSEDDEAL_MUTATION, {
    variables: {
      input: {
        company: missedDeal.company,
        candidateID: missedDeal.candidate,
        ITConsultingFirm: missedDeal.ITConsultingFirm,
        intermedianryAgency: missedDeal.intermedianryAgency,
        missionStart: missedDeal.missionStart,
        missionEnd: missedDeal.missionEnd,
        skills: missedDeal.skills,
        jobTitle: missedDeal.jobTitle,
        dailyRate: missedDeal.dailyRate,
        city: missedDeal.city,
        missionProjectDescription: missedDeal.missionProjectDescription,
      },
      toUpdate,
    },
    refetchQueries: () => [{ query: FETCH_MISSEDDEAL_QUERY }],
    onCompleted() {
      history.push("/home/missedDeal");
      success();
      if (toUpdate.length > 0) {
        success2();
      }
    },
    onError(err) {
      setErrors(err.graphQLErrors[0].message);
    },
  });
  const [editMissedDeal] = useMutation(UPDATE_MISSEDDEAL_MUTATION, {
    variables: {
      missedDealID: id,
      input: {
        company: missedDeal.company,
        candidateID: missedDeal.candidate,
        ITConsultingFirm: missedDeal.ITConsultingFirm,
        intermedianryAgency: missedDeal.intermedianryAgency,
        missionStart: missedDeal.missionStart,
        missionEnd: missedDeal.missionEnd,
        skills: missedDeal.skills,
        jobTitle: missedDeal.jobTitle,
        dailyRate: missedDeal.dailyRate,
        city: missedDeal.city,
        missionProjectDescription: missedDeal.missionProjectDescription,
      },
      toUpdate,
    },
    refetchQueries: () => [{ query: FETCH_MISSEDDEAL_QUERY }],
    onCompleted() {
      history.push("/home/missedDeal");
      success();
      if (toUpdate.length > 0) {
        success2();
      }
    },

    onError(err) {
      setErrors(err.graphQLErrors[0].message);
    },
  });
  const handleOk = (e) => {
    setVisible(false);
    if (MutationType === "create") createMissedDeal();
    else {
      editMissedDeal();
    }
  };
  const handleCancel = (e) => {
    setVisible(false);
  };
  const data = [
    {
      key: 1,
      missedDealFields:
        "Company/Mission Start/Mission End/Daily Rate/Job Title",
      column: "",
      candidateRecordFields: "Employment History",
    },
    {
      key: 2,
      missedDealFields: "Mission End",
      column: "",
      candidateRecordFields: "Status/Date Available/Last Availability Update",
    },
    {
      key: 3,
      missedDealFields: "Skills",
      column: "",
      candidateRecordFields: "Qualified Skills",
    },
    {
      key: 4,
      missedDealFields: "Daily Rate",
      column: "",
      candidateRecordFields: "Desired daily rate",
    },
    {
      key: 5,
      missedDealFields: "Job Title",
      column: "",
      candidateRecordFields: "Current job title",
    },
  ];
  const columns = [
    {
      title: "Missed Deal fields",
      dataIndex: "missedDealFields",
      key: "missedDealFields",
      width: "50%",
    },
    {
      title: "",
      dataIndex: "column",
      key: "column",
      width: "10%",
      render: () => (
        <div>
          <ArrowRightOutlined style={{ color: "#1890ff" }} />
        </div>
      ),
    },
    {
      title: "Candidate record fields",
      dataIndex: "candidateRecordFields",
      width: "50%",
      key: "candidateRecordFields",
    },
  ];

  const exist = (toUpdate, index) => {
    let i = 0;
    for (i = 0; i < toUpdate.length; i += 1) {
      if (index in toUpdate[i]) {
        return i;
      }
    }
    return -1;
  };
  let tab = [];
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      // tab = [];
      for (let i = 0; i < selectedRows.length; i += 1) {
        if (selectedRows[i].key === 1 && exist(tab, "job") === -1) {
          const ob = {
            job: {
              company: missedDeal.company,
              missionStart: new Date(missedDeal.missionStart).toISOString(),
              missionEnd: new Date(missedDeal.missionEnd).toISOString(),
              jobTitle: missedDeal.jobTitle,
              dailyRate: missedDeal.dailyRate,
            },
          };
          // setToUpdate([...toUpdate, ob]);
          tab.push(ob);
        }
        if (selectedRows[i].key === 2 && exist(tab, "missionEnd") === -1) {
          const ob2 = {
            missionEnd: new Date(missedDeal.missionEnd).toISOString(),
          };
          // setToUpdate([...toUpdate, ob2]);
          tab.push(ob2);
        }
        if (selectedRows[i].key === 3 && exist(tab, "skills") === -1) {
          const ob3 = {
            skills: missedDeal.skills,
          };
          // setToUpdate([...toUpdate, ob3]);
          tab.push(ob3);
        }
        if (selectedRows[i].key === 4 && exist(tab, "dailyRate") === -1) {
          const ob4 = {
            dailyRate: missedDeal.dailyRate,
          };
          // setToUpdate([...toUpdate, ob4]);
          tab.push(ob4);
        }
        if (selectedRows[i].key === 5 && exist(tab, "jobTitle") === -1) {
          const ob5 = {
            jobTitle: missedDeal.jobTitle,
          };
          // setToUpdate([...toUpdate, ob5]);
          tab.push(ob5);
        }
      }
      setToUpdate(tab);
    },
    onSelect: (record, selected, selectedRows) => {
      for (let i = 0; i < selectedRows.length; i += 1) {
        if (selectedRows[i].key === 1 && exist(tab, "job") === -1) {
          const ob = {
            job: {
              company: missedDeal.company,
              missionStart: new Date(missedDeal.missionStart).toISOString(),
              missionEnd: new Date(missedDeal.missionEnd).toISOString(),
              jobTitle: missedDeal.jobTitle,
              dailyRate: missedDeal.dailyRate,
            },
          };
          // setToUpdate([...toUpdate, ob]);
          tab.push(ob);
        }
        if (selectedRows[i].key === 2 && exist(tab, "missionEnd") === -1) {
          const ob2 = {
            missionEnd: new Date(missedDeal.missionEnd).toISOString(),
          };
          // setToUpdate([...toUpdate, ob2]);
          tab.push(ob2);
        }
        if (selectedRows[i].key === 3 && exist(tab, "skills") === -1) {
          const ob3 = {
            skills: missedDeal.skills,
          };
          // setToUpdate([...toUpdate, ob3]);
          tab.push(ob3);
        }
        if (selectedRows[i].key === 4 && exist(tab, "dailyRate") === -1) {
          const ob4 = {
            dailyRate: missedDeal.dailyRate,
          };
          // setToUpdate([...toUpdate, ob4]);
          tab.push(ob4);
        }
        if (selectedRows[i].key === 5 && exist(tab, "jobTitle") === -1) {
          const ob5 = {
            jobTitle: missedDeal.jobTitle,
          };
          // setToUpdate([...toUpdate, ob5]);
          tab.push(ob5);
        }
      }
      setToUpdate(tab);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      if (selected === false) {
        tab = [];
      }
      setToUpdate(tab);
    },
  };

  return (
    <div>
      <Modal
        title="Which field would you like to update on the candidate record?"
        visible={visible}
        onOk={handleOk}
        onCancel={handleCancel}
        width="60%"
      >
        <Table
          style={{ position: "relative" }}
          columns={columns}
          rowSelection={rowSelection}
          dataSource={data}
          pagination={false}
        />
      </Modal>
    </div>
  );
}

export default ModalUpdateCandidate;
