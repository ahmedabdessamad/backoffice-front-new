import React, { useState } from "react";
import { Form, Input, Button } from "antd";

const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: { span: 24, offset: 0 },
    sm: { span: 20, offset: 4 },
  },
};
function ChoicesForm(props) {
  const {
    choicesInputs,
    setChoicesInputs,
    onFinish1,
    setChoicesModal,
    choicesModals,
  } = props;
  const [inputList, setInputList] = useState([{ firstName: "", lastName: "" }]);

  /* const onFinish = (values) => {
    console.log("Received values of form:", Object.values(values.names));
    const tabOfVlues = Object.values(values.names);
    const tab = tabOfVlues.map((item) => {
      return { value: item };
    });
    console.log("tab:", tab);
    //setChoicesModal(tab);
  }; */

  return (
    <Form
      name="dynamic_form_item"
      {...formItemLayoutWithOutLabel}
      onFinish={() => onFinish1}
    >
      <Form.List name="names">
        {() => {
          return <div>{choicesInputs.map((field) => field)}</div>;
        }}
      </Form.List>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Validate choices
        </Button>
      </Form.Item>
    </Form>
  );
}

export default ChoicesForm;
