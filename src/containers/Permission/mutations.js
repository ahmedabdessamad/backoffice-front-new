import { gql } from "graphql.macro";

export const editPermissionMutation = gql`
  mutation editPermission($id: ID!, $input: permissionInput!) {
    editPermission(id: $id, input: $input)
  }
`;
export const deletePermissionMutation = gql`
  mutation deletePermission($id: ID!) {
    deletePermission(id: $id)
  }
`;
export const addPermissionMutation = gql`
  mutation addPermission($input: permissionInput!) {
    addPermission(input: $input) {
      name
      tabs {
        name
        permission
        parent
      }
    }
  }
`;
