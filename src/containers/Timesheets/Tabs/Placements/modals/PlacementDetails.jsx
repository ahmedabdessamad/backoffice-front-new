import React from "react";
import * as PropTypes from "prop-types";
import { Col, Row, Typography } from "antd";
import { withRouter } from "react-router-dom";
import getDate from "../../../../../helpers/convertTimestampToDate";

const { Text } = Typography;

function PlacementDetails(props) {
  const { placement } = props;
  console.log(placement);

  const getSignataireName = (pl) => {
    if (pl.signataire)
      if (pl.signataire.secondarySignataires[0])
        return `${pl.signataire.secondarySignataires[0].firstName} ${pl.signataire.secondarySignataires[0].lastName}`;
    return null;
  };
  const getSignataireEmail = (pl) => {
    if (pl.signataire)
      if (pl.signataire.secondarySignataires[0])
        return pl.signataire.secondarySignataires[0].email;
    return null;
  };
  const getSignataireId = (pl) => {
    if (pl.signataire)
      if (pl.signataire.secondarySignataires[0])
        return pl.signataire.secondarySignataires[0].id;
    return null;
  };

  return (
    <div>
      <p>Il est recommandé de mettre à jour ces informations dans Bullhorn</p>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <Text>Placement: </Text>
              <Text style={{ color: "blue" }}>{placement.id}</Text>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Text>Project: </Text>
              <Text style={{ color: "blue" }}>{placement.project}</Text>
            </Col>
            <Col span={12}>
              <Text>Mission: </Text>
              <Text style={{ color: "blue" }}>
                {placement.jobOrder ? placement.jobOrder.title : null}
              </Text>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Text>Debut: </Text>
              <Text style={{ color: "blue" }}>
                {getDate(placement.dateBegin)}
              </Text>
            </Col>
            <Col span={12}>
              <Text>Fin: </Text>
              <Text style={{ color: "blue" }}>
                {getDate(placement.dateEnd)}
              </Text>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Text>TJN Vente: </Text>
              <Text style={{ color: "blue" }}>{placement.clientBillRate}</Text>
            </Col>
            <Col span={12}>
              <Text>TJM Achat: </Text>
              <Text style={{ color: "blue" }}>{placement.payRate}</Text>
            </Col>
          </Row>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={3} />
        <Col span={18}>
          <hr style={{ border: "1px solid blue" }} />
        </Col>
        <Col span={3} />
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <Text>Consultant: </Text>
              <Text style={{ color: "blue" }}>
                {placement.candidate
                  ? `${placement.candidate.firstName} ${placement.candidate.lastName}`
                  : null}
              </Text>
            </Col>
            <Col span={12}>
              <Text>Statut: </Text>
              <Text style={{ color: "blue" }}>{placement.status}</Text>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Text>ID: </Text>
              <Text style={{ color: "blue" }}>
                {placement.candidate ? placement.candidate.id : null}
              </Text>
            </Col>
            <Col span={12}>
              <Text>Email: </Text>
              <Text style={{ color: "blue" }}>{placement.consultantEmail}</Text>
            </Col>
          </Row>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={3} />
        <Col span={18}>
          <hr style={{ border: "1px solid blue" }} />
        </Col>
        <Col span={3} />
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <Text>Signataire 1: </Text>
              <Text style={{ color: "blue" }}>
                {placement.signataire
                  ? `${placement.signataire.principalSignataire.firstName} ${placement.signataire.principalSignataire.lastName}`
                  : "not mentioned"}
              </Text>
            </Col>
            <Col span={12}>
              <Text>Statut: </Text>
              <Text style={{ color: "blue" }}>{null}</Text>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Text>ID: </Text>
              <Text style={{ color: "blue" }}>
                {placement.signataire
                  ? placement.signataire.principalSignataire.id
                  : "not mentioned"}
              </Text>
            </Col>
            <Col span={12}>
              <Text>Email: </Text>
              <Text style={{ color: "blue" }}>
                {placement.signataire
                  ? placement.signataire.principalSignataire.email
                  : "not mentioned"}
              </Text>
            </Col>
          </Row>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={3} />
        <Col span={18}>
          <hr style={{ border: "1px solid blue" }} />
        </Col>
        <Col span={3} />
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <Text>Signataire 2: </Text>
              <Text style={{ color: "blue" }}>
                {getSignataireName(placement)}
              </Text>
            </Col>
            <Col span={12}>
              <Text>Statut: </Text>
              <Text style={{ color: "blue" }}>{null}</Text>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Text>ID: </Text>
              <Text style={{ color: "blue" }}>
                {getSignataireId(placement)}
              </Text>
            </Col>
            <Col span={12}>
              <Text>Email: </Text>
              <Text style={{ color: "blue" }}>
                {getSignataireEmail(placement)}
              </Text>
            </Col>
          </Row>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={3} />
        <Col span={18}>
          <hr style={{ border: "1px solid blue" }} />
        </Col>
        <Col span={3} />
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Row>
            <Col span={12}>
              <Text>KAM: </Text>
              <Text style={{ color: "blue" }}>
                {placement.owner
                  ? `${placement.owner.firstName} ${placement.owner.lastName}`
                  : null}
              </Text>
            </Col>
            <Col span={12}>
              <Text>Email: </Text>
              <Text style={{ color: "blue" }}>{placement.ownerEmail}</Text>
            </Col>
          </Row>
          {/* <Col span={8}>
              <Text type="link" underline style={{ color: "blue" }}>
                {str}
              </Text>
            </Col> */}
        </Col>
      </Row>
    </div>
  );
}

PlacementDetails.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  placement: PropTypes.shape({}).isRequired,
};
export default withRouter(PlacementDetails);
