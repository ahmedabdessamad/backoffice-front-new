import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table } from "antd";
import sendoutAction from "../../redux/sendout/actions";
import CvUploader from "./CvUploader";

const { selectFile } = sendoutAction;

const fileColumns = [
  {
    title: "#ID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "File Name",
    dataIndex: "name",
    key: "name",
    render: (text, record) => (
      <a href={record.url} target="_blank" rel="noopener noreferrer">
        {text}
      </a>
    ),
  },
  {
    title: "Type",
    dataIndex: "type",
    key: "type",
  },
];

export default function () {
  const { sendout } = useSelector((state) => state.SendoutReducer);
  const dispatch = useDispatch();

  const handleFileSelection = (fileId) => dispatch(selectFile(fileId));

  return (
    <div>
      <div style={{ marginBottom: 16 }}>
        <CvUploader />
      </div>
      <Table
        rowSelection={{
          type: "radio",
          onChange: handleFileSelection,
        }}
        rowKey="id"
        columns={fileColumns}
        dataSource={sendout.candidateFiles}
        pagination={false}
      />
    </div>
  );
}
