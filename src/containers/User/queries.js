import { gql } from "graphql.macro";

export const GET_USERS = gql`
  {
    users {
      userID
      username
      email
      permissions {
        name
        tabs {
          name
          permission
          parent
        }
      }
      bhID
    }
  }
`;
export const GET_USER = gql`
  query user($userID: ID!) {
    user(userID: $userID) {
      userID
      username
      email
      permissions {
        name
        tabs {
          name
          permission
          parent
        }
      }
      bhID
    }
  }
`;
export const GET_USERSNUMBER = gql`
  query usersNumber($id: ID!, $field: PermissionField) {
    usersNumber(id: $id, field: $field)
  }
`;
