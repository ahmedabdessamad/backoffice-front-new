/* eslint-disable no-unused-vars */
import { Modal, Form, Input, Card, message } from "antd";
import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import { SEND_FORM_MUTATION } from "../../containers/Form/utile/mutations";

const { TextArea } = Input;

function ModalEmailInput(props) {
  const { visibleModalEmailInput, setVisibleModalEmailInput, form } = props;

  const [email, setEmail] = useState("");
  const [errors, setErrors] = useState({});
  const [form1, setForm1] = useState({ key: "0", formID: "0" });
  if (form) {
    if (form1.key === "0") {
      setForm1(form);
    }
  }
  const onChangeTextArea = (event) => {
    setEmail(event.target.value);
  };
  const success = () => {
    message.success("Form sent successfully");
  };
  const [sendForm] = useMutation(SEND_FORM_MUTATION, {
    variables: {
      formID: form1.key,
      email,
    },
    onCompleted() {
      setEmail("");
      setForm1({ key: "0", formID: "0" });
      success();
    },
    onError(err) {
      setErrors(err.graphQLErrors[0].message);
    },
  });
  const error11 = () => {
    message.error("Please input an email!");
  };
  const handleOk = (e) => {
    setVisibleModalEmailInput(false);
    if (form1.key !== "0") {
      if (email) sendForm();
      else error11();
    }
  };
  const handleCancel = (e) => {
    setVisibleModalEmailInput(false);
  };

  return (
    <div>
      <Modal
        title="Email destination"
        visible={visibleModalEmailInput}
        onOk={handleOk}
        onCancel={handleCancel}
        width="40%"
      >
        <div>
          <Card>
            <Form layout="vertical">
              <div>
                <Form.Item
                  name="email"
                  label="Email"
                  rules={[
                    {
                      required: true,
                      message: "Please input the destination!",
                    },
                  ]}
                >
                  <TextArea
                    name={email}
                    placeholder="email"
                    type="text"
                    onChange={onChangeTextArea}
                    autoSize={{ minRows: 3, maxRows: 5 }}
                  />
                </Form.Item>
              </div>
            </Form>
          </Card>
        </div>
      </Modal>
    </div>
  );
}

export default ModalEmailInput;
