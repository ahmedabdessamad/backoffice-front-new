import { gql } from "graphql.macro";

const STOP_MAILING = gql`
  mutation StopMailing($email: String!, $services: [String]) {
    stopMailing(email: $email, services: $services)
  }
`;

const RESUME_MAILING = gql`
  mutation ResumeMailing($email: String!) {
    resumeMailing(email: $email)
  }
`;

export { STOP_MAILING, RESUME_MAILING };
