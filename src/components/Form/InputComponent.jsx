/* eslint-disable no-unused-vars */
import React from "react";
import { Input, InputNumber } from "antd";
import { useSelector, useDispatch } from "react-redux";
import checkboxChoicesActions from "../../redux/checkboxChoices/actions";

const { setInputList } = checkboxChoicesActions;

const { TextArea } = Input;
function InputComponent(props) {
  const dispatch = useDispatch();
  const { inputList } = useSelector((state) => state.CheckboxChoicesReducer);
  const { x, i } = props;
  // console.log("xxxxx", x.firstName );
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    // console.log("e.target", e.target);
    const list = [...inputList];
    list[index].value = value;
    dispatch(setInputList(list));
  };
  const onChangeTextArea = (e, index) => {
    const { name, value } = e.target;
    // console.log("target", e.target.value);
    const list = [...inputList];
    list[index].value = value;
    dispatch(setInputList(list));
  };
  const onChangeInputNumber = (value, index) => {
    // const { name, value } = e.target;
    //console.log("value", value, "   iiii ", i);
    const list = [...inputList];
    list[index].value = value;
    dispatch(setInputList(list));
  };
  return (
    <div>
      {x.key === "simpleInput" ? (
        <Input
          name={x.name}
          placeholder={x.placeholder}
          onChange={(e) => handleInputChange(e, i)}
          type={x.type}
          defaultValue={x.defaultValue}
        />
      ) : null}

      {x.key === "inputText" ? (
        <TextArea
          name={x.name}
          placeholder={x.placeholder}
          type="text"
          onChange={(e) => onChangeTextArea(e, i)}
          autoSize={{ minRows: 3, maxRows: 5 }}
        />
      ) : null}

      {x.key === "inputNumber" ? (
        <div>
          {x.type === "Number" ? (
            <InputNumber
              name={x.name}
              placeholder={x.placeholder}
              onChange={(value) => onChangeInputNumber(value, i)}
              autoSize={{ minRows: 3, maxRows: 5 }}
              defaultValue={x.defaultValue}
            />
          ) : null}
          {x.type === "Decimal" ? (
            <InputNumber
              name={x.name}
              placeholder={x.placeholder}
              onChange={(value) => onChangeInputNumber(value, i)}
              autoSize={{ minRows: 3, maxRows: 5 }}
              defaultValue={x.defaultValue}
              step={0.1}
            />
          ) : null}
          {x.type === "Percentage" ? (
            <InputNumber
              name={x.name}
              placeholder={x.placeholder}
              onChange={(value) => onChangeInputNumber(value, i)}
              autoSize={{ minRows: 3, maxRows: 5 }}
              defaultValue={x.defaultValue}
              formatter={(value) => `${value}%`}
              parser={(value) => value.replace("%", "")}
              step={0.1}
            />
          ) : null}
          {x.type === "Dollar" ? (
            <InputNumber
              name={x.name}
              placeholder={x.placeholder}
              onChange={(value) => onChangeInputNumber(value, i)}
              autoSize={{ minRows: 3, maxRows: 5 }}
              defaultValue={x.defaultValue}
              step={0.1}
              formatter={(value) =>
                `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
            />
          ) : null}
          {x.type === "Euro" ? (
            <InputNumber
              name={x.name}
              placeholder={x.placeholder}
              onChange={(value) => onChangeInputNumber(value, i)}
              autoSize={{ minRows: 3, maxRows: 5 }}
              defaultValue={x.defaultValue}
              step={0.1}
              formatter={(value) =>
                `€ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\€\s?|(,*)/g, "")}
            />
          ) : null}
          {x.type === "Pound" ? (
            <InputNumber
              name={x.name}
              placeholder={x.placeholder}
              onChange={(value) => onChangeInputNumber(value, i)}
              autoSize={{ minRows: 3, maxRows: 5 }}
              defaultValue={x.defaultValue}
              step={0.1}
              formatter={(value) =>
                `£ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\£\s?|(,*)/g, "")}
            />
          ) : null}
        </div>
      ) : null}
    </div>
  );
}
export default InputComponent;
