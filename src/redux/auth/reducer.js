import Cookies from "universal-cookie";
import actions from "./actions";
import { cookieRemoveOpt } from "../../configs/domainConfigs";

const cookies = new Cookies();

const initState = {
  token: localStorage.getItem("token") || cookies.get("AUTH_TOKEN"),
  user: null,
};

export default function appReducer(state = initState, action) {
  if (action.type === actions.LOGIN_SUCCESS) {
    return {
      ...state,
      token: action.payload.token,
      user: action.payload,
    };
  }

  if (action.type === actions.SET_USER) {
    return {
      ...state,
      user: action.payload,
    };
  }

  if (action.type === actions.LOGOUT) {
    localStorage.removeItem("token");
    cookies.remove("AUTH_TOKEN", cookieRemoveOpt);
    // payload is the cache of apollo client
    action.payload.reset();
    return {
      ...state,
      user: null,
      token: null,
    };
  }

  return state;
}
