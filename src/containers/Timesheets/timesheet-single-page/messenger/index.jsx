import React, { useState } from "react";
import PropTypes from "prop-types";
import { Messenger } from "cf-neo-ui";
import moment from "moment";
import { withRouter } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import classes from "./styles.module.scss";
import CONVERSATION from "./query";
import { SINGLE_TIMESHEET_QUERY } from "../../queries";

function Index(props) {
  const { match } = props;
  const [messages, setMessages] = useState(null);
  const { data } = useQuery(SINGLE_TIMESHEET_QUERY, {
    variables: {
      id: parseInt(match.params.id, 10),
    },
    fetchPolicy: "network-only",
  });

  if (data && !messages) {
    // const { timesheetConversation } = data;conversation
    const { conversation } = data.timesheet;
    if (conversation) {
      const msg = conversation.messages;
      const firstSpeaker = msg[0].speaker;
      for (let i = 0; i < msg.length; i += 1) {
        if (firstSpeaker === msg[i].speaker) {
          msg[i].speaker = null;
        }
        msg[i].date = moment(msg[i].date, "x").format("DD/MM/YY HH:mm");
      }
      setMessages(msg);
    }
  }
  return (
    <div className={classes.messengerHolder}>
      <Messenger
        height="350px"
        messages={messages || []}
        send={() => {}}
        unlimitedChat={false}
        getComment={() => {}}
        disabled
        withInputSection={false}
        withHeader
        headerText="Commentaires"
      />
    </div>
  );
}

Index.propTypes = {
  i18n: PropTypes.shape({
    _: PropTypes.func,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
};

export default withRouter(Index);
