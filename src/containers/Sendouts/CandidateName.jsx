import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Switch } from "antd";
import sendoutAction from "../../redux/sendout/actions";
import EditableText from "../../components/EditableText/EditableText";

const { setTextField, setDisplayName } = sendoutAction;

export default function CandidateName() {
  const { sendout: currentSendout, displayName } = useSelector(
    (state) => state.SendoutReducer
  );
  const dispatch = useDispatch();

  const saveHandler = (fieldName, value) => {
    dispatch(setTextField({ fieldName, value }));
  };

  const switchChangeHandler = (checked) => {
    dispatch(setDisplayName(checked));
  };

  return (
    <div style={{ display: "flex" }}>
      <p style={{ padding: "5px 12px" }}>#{currentSendout.candidateID}</p>
      <EditableText
        editable={displayName}
        text={currentSendout.candidateFirstName}
        onSave={(value) => saveHandler("candidateFirstName", value)}
      />
      <EditableText
        editable={displayName}
        text={currentSendout.candidateLastName}
        onSave={(value) => saveHandler("candidateLastName", value)}
      />
      <div>
        <Switch
          checkedChildren="Display"
          unCheckedChildren="Hide"
          defaultChecked
          onChange={switchChangeHandler}
        />
      </div>
    </div>
  );
}
