/* eslint-disable import/prefer-default-export */
import { gql } from "graphql.macro";

export const editRoleMutation = gql`
  mutation editRole($id: ID!, $input: roleInput!) {
    editRole(id: $id, input: $input)
  }
`;
export const deleteRoleMutation = gql`
  mutation deleteRole($id: ID!) {
    deleteRole(id: $id)
  }
`;
export const addRoleMutation = gql`
  mutation addRole($role: String!) {
    addRole(role: $role) {
      name
    }
  }
`;
