import React, { useState } from "react";
import { Table, Tag } from "antd";
import { ClockCircleOutlined } from "@ant-design/icons";
import { useQuery } from "@apollo/react-hooks";
import { Link, useRouteMatch } from "react-router-dom";
import { SENDOUTS } from "./queries";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../components/ContentContainer/ContentContainer";

export default function PendingSendouts() {
  const match = useRouteMatch();

  const [skip, setSkip] = useState(0);
  const [limit, setLimit] = useState(10);

  const paginate = (page, pageSize) => {
    setSkip((page - 1) * pageSize);
    setLimit(pageSize);
  };

  const columns = [
    {
      title: "Vacancy",
      key: "vacancy",
      render: (text, record) => `${record.jobOrderID} ${record.jobOrderTitle}`,
    },
    {
      title: "Status",
      dataIndex: "validationStatus",
      key: "validationStatus",
      render: () => (
        <Tag icon={<ClockCircleOutlined />} color="default">
          pending
        </Tag>
      ),
    },
    {
      title: "Candidate",
      key: "candidate",
      render: (text, record) =>
        `${record.candidateID} ${record.candidateFirstName} ${record.candidateLastName}`,
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Link to={`${match.path}/${record.id}`}>
          <span>
            <a>Details</a>
          </span>
        </Link>
      ),
    },
  ];

  const { loading, error, data } = useQuery(SENDOUTS, {
    variables: { validationStatus: ["pending"], limit, skip },
  });

  if (error) return <p>Error</p>;
  let pendingSendouts = [];
  let total = null;
  if (data) {
    pendingSendouts = data.sendouts;
    total = data.sendouts && data.sendouts[0] && data.sendouts[0].total;
  }

  return (
    <ContentWrapper>
      <ContentContainer>
        <Table
          columns={columns}
          dataSource={pendingSendouts}
          pagination={{
            onChange: paginate,
            total,
          }}
          loading={loading}
        />
      </ContentContainer>
    </ContentWrapper>
  );
}
