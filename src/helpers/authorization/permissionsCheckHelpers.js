const UNAUTHENTICATED = new Error("UNAUTHENTICATED");

const isUserAuthorized = (user, requiredPermission) => {
  if (!user) throw UNAUTHENTICATED;
  const tabs = user.permissions.map((p) => p.tabs);
  const newTab = tabs.flat().find(({ name, permission }) => {
    // console.log("name",name,"permission",permission);
    return (
      name in requiredPermission && (permission & requiredPermission[name]) > 0
    );
  });
  // console.log("new tab ***************************>", newTab);
  if (newTab) return true;
  return false;
};

const hasGroup = (user, group) =>
  user.permission.find((perm) => perm.groupe.name === group);

module.exports = { isUserAuthorized, hasGroup };
