import React, { useState } from "react";
import { Table, message, Popconfirm, Button, Tag } from "antd";
import { useDispatch } from "react-redux";
import { PlusOutlined } from "@ant-design/icons";
import { useQuery, useMutation } from "@apollo/react-hooks";
import classes from "./user.module.scss";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import Spinner from "../../components/Spinner/Spinner";
import { GET_USERS } from "./queries";
import { deleteUserMutation as DELETE_USER_MUTATION } from "./mutations";
import UserModal from "./UserModal";
import usersAction from "../../redux/users/actions";

const { toggleUserModal } = usersAction;

// const getNames = (arr) => (arr ? arr.map((elm) => elm.name) : []);

export default function UserPage() {
  const dispatch = useDispatch();

  const [page, setPage] = useState(1);
  const { loading, error, data } = useQuery(GET_USERS);

  const [deleteUser] = useMutation(DELETE_USER_MUTATION, {
    onCompleted: () => message.success("User Deleted!"),
    onError: () => {
      message.error("Oops Something went wrong");
    },
    refetchQueries: [{ query: GET_USERS }],
    /* update: (cache, { data: { deleteUser } }) => {
      const { users } = cache.readQuery({ query: GET_USERS });
      const newUsers = users.filter(
        ({ userID }) => userID !== deleteUser.userID
      );
      cache.writeQuery({ query: GET_USERS, data: { users: newUsers } });
    }, */
  });

  const columns = [
    /* { title: "ID", dataIndex: "bhID", key: "bhID" }, */
    { title: "userName", dataIndex: "username", key: "username" },
    { title: "email", dataIndex: "email", key: "email" },
    {
      title: "Permissions",
      key: "permission",
      render: (text, record) => (
        <div>
          {record.permissions.map((p) => (
            <Tag color="geekblue">
              {/* {p.groupe.name}:{p.role.name} */}
              {p.name}
            </Tag>
          ))}
        </div>
      ),
    },

    {
      title: "Action",
      dataIndex: "Action",
      key: "Action",
      render: (text, record) => (
        <span>
          <a
            role="button"
            tabIndex={0}
            style={{ marginRight: 16 }}
            onKeyDown={() => dispatch(toggleUserModal(record))}
            onClick={() => dispatch(toggleUserModal(record))}
          >
            Edit
          </a>
          <Popconfirm
            title="Are you sure delete this User?"
            onCancel={() => message.error("reset Delete")}
            okText="Yes"
            cancelText="No"
            onConfirm={(e) => {
              e.preventDefault();
              e.stopPropagation();
              deleteUser({
                variables: { userID: record.userID },
              });
            }}
          >
            <a>Delete</a>
          </Popconfirm>
        </span>
      ),
    },
  ];

  if (loading) return <Spinner />;

  if (error) return <p>Error</p>;

  const userData = data ? data.users : [];

  return (
    <ContentWrapper>
      <ContentContainer>
        <div className={classes.title}>
          <h3>Users</h3>
          <div>
            <Button
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => dispatch(toggleUserModal())}
            >
              Add New User
            </Button>
          </div>
        </div>
        <UserModal />
        <Table
          dataSource={userData}
          columns={columns}
          pagination={{
            defaultCurrent: 1,
            total: Math.ceil(userData / 5),
            onchange: () => setPage(page),
            pageSize: 5,
          }}
        />
      </ContentContainer>
    </ContentWrapper>
  );
}
