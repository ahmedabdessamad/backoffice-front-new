const UNAUTHENTICATED = new Error("UNAUTHENTICATED");

const checkSpecificPermission = (requiredPermission, user, ownerID) => {
  if (!user) throw UNAUTHENTICATED;
  const tabs = user.permissions.map((p) => p.tabs);
  const newTabAdmin = tabs
    .flat()
    .find(
      ({ name, permission }) =>
        name in requiredPermission && (permission & 4) === 4
    );
  if (newTabAdmin) return true;

  const newTabEdit = tabs
    .flat()
    .find(
      ({ name, permission }) =>
        name in requiredPermission && (permission & 2) === 2
    );

  if (newTabEdit && (!ownerID || ownerID.toString() === user.userID.toString()))
    return true;

  return false;
};

module.exports = checkSpecificPermission;
