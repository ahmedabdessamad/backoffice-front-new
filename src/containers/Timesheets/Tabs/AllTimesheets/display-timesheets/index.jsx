import React, { useState, useEffect } from "react";
import * as PropTypes from "prop-types";
import { withRouter, Link } from "react-router-dom";
import { Table, Button, Input, Space, Tag, Tooltip } from "antd";
import { SettingFilled, SearchOutlined } from "@ant-design/icons";
import { useQuery } from "@apollo/react-hooks";
import TIMESHEETS_QUERY from "./query";
import localStateToQuery from "./localStateToQuery";

const tagColors = {
  open: "geekblue",
  pending: "gold",
  approved: "green",
  rejected: "volcano",
};

const toolTiped = (user) => (
  <Tooltip title={user && user.id}>
    <span>{user && user.name}</span>
  </Tooltip>
);

function TableComponents(props) {
  const { status } = props;
  const initialFilters = {
    status,
    timesheetID: null,
    clientID: null,
    candidateID: null,
    placementID: null,
    limit: 10,
    skip: 0,
    name: null,
    periodStart: null,
    periodEnd: null,
    month: null,
    year: null,
  };

  const [filters, setFilters] = useState(initialFilters);
  const [queryFilters, setQueryFilters] = useState(
    localStateToQuery(initialFilters)
  );

  const searchQuery = () => {
    setFilters({ ...filters, searchTrigger: true });
  };
  const clearSearchQuery = () => {
    setFilters({ ...initialFilters, searchTrigger: true });
  };
  const clearSearchQueryItem = (dataIndex) => {
    const newState = { ...filters };
    newState[dataIndex] = null;
    newState.searchTrigger = true;
    setFilters(newState);
  };
  const filterChangeHandler = (e, dataIndex) => {
    const newState = { ...filters };
    newState[dataIndex] = e.target.value;
    setFilters(newState);
  };

  useEffect(() => {
    if (filters.searchTrigger) setQueryFilters(localStateToQuery(filters));
  }, [filters]);

  useEffect(() => {
    const newState = { ...filters };
    newState.searchTrigger = false;
    setFilters(newState);
  }, [queryFilters]);

  const paginate = (page) => {
    const newState = { ...filters };
    newState.skip = (page - 1) * 10;
    newState.searchTrigger = true;
    setFilters(newState);
  };

  const { loading, data, error } = useQuery(TIMESHEETS_QUERY, {
    variables: queryFilters,
  });
  let timesheets = null;
  let total = null;
  if (data) {
    timesheets = data.timesheets.map((ts) => ({
      timesheetID: ts.id,
      placementID: ts.placementID,
      date: `${ts.month}/${ts.year}`,
      title: ts.title,
      candidate: toolTiped(ts.candidate),
      client: toolTiped(ts.client),
      status: ts.status,
      statusModifier: toolTiped(ts.statusMaker),
      navigate: ts.id,
    }));
    console.log("timesheets", timesheets);
    total = data.timesheets && data.timesheets[0] && data.timesheets[0].total;
  }

  const innerSearch = (dataIndex) => ({
    filterDropdown: () => {
      return (
        <div style={{ padding: 8 }}>
          <Input
            placeholder={`Search by ${dataIndex}`}
            value={filters[dataIndex]}
            onChange={(e) => filterChangeHandler(e, dataIndex)}
            onPressEnter={searchQuery}
            style={{ marginBottom: 10, display: "flex" }}
          />
          <Space>
            <Button
              type="primary"
              onClick={searchQuery}
              icon={<SearchOutlined />}
              size="small"
              style={{ width: 80 }}
            >
              Search
            </Button>
            <Button
              onClick={() => clearSearchQueryItem(dataIndex)}
              size="small"
              style={{ width: 80 }}
            >
              Reset
            </Button>
            <Button
              onClick={() => clearSearchQuery()}
              size="small"
              style={{ width: 80 }}
            >
              Reset All
            </Button>
          </Space>
        </div>
      );
    },
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    render: (text) => text,
  });

  const columns = [
    {
      title: "Timesheet ID",
      dataIndex: "timesheetID",
      key: "timesheetID",
      fixed: "left",
      ...innerSearch("timesheetID"),
    },
    {
      title: "Placement ID",
      dataIndex: "placementID",
      key: "placementID",
      fixed: "left",
      ...innerSearch("placementID"),
    },
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
      ...innerSearch("date"),
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Candidate",
      dataIndex: "candidate",
      key: "candidate",
      ...innerSearch("candidateID"),
    },

    {
      title: "Client",
      dataIndex: "client",
      key: "client",
      ...innerSearch("clientID"),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      fixed: "right",
      render: (passedStatus) => (
        <Tag color={tagColors[passedStatus]} key={passedStatus}>
          {passedStatus}
        </Tag>
      ),
    },
    {
      title: "last modifier",
      dataIndex: "statusModifier",
      key: "statusModifier",
    },
    {
      title: "Action",
      dataIndex: "navigate",
      key: "navigate",
      fixed: "right",
      render: (navigate) => (
        <Link to={`timesheets/${navigate}`}>
          <SettingFilled />
        </Link>
      ),
    },
  ];
  return (
    <Table
      columns={columns}
      dataSource={!error && timesheets}
      scroll
      size="small"
      pagination={{
        onChange: paginate,
        total,
      }}
      loading={loading}
    />
  );
}

TableComponents.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  status: PropTypes.string,
};

TableComponents.defaultProps = {
  status: null,
};

export default withRouter(TableComponents);
