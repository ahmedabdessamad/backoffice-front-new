/* const actions = {
  SET_CHOICES: "SET_CHOICES",

  setChoices: (payload) => ({
    type: actions.SET_CHOICES,
    payload,
  }),
};

export default actions; */

const actions = {
  SET_CHOICES: "SET_CHOICES",
  SET_INPUT_LIST: "INPUT_LIST",
  ADD_INPUT_LIST: "ADD_INPUT_LIST",
  setChoices: (payload) => ({
    type: actions.SET_CHOICES,
    payload,
  }),
  addInputList: (payload) => ({
    type: actions.ADD_INPUT_LIST,
    payload,
  }),
  setInputList: (payload) => ({
    type: actions.SET_INPUT_LIST,
    payload,
  }),
};

export default actions;
