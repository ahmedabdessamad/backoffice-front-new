import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Upload, Button, message } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/react-hooks";
import { ADD_CANDIDATE_FILE } from "./mutations";
import { SENDOUT_DETAILS } from "./queries";

export default function () {
  const { sendoutId } = useParams();
  const [fileList, setFileList] = useState([]);

  const [addCandidateFile, { loading }] = useMutation(ADD_CANDIDATE_FILE, {
    onCompleted: () => {
      message.success("File Added");
      setFileList([]);
    },
    onError: () => message.error("Oops! Something went wrong"),
    refetchQueries: [{ query: SENDOUT_DETAILS, variables: { id: sendoutId } }],
  });

  const handleChange = (info) => {
    let files = [...info.fileList];
    files = files.slice(-1).map((f) => f.originFileObj);

    setFileList(files);
  };

  return (
    <div>
      <Upload
        multiple={false}
        fileList={fileList}
        accept=".pdf,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        onChange={handleChange}
        beforeUpload={() => false}
      >
        <Button>
          <UploadOutlined /> Add New File
        </Button>
      </Upload>
      {!!fileList.length && (
        <Button
          type="primary"
          disabled={!fileList.length}
          loading={loading}
          style={{ marginTop: 16 }}
          onClick={() => {
            addCandidateFile({
              variables: {
                candidateID: 111623,
                file: fileList[0],
              },
            });
          }}
        >
          {loading ? "Uploading" : "Upload File"}
        </Button>
      )}
    </div>
  );
}
