import React, { useEffect } from "react";
import { Layout as AntLayout } from "antd";
import { useDispatch } from "react-redux";
import { useQuery } from "@apollo/react-hooks";
import Spinner from "../../components/Spinner/Spinner";
import appActions from "../../redux/app/actions";
import authActions from "../../redux/auth/actions";

import useWindowSize from "../../hooks/useWindowSize";
import Header from "../Header/Header";
import Sider from "../Sider/Sider";
import { FETCH_USER_QUERY } from "./queries";
import LayoutRouter from "./LayoutRouter";
import classes from "./layout.module.scss";

const { Content, Footer } = AntLayout;

const { toggleOnWindowSizeChange } = appActions;
const { setUser } = authActions;

export default function Layout() {
  const [{ width }] = useWindowSize();
  const dispatch = useDispatch();

  const { loading, error, data } = useQuery(FETCH_USER_QUERY, {
    fetchPolicy: "network-only",
    pollInterval: 60000,
  });

  useEffect(() => {
    dispatch(toggleOnWindowSizeChange({ width }));
  }, [width, dispatch]);

  if (loading) return <Spinner />;
  if (error) return <p>Error...</p>;
  if (data) dispatch(setUser(data.me));

  return (
    <AntLayout className={classes.layout}>
      <Header />
      <AntLayout>
        <Sider />
        <AntLayout>
          <Content className={classes.mainContent}>
            <LayoutRouter />
          </Content>
          <Footer className={classes.footer} />
        </AntLayout>
      </AntLayout>
    </AntLayout>
  );
}
