/* eslint-disable array-callback-return */
import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import {
  Form,
  Input,
  Button,
  Typography,
  Card,
  Alert,
  Select,
  message,
} from "antd";
import { FlagOutlined, LoadingOutlined } from "@ant-design/icons";
import * as PropTypes from "prop-types";
import { useSelector } from "react-redux";
import classes from "./AddMarketInfo.module.scss";
import { UPDATE_MARKETINFO_MUTATION } from "./utile/mutations";
import {
  FETCH_ONE_MARKETINFO_QUERY,
  FETCH_MARKETINFO_QUERY,
  GET_INDUSTRY_QUERY,
  GET_COMPANY_QUERY,
  GET_CATEGORIE_QUERY,
} from "./utile/queries";
import checkSpecificPermission from "../../helpers/authorization/specificPermissionCheckHelpers";


const { Title } = Typography;

function EditMarketInfo(props) {
  const user = useSelector((state) => state.AuthReducer.user);
  const [id, setId] = useState("");
  const [company1, setCompany1] = useState("");
  const [jobTitle1, setJobTitle1] = useState("");
  const [contact1, setContact1] = useState("");
  const [email1, setEmail1] = useState("");
  const [phone1, setPhone1] = useState("");
  const [industry1, setIndustry1] = useState([{}]);
  const [skills1, setSkills1] = useState([{}]);
  const [description1, setDescription1] = useState("");


  // eslint-disable-next-line no-unused-vars
  const [errors, setErrors] = useState({});
  const { history, match } = props;
  const marketInfoIDParam = match.params.id;

  const { data: dataCatego } = useQuery(GET_CATEGORIE_QUERY);
  let categoriesList = null;
  const tab2 = [];
  if (dataCatego) {
    categoriesList = dataCatego.categories.map((catego) => catego.name);

    categoriesList.map((catego) => {
      tab2.push({ value: catego });
    });
  }

  const { data: dataComp } = useQuery(GET_COMPANY_QUERY);
  let companyList = null;
  const tab1 = [];
  if (dataComp) {
    companyList = dataComp.clientCorporations.map((comp) => comp.name);

    companyList.map((comp) => {
      tab1.push({ value: comp });
    });
  }
  const { data: dataI } = useQuery(GET_INDUSTRY_QUERY);
  let industryList = null;
  const tab = [];
  if (dataI) {
    industryList = dataI.businessSectors.map((sector) => sector.name);

    industryList.map((sector) => {
      tab.push({ value: sector });
    });
  }

  const { loading, data, error } = useQuery(FETCH_ONE_MARKETINFO_QUERY, {
    variables: {
      marketInfoID: marketInfoIDParam,
    },
  });
  if (data) {
    const {
      marketInfoID,
      companyName,
      jobTitle,
      contact,
      email,
      phone,
      industry,
      categories,
      description,
      creator,
    } = data.marketInfo;
    console.log(
      "creator.id",
      checkSpecificPermission({ MARKETINFO: 6 }, user, creator.userID)
    );
    if (!checkSpecificPermission({ MARKETINFO: 6 }, user, creator.userID) || error) {
      history.push(`/home/marketInfo`);
    } else if (!id) {
      setId(marketInfoID);
      setCompany1(companyName);
      setJobTitle1(jobTitle);
      setContact1(contact);
      setEmail1(email);
      setPhone1(phone);

      const tabIdustry = [];
      for (let i = 0; i < industry.length; i += 1) {
        tabIdustry.push({ name: industry[i].name });
      }
      setIndustry1(tabIdustry);

      const tabCategorie = [];
      for (let i = 0; i < categories.length; i += 1) {
        tabCategorie.push({ name: categories[i].name });
      }
      setSkills1(tabCategorie);

      setDescription1(description);
    }
  }
  const valideCompany = (rule, value) => {
    if (value) {
      if (value.length < 2 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };

  const valideContact = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const validePhone = (rule, value) => {
    if (value) {
      if (value.length < 8 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };

  const validejobTitle = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const valideDescription = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const changecompanyHandler = (value) => {
    setCompany1(value[0]);
  };
  const changeContactHandler = (event) => {
    setContact1(event.target.value);
  };
  const changeEmailHandler = (event) => {
    setEmail1(event.target.value);
  };
  const changeskillsHandler = (value) => {
    const tabCategories = [];
    for (let i = 0; i < value.length; i += 1) {
      tabCategories.push({ name: value[i] });
    }
    setSkills1(tabCategories);
  };
  const changejobTitleHandler = (event) => {
    setJobTitle1(event.target.value);
  };
  const changePhoneHandler = (event) => {
    setPhone1(event.target.value);
  };
  const changeIndustryHandler = (value) => {
    const tabIdustry = [];
    for (let i = 0; i < value.length; i += 1) {
      tabIdustry.push({ name: value[i] });
    }
    setIndustry1(tabIdustry);
  };
  const changeDescriptionHandler = (event) => {
    setDescription1(event.target.value);
  };
  const success = () => {
    message.success("Market Info updated successfully");
  };
  const [editMarketInfo] = useMutation(UPDATE_MARKETINFO_MUTATION, {
    variables: {
      marketInfoID: id,
      input: {
        company: company1,
        jobTitle: jobTitle1,
        contact: contact1,
        email: email1,
        phone: phone1,
        industry: industry1,
        categories: skills1,
        description: description1,
      },
    },
    refetchQueries: () => [{ query: FETCH_MARKETINFO_QUERY }],
    onCompleted() {
      history.push("/home/marketInfo");
      success();
    },
    onError(err) {
      setErrors(err.graphQLErrors[0].message);
    },
  });

  const onFinish = () => {
    editMarketInfo();
  };
  function onBlur() {}

  function onFocus() {}

  function onSearch() {}
  return (
    <div>
      <Card className={classes.card}>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Edit Market Info
          </Title>
          <FlagOutlined className={classes.icon}> </FlagOutlined>
        </div>
        <br />
        <br />
        {data ? (
          <Form name="nest-messages" onFinish={onFinish}>
            <Form.Item name="company1" label="Company">
              {/* <Select
                showSearch
                style={{ width: "100%" }}
                optionFilterProp="children"
                onChange={changecompanyHandler}
                defaultValue={company1}
                onFocus={onFocus}
                onBlur={onBlur}
                onSearch={onSearch}
                options={tab1}
                filterOption={(input, options) =>
                  options.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              /> */}
              <Select
                mode="tags"
                style={{ width: "100%" }}
                tokenSeparators={[","]}
                onChange={changecompanyHandler}
                defaultValue={company1}
                options={tab1}
                filterOption={(input, options) =>
                  options.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              />
            </Form.Item>
            <Form.Item name="industry1" label="Industry">
              <Select
                mode="multiple"
                style={{ width: "100%" }}
                onChange={changeIndustryHandler}
                defaultValue={industry1.map(({ name }) => name)}
                optionLabelProp="label"
                options={tab}
              />
            </Form.Item>
            <Form.Item
              name="contact1"
              label="Candidate ID"
              rules={[
                {
                  validator: valideContact,
                },
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="contact1"
                onChange={changeContactHandler}
                name="contact1"
                value={contact1}
                defaultValue={contact1}
              />
            </Form.Item>
            <Form.Item
              name="jobTitle1"
              label="JobTitle"
              rules={[
                {
                  validator: validejobTitle,
                },
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="jobTitle1"
                onChange={changejobTitleHandler}
                name="jobTitle1"
                value={jobTitle1}
                defaultValue={jobTitle1}
              />
            </Form.Item>

            <Form.Item
              name="email1"
              label="Email"
              rules={[
                {
                  type: "email",
                  message: "Please input the email!",
                },
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="email1"
                onChange={changeEmailHandler}
                name="email1"
                value={email1}
                defaultValue={email1}
              />
            </Form.Item>
            <Form.Item
              name="phone1"
              label="Phone"
              rules={[
                {
                  validator: validePhone,
                },
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="phone1"
                onChange={changePhoneHandler}
                name="phone1"
                value={phone1}
                defaultValue={phone1}
              />
            </Form.Item>

            <Form.Item name="skills1" label="Community/Technical Stack">
              <Select
                mode="multiple"
                style={{ width: "100%" }}
                onChange={changeskillsHandler}
                defaultValue={skills1.map(({ name }) => name)}
                optionLabelProp="label"
                options={tab2}
              />
            </Form.Item>
            <Form.Item
              name="description1"
              label="Description"
              rules={[
                {
                  validator: valideDescription,
                },
                {
                  max: 3000,
                  message: "Length must be less then 3000 characters!",
                },
              ]}
            >
              <Input
                type="description1"
                onChange={changeDescriptionHandler}
                name="description1"
                value={description1}
                defaultValue={description1}
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Save edit
              </Button>
            </Form.Item>
          </Form>
        ) : null}
        {loading ? <LoadingOutlined className={classes.icon2} /> : null}
        {!loading && !data ? (
          <Alert message="Please check the ID!" type="error" showIcon />
        ) : null}
      </Card>
    </div>
  );
}
EditMarketInfo.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
};
EditMarketInfo.defaultProps = {
  match: () => {},
};
export default withRouter(EditMarketInfo);
