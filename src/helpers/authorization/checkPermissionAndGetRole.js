const UNAUTHORIZED = new Error("UNAUTHORIZED");
const UNAUTHENTICATED = new Error("UNAUTHENTICATED");

const checkPermissionAndGetRole = (requiredPermission, user) => {
  if (!user) throw UNAUTHENTICATED;
  const tabs = user.permissions.map((p) => p.tabs);

  const newTabAdmin = tabs
    .flat()
    .find(
      ({ name, permission }) =>
        name in requiredPermission && (permission & 4) === 4
    );
  if (newTabAdmin) return "Admin";

  const newTabEdit = tabs
    .flat()
    .find(
      ({ name, permission }) =>
        name in requiredPermission && (permission & 2) === 2
    );
  if (newTabEdit) return "Editor";

  const newTabRead = tabs
    .flat()
    .find(
      ({ name, permission }) =>
        name in requiredPermission && (permission & 1) === 1
    );
  if (newTabRead) return "Reader";

  throw UNAUTHORIZED;
};

module.exports = checkPermissionAndGetRole;
