import { gql } from "graphql.macro";

export const editGroupMutation = gql`
  mutation editGroup($id: ID!, $input: groupInput!) {
    editGroup(id: $id, input: $input)
  }
`;
export const deleteGroupMutation = gql`
  mutation deleteGroup($id: ID!) {
    deleteGroup(id: $id)
  }
`;
export const addGroupMutation = gql`
  mutation addGroup($group: String!) {
    addGroup(group: $group) {
      name
    }
  }
`;
