import React from "react";
import { Input, Form } from "antd";

export default ({
  editing,
  dataIndex,
  title,
  record,
  index,
  children,
  rules,
  ...restProps
}) => {
  const inputNode = (
    <Input.TextArea
      onChange={({ target: { value } }) => {
        record && (record[dataIndex] = value);
      }}
      autoSize
    />
  );

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
          initialValue={record && record[dataIndex]}
          rules={rules}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};
