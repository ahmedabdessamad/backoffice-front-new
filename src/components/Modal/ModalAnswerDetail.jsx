import React from "react";
import { Modal, Descriptions } from "antd";

function ModalAnswerDetail(form) {
  Modal.info({
    title: "Form Answers",
    content: (
      <Descriptions size="small" column={1}>
        <br />
        <br />

        {form.answers.map((x, i) => {
          console.log("xxxxxxxxxxxxxxx", x);
          return (
            <Descriptions.Item label={x.question}>{x.answer}</Descriptions.Item>
          );
        })}
      </Descriptions>
    ),

    width: "40%",
    onOk() {},
  });
}
export default ModalAnswerDetail;
