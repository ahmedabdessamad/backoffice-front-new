import React from "react";
import * as PropTypes from "prop-types";
import { DatePicker, TextInput, Select, Icon } from "cf-neo-ui";
import moment from "moment";
import classes from "./Line.module.scss";

function Line(props) {
  function disableDates(day) {
    const { month, year } = props;
    const iterator = moment(day);
    return iterator.month() !== month || iterator.year() !== year;
  }

  const { specialHour, month, year } = props;
  const date = specialHour.date
    ? moment(specialHour.date).format("DD/MM/YYYY")
    : `01/${month}/${year}`;
  return (
    <div className={classes.container}>
      <span className={classes.deleteItem}>
        <Icon
          type="chevron-right"
          color="#8d0417"
          color2="#d3354a"
          width={20}
          height={20}
        />
      </span>
      <span className={classes.selectItem}>
        <Select
          className={classes.modifySelect}
          size="small"
          options={{
            groupA: [
              {
                text: "option 1",
                value: "0",
                selected: specialHour.type === "0",
              },
              {
                text: "option 2",
                value: "1",
                selected: specialHour.type === "1",
              },
            ],
          }}
          placeholder="type"
          disabled
        />
      </span>
      <span className={classes.datePickerItem}>
        <DatePicker
          size="small"
          className={classes.modifyDatePicker}
          dateFormat="DD/MM/YYYY"
          defaultDate={date}
          disabledDaysMatcher={(day) => disableDates(day)}
          locale="fr"
          acceptWhenOutsideClick
          disabled
        />
      </span>
      <span className={classes.label}>h:</span>
      <span className={classes.numberItem}>
        <TextInput
          type="number"
          max="99"
          min="0"
          size="small"
          defaultValue={specialHour.hours && specialHour.hours.toString()}
          placeholder="h"
          className={classes.modifyTextInput}
          disabled
        />
      </span>
      <span className={classes.label}>mn:</span>
      <span className={classes.numberItem}>
        <TextInput
          type="number"
          max="59"
          min="0"
          size="small"
          placeholder="mn"
          defaultValue={specialHour.minutes && specialHour.minutes.toString()}
          className={classes.modifyTextInput}
          disabled
        />
      </span>
    </div>
  );
}

Line.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
  specialHour: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    date: PropTypes.string,
    hours: PropTypes.number,
    minutes: PropTypes.number,
    type: PropTypes.string,
  }).isRequired,
};

export default Line;
