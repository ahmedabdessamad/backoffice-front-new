import { gql } from "graphql.macro";

const TIMESHEETS_QUERY = gql`
  query Timesheets(
    $clientIDs: [Int]
    $candidateIDs: [Int]
    $placementIDs: [Int]
    $limit: Int
    $skip: Int
    $ids: [Int]
    $name: String
    $status: [timesheetSupportedstatus]
    $periodStart: Float
    $periodEnd: Float
    $month: Int
    $year: Int
  ) {
    timesheets(
      filter: {
        clientIDs: $clientIDs
        candidateIDs: $candidateIDs
        placementIDs: $placementIDs
        limit: $limit
        skip: $skip
        ids: $ids
        name: $name
        status: $status
        periodStart: $periodStart
        periodEnd: $periodEnd
        month: $month
        year: $year
      }
    ) {
      id
      title
      client {
        id
        name
      }
      candidate {
        id
        name
      }
      placementID
      month
      year
      status
      statusMaker {
        id
        name
      }
      total
    }
  }
`;

export default TIMESHEETS_QUERY;
