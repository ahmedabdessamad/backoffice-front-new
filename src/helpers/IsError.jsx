/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import { useDispatch } from "react-redux";
// import { Redirect } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import { notification, Badge } from "antd";
import { NotificationOutlined } from "@ant-design/icons";
import { gql } from "graphql.macro";
import xeroLoginActions from "../redux/xeroLogin/actions";

const GET_EXPIRED_TOKEN = gql`
  query getExpiredToken {
    getExpiredToken
  }
`;
const { xeroLogin } = xeroLoginActions;
function IsError() {
  const dispatch = useDispatch();
  const { loading, data } = useQuery(GET_EXPIRED_TOKEN);

  if (loading) return <p>...loading</p>;

  if (data) dispatch(xeroLogin(data.getExpiredToken));
  const openNotification = () => {
    const description = (
      <div>
        {"your xero access token is expired, please"}
        {<a href="/home/xeroLogin"> Login Again</a>}
      </div>
    );

    const args = {
      message: "Xero Login",
      // description: `your xero access token is expired, please login again to get a new access token`,
      description,
    };

    notification.open(args);
  };
  const exists = data ? data.getExpiredToken : false;
  return (
    exists && (
      <div style={{ position: "fixed", right: 100, top: 10 }}>
        <a
          role="button"
          tabIndex={0}
          onClick={openNotification}
          onKeyDown={openNotification}
        >
          <Badge dot>
            <NotificationOutlined />
          </Badge>
        </a>
      </div>
    )
  );
}
export default IsError;
