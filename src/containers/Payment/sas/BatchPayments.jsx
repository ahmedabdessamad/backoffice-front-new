import React, { useState, useEffect } from "react";
import { useLocation, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { useQuery } from "@apollo/react-hooks";
import { message } from "antd";
import queryString from "query-string";
import Spinner from "../../../components/Spinner/Spinner";
import ContentWrapper from "../../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../../components/ContentContainer/ContentContainer";
import { VERIF_DATA } from "../queries";
import XeroLogin from "../XeroLogin";
import BatchPaymentTable from "../BatchPayment-Components/BatchPaymentTable";

function BatchPayments() {
  const Location = useLocation();
  const [validationState] = useState(queryString.parse(Location.search));

  const { loading: vloading, error: verror, data: vdata } = useQuery(
    VERIF_DATA
  );

  useEffect(() => {
    if (validationState.status === "success") {
      message.success("Batch payment paid!");
    }

    if (validationState.status === "failed") {
      message.error("Batch payment failed!");
    }
  }, [validationState]);

  if (vloading) return <Spinner />;
  if (verror) return <p>Error...</p>;

  const props = {
    title: "Pending batchPayments (SAS)",
    component: "batchPayments",
    validationStatus: ["PENDING"],
    organisation: ["Club Freelance SAS"],
  };
  return (
    <ContentWrapper>
      <ContentContainer>
        <div>
          {vdata.verifData === false ? (
            <XeroLogin />
          ) : (
            <div>
              <BatchPaymentTable {...props} />
            </div>
          )}
        </div>
      </ContentContainer>
    </ContentWrapper>
  );
}
BatchPayments.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(BatchPayments);
