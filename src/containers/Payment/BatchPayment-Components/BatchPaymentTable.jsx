import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import PropTypes, { string } from "prop-types";
import { useSelector } from "react-redux";
import { Link, useLocation, withRouter } from "react-router-dom";
import { Table, Tag, Input, Button, Alert } from "antd";
import moment from "moment";
import { CheckCircleOutlined, CloseCircleOutlined } from "@ant-design/icons";
import { GET_BATCHPAYMENTS_BY_FILTER, GET_LAST_UPDATE } from "../queries";
import Spinner from "../../../components/Spinner/Spinner";
import classes from "../../User/user.module.scss";

function BatchPaymentsTable(props) {
  const Location = useLocation();
  const { xeroToken } = useSelector((state) => state.xeroLoginReducer);
  // console.log("xero token:", xeroToken);
  const limit = 5;
  let columns;
  const { component, validationStatus, title, organisation } = props;
  const { Search } = Input;
  const [status, setStatus] = useState(null);
  const [type, setType] = useState(null);
  const [batchPaymentID, setBatchPaymentID] = useState(null);
  const [page, setPage] = useState(1);

  const { loading, error, data } = useQuery(GET_BATCHPAYMENTS_BY_FILTER, {
    variables: {
      input: {
        batchPaymentID,
        validationStatus,
        organisation,
        status,
        type,
      },
    },
    fetchPolicy: "cache-and-network",
  });
  const { data: msg, msgLoading } = useQuery(GET_LAST_UPDATE);
  const msgToDisplay = msg ? msg.getLastUpdate : "";

  if (loading || msgLoading) return <Spinner />;
  if (error) return <p>Error...</p>;

  const bData = data ? data.getBatchPaymentByFilter : [];
  if (component === "logs") {
    columns = [
      {
        title: "batchPayment_ID",
        dataIndex: "batchPaymentID",
        key: "batchPaymentID",
        fixed: "left",
      },
      {
        title: "Payment_Date",
        dataIndex: "dateString",
        key: "dateString",
        defaultSortOrder: "descend",
        sorter: (a, b) => a.date - b.date,
        render: (text) => moment(text).format("YYYY-MM-DD"),
      },
      {
        title: "Status",
        dataIndex: "status",
        key: "status",
        filters: [
          { text: "AUTHORISED", value: "AUTHORISED" },
          { text: "DELETED", value: "DELETED" },
        ],
        filteredValue: status,
        onFilter: (value, record) => record.status.includes(value),
        render: (text) => {
          if (text === "AUTHORISED") {
            return (
              <Tag icon={<CheckCircleOutlined />} color="success">
                {text}
              </Tag>
            );
          }
          if (text === "DELETED") {
            return (
              <Tag icon={<CloseCircleOutlined />} color="error">
                {text}
              </Tag>
            );
          }
          return text;
        },
      },
      {
        title: "Type",
        dataIndex: "type",
        key: "type",
        filters: [
          { text: "RECBATCH", value: "RECBATCH" },
          { text: "PAYBATCH", value: "PAYBATCH" },
        ],
        filteredValue: type,
        onFilter: (value, record) => record.type.includes(value),
        render: (text) => {
          if (text === "RECBATCH") {
            return <Tag color="processing">{text}</Tag>;
          }
          if (text === "PAYBATCH") {
            return <Tag color="gold">{text}</Tag>;
          }
          return text;
        },
      },
      {
        title: "Validation_Status",
        dataIndex: "validationStatus",
        key: "validationStatus",
        filters: [
          { text: "EXPORTED", value: "EXPORTED" },
          { text: "REJECTED", value: "REJECTED" },
          { text: "PAID", value: "PAID" },
        ],
        // filteredValue: validationStatus || null,
        onFilter: (value, record) => record.validationStatus.includes(value),
        render: (text) => {
          if (text === "PAID") {
            return (
              <Tag color="success" icon={<CheckCircleOutlined />}>
                {text}
              </Tag>
            );
          }

          if (text === "REJECTED") {
            return (
              <Tag icon={<CloseCircleOutlined />} color="error">
                {text}
              </Tag>
            );
          }
          if (text === "EXPORTED") {
            return (
              <Tag icon={<CheckCircleOutlined />} color="purple">
                {text}
              </Tag>
            );
          }
          return text;
        },
      },
      {
        title: "action_Date",
        dataIndex: "updatedAt",
        key: "updatedAt",
        defaultSortOrder: "descend",
        sorter: (a, b) => a.updatedAt - b.updatedAt,
        render: (text) => moment(text).format("YYYY-MM-DD"),
      },
      {
        title: "Action",
        dataIndex: "Action",
        key: "Action",
        fixed: "right",
        render: (text, record) => (
          <span>
            <Link
              to={`${Location.pathname}/${record.batchPaymentID}`}
            >
              <a style={{ marginRight: 16 }}> payments Details</a>
            </Link>
          </span>
        ),
      },
    ];
  }
  if (component === "batchPayments")
    columns = [
      {
        title: "batchPayment_ID",
        dataIndex: "batchPaymentID",
        key: "batchPaymentID",
        fixed: "left",
      },
      {
        title: "Payment_Date",
        dataIndex: "dateString",
        key: "dateString",
        defaultSortOrder: "descend",
        sorter: (a, b) => a.date - b.date,
        render: (text) => moment(text).format("YYYY-MM-DD"),
      },
      {
        title: "Status",
        dataIndex: "status",
        key: "status",
        filters: [
          { text: "AUTHORISED", value: "AUTHORISED" },
          { text: "DELETED", value: "DELETED" },
        ],
        filteredValue: status,
        onFilter: (value, record) => record.status.includes(value),
        render: (text) => {
          if (text === "AUTHORISED") {
            return (
              <Tag icon={<CheckCircleOutlined />} color="success">
                {text}
              </Tag>
            );
          }
          if (text === "DELETED") {
            return (
              <Tag icon={<CloseCircleOutlined />} color="error">
                {text}
              </Tag>
            );
          }
          return text;
        },
      },
      {
        title: "Type",
        dataIndex: "type",
        key: "type",
        filters: [
          { text: "RECBATCH", value: "RECBATCH" },
          { text: "PAYBATCH", value: "PAYBATCH" },
        ],
        filteredValue: type,
        onFilter: (value, record) => record.type.includes(value),
        render: (text) => {
          if (text === "RECBATCH") {
            return <Tag color="processing">{text}</Tag>;
          }
          if (text === "PAYBATCH") {
            return <Tag color="gold">{text}</Tag>;
          }
          return text;
        },
      },

      {
        title: "Action",
        dataIndex: "Action",
        key: "Action",
        fixed: "right",
        width: 200,
        render: (text, record) => (
          <span>
            <Link
              to={`${Location.pathname}/${record.batchPaymentID}`}
            >
              <a style={{ marginRight: 16 }}> payments Details</a>
            </Link>
          </span>
        ),
      },
    ];

  const changeHandler = () => setPage(page);
  const handleColumnChange = (filters) => {
    // console.log("filters=", filters);
    setStatus(filters.status);
    setType(filters.type);
  };
  const handleSearch = (filters) =>
    filters && setBatchPaymentID(filters.split(","));

  const onReset = () => {
    setStatus(null);
    setType(null);
    setBatchPaymentID(null);
  };

  return (
    <div>
      <div
        style={{
          width: 450,
          display: "flex",
          marginTop: 0,
          marginLeft: "65%",
          marginRight: 5,
        }}
      >
        {xeroToken ? (
          <Alert
            message={
              <div>
                {`Last synchronisation with xero:${moment(
                  Number(msgToDisplay)
                ).format("YYYY-MM-DD")}, your xero acces token is expired `}
                {<a href="/home/xeroLogin"> Login Again</a>}
              </div>
            }
            type="info"
            showIcon
          />
        ) : (
          <Alert
            message={`Last synchronisation with xero:${moment(
              Number(msgToDisplay)
            ).format("YYYY-MM-DD")}`}
            type="info"
            showIcon
          />
        )}
      </div>
      <div className={classes.header}>
        <h1 style={{ textAlign: "center", fontSize: "large" }}>{title}</h1>

        <Search
          placeholder="Search by batchPaymentID"
          style={{ width: 300, float: "left" }}
          onSearch={handleSearch}
          // onChange={handleChange}
        />
        <Button type="primary" onClick={onReset} style={{ width: 90 }}>
          reset
        </Button>
      </div>
      <Table
        dataSource={bData}
        columns={columns}
        scroll={{ x: 1500, y: 300 }}
        onChange={(pagination, filters) => handleColumnChange(filters)}
        pagination={{
          defaultCurrent: 1,
          total: Math.ceil(bData / limit),
          onchange: { changeHandler },
          pageSize: limit,
        }}
      />
    </div>
  );
}
BatchPaymentsTable.propTypes = {
  title: PropTypes.string.isRequired,
  component: PropTypes.string.isRequired,
  validationStatus: PropTypes.arrayOf(string).isRequired,
  organisation: PropTypes.arrayOf(string).isRequired,
};
export default withRouter(BatchPaymentsTable);
