import { gql } from "graphql.macro";

const CONVERSATION = gql`
  query TimesheetConversation($timesheetID: Int!) {
    timesheetConversation(timesheetID: $timesheetID) {
      id
      client {
        id
        firstName
        lastName
      }
      candidate {
        id
        firstName
        lastName
      }
      timesheetID
      dateAdded
      dateLastUpdate
      messages {
        id
        text
        date
        speaker
      }
      comment
    }
  }
`;

export default CONVERSATION;
