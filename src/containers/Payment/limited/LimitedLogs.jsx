import React from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import BatchPaymentTable from "../BatchPayment-Components/BatchPaymentTable";
import ContentWrapper from "../../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../../components/ContentContainer/ContentContainer";

function LimitedLogs() {
  const props = {
    title: "Logs",
    validationStatus: ["REJECTED", "PAID", "EXPORTED"],
    component: "logs",
    organisation: ["Club Freelance Limited"],
  };
  return (
    <ContentWrapper>
      <ContentContainer>
        <div>
          <BatchPaymentTable {...props} />
        </div>
      </ContentContainer>
    </ContentWrapper>
  );
}
LimitedLogs.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(LimitedLogs);
