import React, { useEffect, useState } from "react";
import * as PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import { Typography, Tabs, Table, Button, message } from "antd";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { SettingFilled } from "@ant-design/icons";
import GET_LOGS from "./query";
import { SEND_TIMESHEET_EMAIL } from "../Mailing/mutations";
import classes from "./Logs.module.scss";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import localStateToQuery from "./localStateToQuery";

const { Title } = Typography;

function Logs() {
  const BhUrl = window.location.href;
  const split1 = BhUrl.split("timesheets/");
  const inTimesheets = split1[1] === "logs";

  let initialFilters = {};
  if (inTimesheets) {
    initialFilters = {
      limit: 10,
      skip: 0,
      startDate: null,
      endDate: null,
      sources: [],
      services: ["TIMESHEET"],
    };
  } else {
    initialFilters = {
      limit: 10,
      skip: 0,
      startDate: null,
      endDate: null,
      sources: [],
      services: [ "accounts", "ACCOUNTS", "TIMESHEET"],
    };
  }

  const [filters, setFilters] = useState({});
  const [queryFilters, setQueryFilters] = useState(
    localStateToQuery(initialFilters)
  );

  useEffect(() => {
    if (filters.searchTrigger) setQueryFilters(localStateToQuery(filters));
  }, [filters]);

  useEffect(() => {
    const newState = { ...filters };
    newState.searchTrigger = false;
    setFilters(newState);
  }, [queryFilters]);

  const paginate = (page, pageSize) => {
    const newState = { ...filters };
    newState.skip = (page - 1) * pageSize;
    newState.limit = pageSize;
    newState.searchTrigger = true;
    newState.services = [ "accounts", "ACCOUNTS", "TIMESHEET"];
    if (inTimesheets) newState.services = ["TIMESHEET"];
    setFilters(newState);
  };

  // const [period, setPeriod] = useState(null);
  // const [searchText, setSearchText] = useState("");
  // const [searchedColumn, setSearchedColumn] = useState("");
  // set up url

  const { loading, data, error } = useQuery(GET_LOGS, {
    variables: queryFilters,
    fetchPolicy: "network-only",
  });

  const successResendEmail = () => {
    message.success("Email was resend successfully");
  };

  const [sendTimesheetEmail] = useMutation(SEND_TIMESHEET_EMAIL, {
    onCompleted() {
      successResendEmail();
    },
    onError(e) {
      console.log("error", e);
    },
  });

  const buttonAction = (
    button,
    service,
    action,
    placementID,
    month,
    year,
    status
  ) => {
    if (button === "Resend Email") {
      console.log("in button ", placementID, month, year);
      if (status === "open") action = "opened";
      if (status === "pending") action = "submitted";
      if (status === "approved") action = "approved";
      if (status === "rejected") action = "rejected";
      sendTimesheetEmail({
        variables: {
          service,
          action,
          placementId: placementID,
          month,
          year,
        },
      });
    }
  };



  const viewTimesheetAction = (
    placementID,
    month,
    year
  ) => {

  };

  const creatComment = (service, action, metadata) => {
    if (service === "TIMESHEET") {
      if (action === "CREATED") {
        return `Timesheet of the month ${metadata.month}/${metadata.year} has been opened for placement ${metadata.placementID}`;
      }
      if (action === "UPDATED") {
        if (metadata.status === "pending") {
          return `Timesheet submitted by the candidate ${metadata.statusMaker}`;
        }
        if (metadata.status === "rejected") {
          return `Timesheet "${metadata.title}" of candidate ${metadata.candidateID} rejected by the client`;
        }
        if (metadata.status === "approved") {
          return `Timesheet "${metadata.title}" of candidate ${metadata.candidateID} approved by the client`;
        }
      }
    } else if (
      service === "timesheets" ||
      service === "accounts" ||
      service === "bot_notifs"
    ) {
      return `Email has been sent to ${metadata.mailTo.email} with the subject "${metadata.subject}"`;
    } else if (service === "ACCOUNTS") {
      if (action === "SIGN_UP") {
        return ` "${metadata.name}" signed up in Club Freelance website ${metadata.cfWebsite}`;
      }
      if (action === "RESET_PASSWORD" || action === "RESET_PASSWORD_REQUEST") {
        return ` Password has been reset for ${metadata.name}`;
      }
    }
  };

  let logs = null;
  let total = null;
  if (data && data.getLogs && data.getLogs.logs) {
    /* logs = data.getLogs.logs.map((l) => ({
      date: new Date(l.createdAt).toLocaleDateString("fr-FR"),
      type: l.action,
      category: l.category,
      comment: l.comment,
      user:
        l.relatedConsultant && l.relatedClient
          ? `consultant: ${l.relatedConsultant.firstName} ${l.relatedConsultant.lastName} / client: ${l.relatedClient.firstName} ${l.relatedClient.lastName} `
          : "",
    })); */
    logs = data.getLogs.logs.map((l) => ({
      date: new Date(l.dateAdded).toLocaleDateString("fr-FR"),
      category:
        l.service === "TIMESHEET" || l.service === "timesheets"
          ? "Timesheet"
          : l.service === "accounts" || l.service === "ACCOUNTS"
          ? "Account"
          : l.service === "bot_notifs"
          ? "Email"
          : l.service,
      comment: creatComment(l.service, l.action, l.metadata),
      status: l.metadata.status,
      action: l.action,
      placementID: l.metadata.placementID,
      month: l.metadata.month,
      year: l.metadata.year,
      service: l.service,
      idTimesheet: l.metadata.id,
    }));
    total = data.getLogs && data.getLogs.total;
  }

  const columns = [
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
      width: 30,
      fixed: "left",
    },
    {
      title: "Category",
      width: 30,
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Comment",
      dataIndex: "comment",
      key: "comment",
      width: 190,
    },
    {
      key: "action",
      fixed: "right",
      width: 80,
      render: (_, l) => {
        let button = "";
        if (
          l.status === "open" ||
          l.status === "pending" ||
          l.status === "approved" ||
          l.status === "rejected"
        )
          button = "Resend Email";
        console.log("action", l, "button", button);
        return (
          <div>
            <Button
              onClick={() =>
                buttonAction(
                  button,
                  l.service,
                  l.action,
                  l.placementID,
                  l.month,
                  l.year,
                  l.status
                )
              }
              type="link"
            >
              {button}
            </Button>
            {l.category === "Timesheet" && inTimesheets ? (

              <Link to={`timesheets/${l.idTimesheet}`}>
            View Timesheet
              </Link>
            ) : null}
            {l.category === "Timesheet" && !inTimesheets ? (

              <Link to={`timesheets/timesheets/${l.idTimesheet}`}>
                View Timesheet
              </Link>
            ) : null}
          </div>
        );
      },
    },
  ];

  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        {!inTimesheets ? (
          <div className={classes.header}>
            <Title level={3} className={classes.title}>
              Logs
            </Title>
          </div>
        ) : null}

        <Table
          columns={columns}
          dataSource={!error && logs}
          scroll={{ y: 450 }}
          sticky
          size="small"
          pagination={{
            onChange: paginate,
            total,
          }}
          loading={loading}
        />
      </ContentContainer>
    </ContentWrapper>
  );
}

Logs.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
};
export default withRouter(Logs);
