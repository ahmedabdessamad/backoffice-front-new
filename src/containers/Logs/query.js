import { gql } from "graphql.macro";

const GET_LOGS = gql`
  query getLogs(
    $limit: Int
    $skip: Int
    $startDate: Float
    $endDate: Float
    $sources: [String]
    $services: [String]
  ) {
    getLogs(
      filter: {
        limit: $limit
        skip: $skip
        startDate: $startDate
        endDate: $endDate
        sources: $sources
        services: $services
      }
    ) {
      logs {
        service
        action
        dateAdded
        metadata {
          status
          placementID
          subject
          mailTo {
            email
            name
          }
          clientID
          candidateID
          statusMaker
          title
          name
          cfWebsite
          month
          year
          id
        }
      }
      total
    }
  }
`;

export default GET_LOGS;
