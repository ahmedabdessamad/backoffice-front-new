import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Popconfirm, Button, Form } from "antd";
import sendoutAction from "../../redux/sendout/actions";
import EditableCell from "../../components/editable-cell/EditableCell";

const { selectReference, editReference, addReference } = sendoutAction;

export default function () {
  const { sendout } = useSelector((state) => state.SendoutReducer);
  const dispatch = useDispatch();

  const [form] = Form.useForm();

  const [editingRow, setEditingRow] = useState("");

  const isEditing = (record) => record.id === editingRow;

  const edit = (record) => {
    form.setFieldsValue(record);
    setEditingRow(record.id);
  };

  const cancel = () => {
    setEditingRow("");
  };

  const save = (record) => {
    form
      .validateFields()
      .then(() => {
        dispatch(editReference(record));
        setEditingRow("");
      })
      .catch((info) => console.log("Validation Failed:", info));
  };

  const handleReferenceSelection = (referencesId) => {
    dispatch(selectReference(referencesId));
  };

  const handleAdd = () => {
    const newRefId = Math.floor(Math.random() * 100000);
    cancel();
    dispatch(addReference(newRefId));
    setEditingRow(newRefId);
  };

  const referenceColumns = [
    {
      title: "#ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Reference First Name",
      key: "referenceFirstName",
      dataIndex: "referenceFirstName",
      editable: true,
    },
    {
      title: "Reference Last Name",
      key: "referenceLastName",
      dataIndex: "referenceLastName",
      editable: true,
    },
    {
      title: "Reference Title",
      dataIndex: "referenceTitle",
      key: "referenceTitle",
      editable: true,
    },
    {
      title: "Outcome",
      dataIndex: "outcome",
      key: "outcome",
      editable: true,
    },
    {
      title: "actions",
      dataIndex: "actions",
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <a
              onClick={() => save(record)}
              style={{
                marginRight: 8,
              }}
            >
              Save
            </a>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
          <a disabled={editingRow !== ""} onClick={() => edit(record)}>
            Edit
          </a>
        );
      },
    },
  ];

  const columns = referenceColumns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
        rules: (col.dataIndex === "referenceFirstName" ||
          col.dataIndex === "referenceLastName") && [
          {
            pattern: RegExp(/^[a-zA-Z][a-zA-Z\s]*$/),
            message: `Invalid ${col.title}`,
          },
        ],
      }),
    };
  });

  return (
    <div>
      <Button
        onClick={handleAdd}
        type="primary"
        style={{
          marginBottom: 16,
        }}
      >
        Add New Reference
      </Button>
      <Form form={form} component={false}>
        <Table
          components={{
            body: {
              cell: EditableCell,
            },
          }}
          rowSelection={{
            type: "checkBox",
            onChange: handleReferenceSelection,
          }}
          rowKey="id"
          columns={columns}
          dataSource={sendout.reference}
          rowClassName="editable-row"
          pagination={false}
        />
      </Form>
    </div>
  );
}
