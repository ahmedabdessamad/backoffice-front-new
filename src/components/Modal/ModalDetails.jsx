import React from "react";
import { Modal, Descriptions } from "antd";

function ModalDetails(missedDeal) {
  const splitDate = missedDeal.missionStart.split("T");
  const startDate = splitDate[0];
  const splitDate2 = missedDeal.missionEnd.split("T");
  const endtDate = splitDate2[0];
  Modal.info({
    title: "Missed deal Info",
    content: (
      <Descriptions size="small" column={1}>
        <br />
        <br />
        <Descriptions.Item label="Company">
          {missedDeal.company}
        </Descriptions.Item>
        <Descriptions.Item label="Candidate">
          {missedDeal.candidate}
        </Descriptions.Item>
        <Descriptions.Item label="Mission start">{startDate}</Descriptions.Item>
        <Descriptions.Item label="Mission end">{endtDate}</Descriptions.Item>
        <Descriptions.Item label="IT consulting firm">
          {missedDeal.ITConsultingFirm}
        </Descriptions.Item>
        <Descriptions.Item label="Intermediary agency">
          {missedDeal.intermedianryAgency}
        </Descriptions.Item>
        <Descriptions.Item label="Job title">
          {missedDeal.jobTitle}
        </Descriptions.Item>
        <Descriptions.Item label="Daily rate">
          {missedDeal.dailyRate}
        </Descriptions.Item>
        <Descriptions.Item label="City">{missedDeal.city}</Descriptions.Item>
        <Descriptions.Item label="Skills">
          {missedDeal.skills.map(({ name }) => name)}
        </Descriptions.Item>
        <Descriptions.Item label="Creator">
          {missedDeal.creator}
        </Descriptions.Item>
        <Descriptions.Item label="Mission project description">
          {missedDeal.missionProjectDescription}
        </Descriptions.Item>
      </Descriptions>
    ),

    width: "40%",
    onOk() {},
  });
}
export default ModalDetails;
