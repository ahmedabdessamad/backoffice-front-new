import React from "react";
import {
  Table,
  Badge,
  Tag,
  message,
  Popconfirm,
  Modal,
  Form,
  Input,
} from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { useQuery, useMutation } from "@apollo/react-hooks";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import Spinner from "../../components/Spinner/Spinner";
import GetUsersNumber from "../User/GetUsersNumber";
import { GET_ROLES } from "./queries";
import { deleteRoleMutation, editRoleMutation } from "./mutations";
import classes from "../User/user.module.scss";
import AddRole from "./AddRole";

export default function Roles() {
  const { Column } = Table;
  const [form] = Form.useForm();
  const [role, setRole] = React.useState("");
  const [visible, setVisible] = React.useState(false);

  const [deleteRole] = useMutation(deleteRoleMutation, {
    onCompleted: () => message.success("Role deleted"),
    refetchQueries: [{ query: GET_ROLES }],
  });

  const [editRole] = useMutation(editRoleMutation, {
    onCompleted: () => message.success("Role updated"),
    refetchQueries: [{ query: GET_ROLES }],
  });

  const { loading, error, data } = useQuery(GET_ROLES);

  if (loading) return <Spinner />;
  if (error) return <p>Error</p>;

  const roleData = data.roles;

  return (
    <div>
      <ContentWrapper style={{ height: "100vh" }}>
        <ContentContainer>
          <h1>Roles </h1>
          <div className={classes.header}>
            <AddRole />
          </div>
          <Table dataSource={roleData} pagination={false}>
            <Column title="roleID" dataIndex="id" key="id" />,
            <Column
              title="roleName"
              dataIndex="name"
              key="name"
              render={(text) => <Tag color="blue">{text}</Tag>}
            />
            <Column
              title="Number of Users"
              key="Number of Users"
              render={(record) => (
                <Badge count={GetUsersNumber(record.id, "ROLE")} />
              )}
            />
            <Column
              title="Action"
              dataIndex="Action"
              key="Action"
              render={(text, record) => (
                <span>
                  <div>
                    <EditOutlined
                      style={{ color: "rgb(26, 192, 198)", marginRight: 16 }}
                      onClick={() => setVisible(true)}
                    />

                    <Modal
                      title="Edit ROLE"
                      visible={visible}
                      onOk={() => {
                        setVisible(false);
                        editRole({
                          variables: {
                            id: record.id,
                            input: { role },
                          },
                        });
                      }}
                      onCancel={() => setVisible(false)}
                    >
                      <Form form={form}>
                        <Form.Item label="Role Name">
                          <Input
                            placeholder="change role Name"
                            onChange={(e) => setRole(e.target.value)}
                          />
                        </Form.Item>
                      </Form>
                    </Modal>
                  </div>

                  <Popconfirm
                    title="Are you sure delete this Role?"
                    onCancel={() => message.error("reset Delete")}
                    okText="Yes"
                    cancelText="No"
                    onConfirm={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                      deleteRole({
                        variables: { id: record.id },
                      });
                    }}
                  >
                    <DeleteOutlined style={{ color: "rgb(178,34,34)" }} />
                  </Popconfirm>
                </span>
              )}
            />
          </Table>
        </ContentContainer>
      </ContentWrapper>
    </div>
  );
}
