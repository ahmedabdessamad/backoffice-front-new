import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Form, Input, Button } from "antd";
import { useMutation } from "@apollo/react-hooks";
import { REQUEST_RESET } from "./mutations";

export default function ForgotPassword() {
  const [requestReset, { called, loading, error }] = useMutation(REQUEST_RESET);
  const [email, setEmail] = useState("");
  // const [submitted, setSubmitted] = useState(false);
  if (called && loading) return <p>...Loading</p>;
  if (error) return <p>Error...</p>;
  return (
    <div>
      <h3>Forgot password ?</h3>
      <Form
        name="normal_login"
        className="login-form"
        onFinish={() => {
          requestReset({
            variables: {
              email,
            },
          });
        }}
      >
        <Form.Item
          name="email"
          rules={[{ required: true, message: "Please input your email!" }]}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        >
          <Input
            // prefix={<UserOutlined className="site-form-item-icon" />}
            name="email"
            placeholder="email"
          />
        </Form.Item>

        <Form.Item>
          <Link to="/Login" href="">
            Back to Sign In
          </Link>
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Reset Password
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
