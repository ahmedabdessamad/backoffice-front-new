import { gql } from "graphql.macro";

const CHANGE_PASSWORD = gql`
  mutation changePassword(
    $email: String!
    $password: String!
    $confirmPassword: String!
  ) {
    changePassword(
      email: $email
      password: $password
      confirmPassword: $confirmPassword
    ) {
      userID
      username
      email
      permission {
        _id
        role {
          roleID
          name
        }
        groupe {
          groupID
          name
        }
      }
      bhID
    }
  }
`;
export default CHANGE_PASSWORD;
