const actions = {
  TOGGLE_PERMISSION_MODAL: "TOGGLE_PERMISSION_MODAL",
  togglePermissionModal: (payload = {}) => ({
    type: actions.TOGGLE_PERMISSION_MODAL,
    payload,
  }),
};

export default actions;
