import React from "react";
import { useSelector } from "react-redux";
import { Layout, Menu } from "antd";
import {
  UserOutlined,
  BankOutlined,
  FilePdfOutlined,
  FieldTimeOutlined,
  AuditOutlined,
  BookOutlined,
  FormOutlined,
  MailOutlined,
  FileTextOutlined,
} from "@ant-design/icons";
import { Link, useRouteMatch } from "react-router-dom";
import { isUserAuthorized } from "../../helpers/authorization/permissionsCheckHelpers";
import "./sider.scss";

const { Sider: AntSider } = Layout;
const { SubMenu } = Menu;

const stripTrailingSlash = (str) => {
  if (str.substr(-1) === "/") {
    return str.substr(0, str.length - 1);
  }
  return str;
};

export default function Sider() {
  const { collapsed } = useSelector((state) => state.AppReducer);
  const { user } = useSelector((state) => state.AuthReducer);
  const match = useRouteMatch();
  const url = stripTrailingSlash(match.url);
  // console.log("user", user);
  return (
    <AntSider className="sider" collapsed={collapsed}>
      <div className="logo">
        <Link to={`${url}`}>
          <input
            type="image"
            src="/logo192.png"
            width="48"
            height="48"
            alt=""
          />
        </Link>
      </div>
      <Menu
        className="sideMenu"
        theme="dark"
        mode="inline"
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={[]}
        inlineCollapsed
      >
        {isUserAuthorized(user, { USERS: 4, PERMISSIONS: 4 }) && (
          <SubMenu
            key="1"
            title={
              <span>
                <UserOutlined />
                <span>User Management</span>
              </span>
            }
          >
            <Menu.Item key="1">
              <Link to={`${url}/users`}>
                <span>Users</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to={`${url}/permissions`}>
                <span>Permissions</span>
              </Link>
            </Menu.Item>
          </SubMenu>
        )}
        {isUserAuthorized(user, { MISSEDDEAL: 7 }) && (
          <Menu.Item key="missedDeal" icon={<AuditOutlined />}>
            <Link to={`${url}/missedDeal`}>
              <span>Missed Deal</span>
            </Link>
          </Menu.Item>
        )}
        {isUserAuthorized(user, { MARKETINFO: 7 }) && (
          <Menu.Item key="marketInfo" icon={<BookOutlined />}>
            <Link to={`${url}/marketInfo`}>
              <span>Market Info</span>
            </Link>
          </Menu.Item>
        )}
        {isUserAuthorized(user, {
        //  HSBCCONNECTOR: 4,
          SAS: 4,
          LIMITED: 4,
        }) && (
          <SubMenu
            key="sub"
            title={
              <span>
                <BankOutlined />
                <span>HSBC Connector</span>
              </span>
            }
          >
            {isUserAuthorized(user, {
              SAS: 4,
            }) && (
              <SubMenu
                key="2"
                title={
                  <span>
                    <span>SAS</span>
                  </span>
                }
              >
                {/* <Menu.Item key="1">
                  <Link to={`${url}/xeroLogin`}>
                    <span>xeroLogin</span>
                  </Link>
              </Menu.Item> */}
                <Menu.Item key="2">
                  <Link to={`${url}/sas_validation`}>
                    <span>Validation</span>
                  </Link>
                </Menu.Item>

                <Menu.Item key="3">
                  <Link to={`${url}/sas_logs`}>
                    <span>Logs</span>
                  </Link>
                </Menu.Item>
              </SubMenu>
            )}
            {isUserAuthorized(user, {
              LIMITED: 4,
            }) && (
              <SubMenu
                key="2"
                title={
                  <span>
                    <span>LIMITED</span>
                  </span>
                }
              >
                {/* <Menu.Item key="1">
                  <Link to={`${url}/xeroLogin`}>
                    <span>xeroLogin</span>
                  </Link>
              </Menu.Item> */}
                <Menu.Item key="2">
                  <Link to={`${url}/limited_validation`}>
                    <span>Validation</span>
                  </Link>
                </Menu.Item>

                <Menu.Item key="3">
                  <Link to={`${url}/limited_logs`}>
                    <span>Logs</span>
                  </Link>
                </Menu.Item>
              </SubMenu>
            )}
          </SubMenu>
        )}
        {isUserAuthorized(user, {
          SENDOUTS: 6,
          PENDINGSENDOUTS: 6,
          ARCHIVEDSENDOUTS: 6,
        }) && (
          <SubMenu
            key="3"
            title={
              <span>
                <FilePdfOutlined />
                <span>Sendouts</span>
              </span>
            }
          >
            {isUserAuthorized(user, {
              PENDINGSENDOUTS: 6,
            }) && (
              <Menu.Item key="1">
                <Link to={`${url}/pending_sendouts`}>
                  <span>Pending Sendouts</span>
                </Link>
              </Menu.Item>
            )}
            {isUserAuthorized(user, {
              ARCHIVEDSENDOUTS: 6,
            }) && (
              <Menu.Item key="2">
                <Link to={`${url}/archived_sendouts`}>
                  <span>Archived Sendouts</span>
                </Link>
              </Menu.Item>
            )}
          </SubMenu>
        )}
        {isUserAuthorized(user, {
          TIMESHEET: 6,
        }) && (
          <Menu.Item key="timesheets" icon={<FieldTimeOutlined />}>
            <Link to={`${url}/timesheets`}>
              <span>Timesheets</span>
            </Link>
          </Menu.Item>
        )}
        {isUserAuthorized(user, {
          FORM: 6,
          FORMLIST: 6,
          EDITFORM: 6,
          FORMTOBESENT: 6,
          FORMANSWERSLIST: 6,
          ONEFORMANSWERS: 6,
        }) && (
          <SubMenu
            key="4"
            title={
              <span>
                <FormOutlined />
                <span> Form </span>
              </span>
            }
          >
            <Menu.Item key="1">
              <Link to={`${url}/form`}>
                <span>Create Form</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to={`${url}/formList`}>
                <span>Form List</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to={`${url}/formAnswersList`}>
                <span>Forms Answers List</span>
              </Link>
            </Menu.Item>
          </SubMenu>
        )}
        {isUserAuthorized(user, { MAILING: 4 }) && (
          <Menu.Item key="emails" icon={<MailOutlined />}>
            <Link to={`${url}/mailing`}>
              <span>Emails</span>
            </Link>
          </Menu.Item>
        )}
        {isUserAuthorized(user, { LOGS: 4 }) && (
          <Menu.Item key="logs" icon={<FileTextOutlined />}>
            <Link to={`${url}/logs`}>
              <span>Logs</span>
            </Link>
          </Menu.Item>
        )}
      </Menu>
    </AntSider>
  );
}
