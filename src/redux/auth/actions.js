const actions = {
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  SET_USER: "SET_USER",
  LOGOUT: "LOGOUT",
  login: (payload) => ({
    type: actions.LOGIN_SUCCESS,
    payload,
  }),
  setUser: (payload) => ({
    type: actions.SET_USER,
    payload,
  }),
  logout: (payload) => ({
    type: actions.LOGOUT,
    payload,
  }),
};

export default actions;
