/* eslint-disable import/prefer-default-export */
import { gql } from "graphql.macro";

export const GET_PERMISSIONS = gql`
  {
    permissions {
      id
      name
      tabs {
        name
        permission
        parent
      }
    }
  }
`;
