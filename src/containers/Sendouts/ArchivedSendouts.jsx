import React from "react";
import { Table, Tag } from "antd";
import { CheckCircleOutlined } from "@ant-design/icons";
import { useQuery } from "@apollo/react-hooks";
import { Link, useRouteMatch } from "react-router-dom";
import { SENDOUTS } from "./queries";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../components/ContentContainer/ContentContainer";

export default function () {
  const match = useRouteMatch();

  const columns = [
    {
      title: "Vacancy",
      key: "vacancy",
      render: (text, record) => `${record.jobOrderID} ${record.jobOrderTitle}`,
    },
    {
      title: "Status",
      dataIndex: "validationStatus",
      key: "validationStatus",
      render: () => (
        <Tag icon={<CheckCircleOutlined />} color="success">
          validated
        </Tag>
      ),
    },
    {
      title: "Candidate",
      key: "candidate",
      render: (text, record) =>
        `${record.candidateID} ${record.candidateFirstName} ${record.candidateLastName}`,
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Link to={`${match.path}/${record.id}`}>
          <span>
            <a>Details</a>
          </span>
        </Link>
      ),
    },
  ];

  const { loading, error, data } = useQuery(SENDOUTS, {
    variables: { validationStatus: ["validated"] },
  });

  if (error) return <p>Error</p>;

  const archivedSendouts = data ? data.sendouts : [];

  return (
    <ContentWrapper>
      <ContentContainer>
        <Table
          columns={columns}
          dataSource={archivedSendouts}
          loading={loading}
        />
      </ContentContainer>
    </ContentWrapper>
  );
}
