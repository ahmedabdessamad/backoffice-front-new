/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unused-prop-types */
import React, { useState } from "react";
import { Table, Input, Space, Button, DatePicker } from "antd";
import * as PropTypes from "prop-types";
import { SearchOutlined, AuditOutlined } from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import moment from "moment";
import classes from "./TableForm.module.scss";
import useWindowSize from "../../hooks/useWindowSize";
import ModalAnswerDetail from "../Modal/ModalAnswerDetail";

function TableFormAnswers(props) {
  const { forms } = props;

  const [{ width, height }] = useWindowSize();
  let size;
  if (width < 450) size = 600;
  else if (width >= 450 && width < 550) size = 450;
  else if (width >= 550 && width < 700) size = 320;
  else if (width >= 700 && width < 900) size = 300;
  else if (width >= 900 && width < 1000) size = 250;
  else if (width >= 1000 && width < 1250) size = 200;
  else size = 100;

  let sizeHeight;
  if (height < 800) sizeHeight = 350;
  else sizeHeight = 550;

  let data1 = [];
  const [searched, setSearched] = useState([]);
  const [column, setColumn] = useState("");

  /* const { data, loading } = useQuery(FETCH_MISSEDDEAL_BY_COLUMN_QUERY, {
    variables: {
      searched,
    },
    fetchPolicy: "network-only",
  }); */
  /* if (searched.length > 0) {
    if (data) {
      data1 = data.find.map((missedDeal) => {
        const splitDate = missedDeal.missionStart.split("T");
        const startDate = splitDate[0];
        const splitDate2 = missedDeal.missionEnd.split("T");
        const endtDate = splitDate2[0];
        const splitDate3 = missedDeal.createdAt.split("T");
        const createDate = splitDate3[0];
        return {
          key: missedDeal.missedDealID,
          missedDealID: missedDeal.missedDealID,
          company: missedDeal.companyName,
          ITConsultingFirm: missedDeal.ITConsultingFirm,
          intermedianryAgency: missedDeal.intermedianryAgency,
          missionStart: startDate,
          missionEnd: endtDate,
          skills: missedDeal.skills,
          jobTitle: missedDeal.jobTitle,
          dailyRate: missedDeal.dailyRate,
          city: missedDeal.city,
          missionProjectDescription: missedDeal.missionProjectDescription,
          creator: missedDeal.creator.email,
          candidate: missedDeal.candidate,
          createdAt: createDate,
        };
      });
    }
  } else { */
  data1 = forms.map((form) => {
    const splitDate3 = form.createdAt.split("T");
    const createDate = splitDate3[0];
    return {
      key: form.formResultID,
      formResultID: form.formResultID,
      formID: form.formID,
      formTitle: form.formTitle,
      formCreator: form.formCreator,
      clientID: form.clientID,
      answers: form.answers,
      createdAt: createDate,
    };
  });
  // }
  /* const Compare = (skill1, searched1) => {
    const splitSerched = searched1.toLowerCase().split(" ");
    const skill2 = skill1.toLowerCase();
    let exist = -1;
    splitSerched.map((serched) => {
      if (serched === skill2) {
        exist = 1;
      }
      return exist;
    });
    return exist;
  }; */

  const getDetail = (form) => {
    ModalAnswerDetail(form);
  };

  const exist = (searched1, dataIndex) => {
    let i = 0;
    for (i = 0; i < searched1.length; i += 1) {
      if (dataIndex in searched1[i]) {
        return i;
      }
    }
    return -1;
  };

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    setColumn([...column, dataIndex]);
    if (
      dataIndex === "missionStart" ||
      dataIndex === "missionEnd" ||
      dataIndex === "createdAt"
    ) {
      if (selectedKeys[0]) {
        const time = moment(selectedKeys[0]._d);
        const ob1 = { [dataIndex]: time.format("YYYY-MM-DDTHH:MM:ss") };
        const isExist1 = exist(searched, dataIndex);
        if (searched.length === 0 || isExist1 === -1) {
          setSearched([...searched, ob1]);
        } else {
          const newArr3 = [...searched];
          newArr3[isExist1] = ob1;
          setSearched(newArr3);
        }
      }
    } else if (dataIndex === "skills") {
      if (selectedKeys[0]) {
        const splitSkills = selectedKeys[0].split(";");
        const searchedS = [];
        for (let i = 0; i < splitSkills.length; i += 1) {
          searchedS.push({ name: splitSkills[i] });
        }
        const ob3 = { [dataIndex]: searchedS };
        const isExist = exist(searched, dataIndex);
        if (searched.length === 0 || isExist === -1) {
          setSearched([...searched, ob3]);
        } else {
          const newArr = [...searched];
          newArr[isExist] = ob3;
          setSearched(newArr);
        }
      }
    } else if (selectedKeys[0]) {
      const ob = { [dataIndex]: selectedKeys[0] };
      const isExist2 = exist(searched, dataIndex);
      if (searched.length === 0 || isExist2 === -1) {
        setSearched([...searched, ob]);
      } else {
        const newArr1 = [...searched];
        newArr1[isExist2] = ob;
        setSearched(newArr1);
      }
    }
  };

  const handleReset = () => {
    setColumn("");
    setSearched([]);
  };
  const handleOneReset = (dataIndex) => {
    setColumn("");
    const items = searched.filter((search) => !(dataIndex in search));
    setSearched(items);
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
      <div style={{ padding: 8 }}>
        {dataIndex === "missionStart" ||
        dataIndex === "missionEnd" ||
        dataIndex === "createdAt" ? (
          <DatePicker
            initialValues={moment("2020/01/01", "YYYY/MM/DD")}
            format="YYYY/MM/DD"
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={(date) => setSelectedKeys(date ? [date] : [])}
            onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
        ) : (
          <Input
            placeholder={
              dataIndex === "skills"
                ? "skill;skill;skill..."
                : `Search ${dataIndex}`
            }
            value={selectedKeys[0]}
            onChange={(e) =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
        )}

        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 30 }}
          />
          <Button
            onClick={() => handleOneReset(dataIndex)}
            size="small"
            style={{ width: 70 }}
          >
            Reset
          </Button>
          <Button
            onClick={() => handleReset()}
            size="small"
            style={{ width: 70 }}
          >
            Reset All
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#e6f7ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    render: (text) => {
      const tabS = [];
      for (let i = 0; i < searched.length; i += 1) {
        if (!("skills" in searched[i])) {
          const arrayTemp = Object.values(searched[i]);
          tabS.push(arrayTemp[0]);
        }
      }

      if (tabS)
        return (
          <Highlighter
            highlightStyle={{ backgroundColor: "#40a9ff", padding: 0 }}
            searchWords={tabS}
            autoEscape
            textToHighlight={text.toString()}
          />
        );
      return text;
    },
  });
  const columns = [
    {
      title: " Answer Date",
      dataIndex: "createdAt",
      key: "createdAt",
      // ...getColumnSearchProps("createdAt"),
      sorter: (a, b) => moment(a.createdAt).unix() - moment(b.createdAt).unix(),
    },
    {
      title: "Candidate",
      dataIndex: "clientID",
      key: "clientID",
      // ...getColumnSearchProps("subject"),
    },

    {
      title: "Form Title",
      dataIndex: "formTitle",
      key: "formTitle",
      // ...getColumnSearchProps("creator"),
    },

    {
      title: "Answers",
      dataIndex: "key",
      key: "key",
      render: (key, form) => (
        <div style={{ display: "flex" }}>
          {console.log("form222", form)}
          <AuditOutlined
            className={classes.icon2}
            onClick={() => getDetail(form)}
          />
        </div>
      ),
    },
  ];
  return (
    <div style={{ position: "relative", height: "80%" }}>
      {/*  {visibleModalForm && (
        <ModalForm
          visibleModalForm={visibleModalForm}
          setVisibleModalForm={setVisibleModalForm}
          form={formItem}
        />
      )} */}

      {/* {loading ? <LoadingOutlined className={classes.icon3} /> : null} */}
      {data1 ? (
        <Table
          style={{ position: "relative" }}
          columns={columns}
          dataSource={data1}
          scroll={{ x: `calc(1px + ${size}%)`, y: sizeHeight }}
        />
      ) : null}
    </div>
  );
}

TableFormAnswers.propTypes = {
  forms: PropTypes.arrayOf(PropTypes.object),
  setSelectedKeys: PropTypes.func,
  selectedKeys: PropTypes.string,
  confirm: PropTypes.func,
  clearFilters: PropTypes.func,
  user: PropTypes.shape({
    role: PropTypes.array,
    email: PropTypes.string,
    groups: PropTypes.array,
  }).isRequired,
};

TableFormAnswers.defaultProps = {
  forms: [],
  setSelectedKeys: () => {},
  selectedKeys: "",
  confirm: () => {},
  clearFilters: () => {},
};

export default TableFormAnswers;
