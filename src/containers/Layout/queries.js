/* eslint-disable import/prefer-default-export */
import { gql } from "graphql.macro";

export const FETCH_USER_QUERY = gql`
  {
    me {
      userID
      username
      email
      permissions {
        name
        tabs {
          name
          permission
        }
      }
    }
  }
`;
