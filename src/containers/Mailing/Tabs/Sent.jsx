import React, { useEffect, useState } from "react";
import * as PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { Button, message, Table } from "antd";
import { useMutation, useQuery } from "@apollo/react-hooks";
import ContentContainer from "../../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../../components/ContentWrapper/ContentWrapper";
import { EXCLUDED_EMAILS, SENT_EMAILS } from "../query";
import { RESUME_MAILING, SEND_EMAIL, SEND_TIMESHEET_EMAIL } from "../mutations";
import localStateToQuery from "../localStateToQuery";
import { FETCH_MARKETINFO_QUERY } from "../../MarketInfo/utile/queries";

function Sent(props) {
  const BhUrl = window.location.href;
  const split1 = BhUrl.split("timesheets/");
  const [emailToBeSent, setEmailToBeSent] = useState({});
  const inTimesheets = split1[1] === "mailing";
  console.log("split1split1split1split1", inTimesheets);
  let initialFilters = {};
  if (inTimesheets) {
    initialFilters = {
      limit: 10,
      skip: 0,
      startDate: null,
      endDate: null,
      services: ["timesheets"],
    };
  } else {
    initialFilters = {
      limit: 10,
      skip: 0,
      startDate: null,
      endDate: null,
      services: ["bot_notifs", "accounts", "ACCOUNTS", "timesheets"],
    };
  }

  /* const initialFilters = {
    $limit: 1000,
    $skip: 0,
    $startDate: null,
    $endDate: null,
  }; */

  const [filters, setFilters] = useState({});
  const [queryFilters, setQueryFilters] = useState(
    localStateToQuery(initialFilters)
  );

  useEffect(() => {
    if (filters.searchTrigger) setQueryFilters(localStateToQuery(filters));
  }, [filters]);

  useEffect(() => {
    const newState = { ...filters };
    newState.searchTrigger = false;
    setFilters(newState);
  }, [queryFilters]);

  const paginate = (page, pageSize) => {
    const newState = { ...filters };
    newState.skip = (page - 1) * pageSize;
    newState.limit = pageSize;
    newState.searchTrigger = true;
    newState.services = ["bot_notifs", "accounts", "ACCOUNTS", "timesheets"];
    if (inTimesheets) newState.services = ["timesheets"];
    setFilters(newState);
  };

  const { loading, data, error } = useQuery(SENT_EMAILS, {
    variables: queryFilters,
    fetchPolicy: "network-only",
  });
  /* const success = () => {
    message.success("Email sent successfully");
  };
 console.log("emailToBeSent",emailToBeSent);
  const [sendEmail] = useMutation(SEND_EMAIL, {
    variables: {
      service: "timesheets",
      action: "opened",
      param: 4,
    },
    onCompleted() {
      success();
    },
  }); */

  // console.log("data", data);
  let emails = null;
  let total = null;
  if (data && data.sentEmails && data.sentEmails.sent) {
    emails = data.sentEmails.sent.map((m) => ({
      date: new Date(Number.parseInt(m.createdAt, 10)).toLocaleDateString(
        "fr-FR"
      ),
      mailTo: `${m.mailTo.email} / ${m.mailTo.name}`,
      subject: m.subject,
      action: m.action,
      service: m.service,
      param: m.param,
    }));
    total = data.sentEmails.total;
  }

  const successResendEmail = () => {
    message.success("Email was resend successfully");
  };

  const [sendTimesheetEmail] = useMutation(SEND_TIMESHEET_EMAIL, {
    onCompleted() {
      successResendEmail();
    },
    refetchQueries: () => [{ query: SENT_EMAILS }],
    onError(e) {
      console.log("error", e);
    },
  });
  const buttonAction = (button, service, action, timesheetId) => {
    // if(status === "open") action = "opened";
    // if(status === "pending") action = "submitted";
    // if(status === "approved") action = "approved";
    //  if(status === "rejected") action = "rejected";
    console.log("service",service,"action",action,"timesheetId",timesheetId);
    sendTimesheetEmail({
      variables: {
        service,
        action,
        timesheetId: Number.parseInt(timesheetId, 10),
      },
    });
  };

  const columns = [
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
      width: 50,
      fixed: "left",
    },
    {
      title: "Mail To",
      width: 180,
      dataIndex: "mailTo",
      key: "mailTo",
      fixed: "left",
    },
    {
      title: "Subject",
      width: 200,
      dataIndex: "subject",
      key: "subject",
    },
    {
      title: "Service",
      dataIndex: "service",
      key: "service",
      width: 100,
    },
    {
      title: "Action",
      key: "action",
      width: 60,
      render: (_, m) => {
        const button = "";
        console.log("text = ", m.mailTo, "timesheetID", m.param);
        return (
          <Button
            type="link"
            onClick={() => buttonAction(button, m.service, m.action, m.param)}
          >
            Resend Email
          </Button>
        );
      },
    },
  ];
  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <Table
          columns={columns}
          dataSource={!error && emails}
          size="small"
          scroll={{ y: 450 }}
          sticky
          pagination={{
            onChange: paginate,
            total,
          }}
          loading={loading}
        />
      </ContentContainer>
    </ContentWrapper>
  );
}

Sent.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
};
export default withRouter(Sent);
