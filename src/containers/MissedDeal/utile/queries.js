import { gql } from "graphql.macro";

const FETCH_MISSEDDEAL_QUERY = gql`
  {
    missedDeals {
      missedDealID
      companyId
      companyName
      ITConsultingFirm
      intermedianryAgency
      missionStart
      missionEnd
      createdAt
      skills {
        name
      }
      jobTitle
      dailyRate
      city
      missionProjectDescription
      creator {
        email
        userID
      }
      candidate
    }
  }
`;

const FETCH_ONE_MISSEDDEAL_QUERY = gql`
  query($missedDealID: ID!) {
    missedDeal(missedDealID: $missedDealID) {
      missedDealID
      companyId
      companyName
      ITConsultingFirm
      intermedianryAgency
      missionStart
      missionEnd
      createdAt
      skills {
        name
      }
      jobTitle
      dailyRate
      city
      missionProjectDescription
      creator {
        email
        userID
      }
      candidate
    }
  }
`;
const GET_SKILLS_QUERY = gql`
  {
    skills {
      name
    }
  }
`;
const GET_COMPANY_QUERY = gql`
  {
    clientCorporations {
      id
      name
    }
  }
`;
export {
  FETCH_MISSEDDEAL_QUERY,
  FETCH_ONE_MISSEDDEAL_QUERY,
  GET_SKILLS_QUERY,
  GET_COMPANY_QUERY,
};
