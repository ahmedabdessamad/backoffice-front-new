import { gql } from "graphql.macro";

export const VALIDATE_SENDOUT = gql`
  mutation ValidateSendout($input: SendoutValidationInput!) {
    validateSendout(input: $input) {
      jobSubmissionID
      jobOrderID
      jobOrderTitle
      candidateID
      candidateFirstName
      candidateLastName
      candidateOccupation
      candidateAvailableDate
      candidateAddress {
        address1
        address2
        city
        state
        zip
        countryID
      }
      candidateCv
      comment
      payRate
      reference {
        id
        referenceFirstName
        referenceLastName
        referenceTitle
        outcome
      }
    }
  }
`;

export const REJECT_SENDOUT = gql`
  mutation RejectSendout($id: ID!) {
    rejectSendout(id: $id) {
      id
      status
    }
  }
`;

export const ADD_CANDIDATE_FILE = gql`
  mutation($candidateID: ID!, $file: Upload!) {
    addCandidateFile(candidateID: $candidateID, file: $file)
  }
`;
