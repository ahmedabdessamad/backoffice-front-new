import React, { useEffect, useState, useRef } from "react";
import { Input } from "antd";
import classes from "./editableText.module.scss";

const EditableText = ({ text, editable, onSave, inputType, ...restProps }) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
      inputRef.current.state.value = text;
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
  };

  const save = () => {
    toggleEdit();
    onSave(inputRef.current.state.value);
  };

  let childNode = <div className={classes.textWrapper}>{text}</div>;

  if (editable) {
    childNode = editing ? (
      <Input
        ref={inputRef}
        onPressEnter={save}
        onBlur={save}
        type={inputType}
      />
    ) : (
      <div className={classes.editableTextWrapper} onClick={toggleEdit}>
        {text}
      </div>
    );
  }

  return <div {...restProps}>{childNode}</div>;
};

export default EditableText;
