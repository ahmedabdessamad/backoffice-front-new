import React from "react";
import { useQuery } from "@apollo/react-hooks";
import * as PropTypes from "prop-types";
import { Typography, Button } from "antd";
import { FlagOutlined, LoadingOutlined } from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import { useSelector } from "react-redux";
import { FETCH_MARKETINFO_QUERY } from "./utile/queries";
import TableMarketInfo from "../../components/TableMarketInfo/TableMarketInfo";
import classes from "./MarketInfo.module.scss";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import { isUserAuthorized } from "../../helpers/authorization/permissionsCheckHelpers";
import checkSpecificPermission from "../../helpers/authorization/specificPermissionCheckHelpers";

const { Title } = Typography;

function MarketInfo(props) {
  const user = useSelector((state) => state.AuthReducer.user);
  const { history } = props;
  const { loading, data } = useQuery(FETCH_MARKETINFO_QUERY);

  const addMarketInfoHandler = () => {
    history.push(`marketInfo/add`);
  };

  const objectToCsv = (list) => {
    const csvRows = [];
    // get the headers
    const headers = Object.keys(list[0]);
    csvRows.push(headers.join(","));
    // loop over the rows
    // eslint-disable-next-line no-restricted-syntax
    for (const row of list) {
      const values = headers.map((header) => {
        const escaped = `${row[header]}`.replace(/"/g, '\\"');
        return `"${escaped}"`;
      });
      csvRows.push(values.join(","));
    }
    return csvRows.join("\n");
  };

  const download = (list) => {
    const blob = new Blob([list], { type: "text/csv" });
    const url1 = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.setAttribute("hidden", "");
    a.setAttribute("href", url1);
    a.setAttribute("download", "MarketInfos.csv");
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };

  const getReport = () => {
    if (data) {
      const list = data.marketInfos.map((marketInfo) => {
        return {
          createdAt: marketInfo.createdAt,
          company: marketInfo.companyName,
          jobTitle: marketInfo.jobTitle,
          contact: marketInfo.contact,
          email: marketInfo.email,
          phone: marketInfo.phone,
          industry: marketInfo.industry.map(({ name }) => name),
          categories: marketInfo.categories.map(({ name }) => name),
          description: marketInfo.description,
          creator: marketInfo.creator.email,
        };
      });

      const csvData = objectToCsv(list);

      download(csvData);
    }
  };

  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Market Info
          </Title>
          <FlagOutlined className={classes.icon}> </FlagOutlined>
          {user ? (
            <div>
              {isUserAuthorized(user, { MARKETINFO: 6 }) ? (
                <Button
                  type="primary"
                  htmlType="submit"
                  className={classes.button}
                  onClick={addMarketInfoHandler}
                >
                  Add Market Info
                </Button>
              ) : null}
            </div>
          ) : null}
          <div>
            {isUserAuthorized(user, { MARKETINFO: 4 }) ? (
              <div>
                <Button
                  type="primary"
                  htmlType="submit"
                  className={classes.buttonExport}
                  onClick={() => getReport()}
                >
                  Export CSV
                </Button>
              </div>
            ) : null}
          </div>
        </div>

        <br />
        {loading ? <LoadingOutlined className={classes.icon2} /> : null}
        {data ? (
          <TableMarketInfo
            marketInfos={data.marketInfos}
            setSelectedKeys={() => {}}
            selectedKeys=""
            confirm={() => {}}
            clearFilters={() => {}}
            user={user}
          />
        ) : null}
      </ContentContainer>
    </ContentWrapper>
  );
}

MarketInfo.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(MarketInfo);
