import { Modal, Form, Input, Card, Select, Radio, InputNumber } from "antd";
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import checkboxChoicesActions from "../../redux/checkboxChoices/actions";

const { Option, OptGroup } = Select;
const { setInputList } = checkboxChoicesActions;

function EditModalInput(props) {
  const {
    visibleEditModalInput,
    setVisibleEditModalInput,
    formItem,
    setFormItem,
    formItemIndex,
    menuKey,
  } = props;
  const [name, setName] = useState("");
  const [type, setType] = useState("");

  const [choicesInputs, setChoicesInputs] = useState([]);
  const [numbCheckbox, setNumbCheckbox] = useState(0);
  const [placeholder, setPlaceholder] = useState("");
  const [defaultValue, setDefaultValue] = useState("");
  const [maxLength, setMaxLength] = useState();
  const [radioValue, setRadioValue] = useState();
  const [inputChoicesValues, setInputChoicesValues] = useState({});
  let ipp = 1;

  const inputList = useSelector(
    (state) => state.CheckboxChoicesReducer.inputList
  );
  if (formItem) {
    if (!name) {
      if (formItem.key === "Checkbox") {
        setNumbCheckbox(formItem.choices.length);
      }
      setName(formItem.name);
      setType(formItem.type);
      setRadioValue(formItem.required);
      setPlaceholder(formItem.placeholder);
      setDefaultValue(formItem.defaultValue);
      setChoicesInputs(formItem.choices);
    }
  }
  const dispatch = useDispatch();
  useEffect(() => {}, [inputChoicesValues, inputList]);

  const handleChange = ({ target: { value } }) => {
    setInputChoicesValues((prevState) => {
      return {
        ...prevState,
        [ipp]: value,
      };
    });
  };

  const changeFocus = (i) => {
    ipp = i;
  };

  const renderForLoop = () => {
    const arr = [];
    if (numbCheckbox > 1 && choicesInputs.length > 0) {
      for (let i = 1; i <= numbCheckbox; i += 1) {
        arr.push(
          <Form.Item
            name={`choice${i}`}
            label={`choice ${i}`}
            validateTrigger={["onChange", "onBlur"]}
          >
            <Input
              onFocus={() => changeFocus(i)}
              name={`choice${i}`}
              onChange={handleChange}
              defaultValue={choicesInputs[i - 1]}
            />
          </Form.Item>
        );
      }
    }

    return arr;
  };

  useEffect(() => {
    if (numbCheckbox) {
      const tab = renderForLoop();
      setChoicesInputs(tab);
    }
  }, [numbCheckbox]);

  const handleOk = (e) => {
    if (
      formItem.key === "simpleInput" ||
      formItem.key === "inputText" ||
      formItem.key === "inputNumber"
    ) {
      /* dispatch(
        addInputList({
          name,
          type,
          placeholder,
          key: menuKey,
          defaultValue,
          maxLength,
          required: radioValue,
        })
      ); */
      const ob = {
        name,
        type,
        placeholder,
        key: menuKey,
        defaultValue,
        maxLength,
        required: radioValue,
      };
      const list = [...inputList];
      list[formItemIndex] = ob;
      dispatch(setInputList(list));

      setVisibleEditModalInput(false);
    } else if (menuKey === "Checkbox") {
      // const tabChoices = Object.values(inputChoicesValues);
      // setChoices(tabChoices);
      // dispatch(setChoices({name, tabChoices}));
      setInputChoicesValues([]);
      /* dispatch(
        addInputList({
          name,
          type,
          key: menuKey,
          required: radioValue,
          choices: Object.values(inputChoicesValues),
        })
      ); */
      const ob1 = {
        name,
        type,
        key: menuKey,
        required: radioValue,
        choices: Object.values(inputChoicesValues),
      };
      const list = [...inputList];
      list[formItemIndex] = ob1;
      dispatch(setInputList(list));

      setVisibleEditModalInput(false);
    }
    setFormItem(null);
  };
  const handleCancel = (e) => {
    setVisibleEditModalInput(false);
    setFormItem(null);
  };
  const changenameHandler = (event) => {
    setName(event.target.value);
  };
  const changetypeHandler = (value) => {
    setType(value);
  };
  const changeplaceholderHandler = (event) => {
    setPlaceholder(event.target.value);
  };
  const changedefaultValueHandler = (event) => {
    setDefaultValue(event.target.value);
  };
  const changemaxLengthHandler = (event) => {
    setMaxLength(event.target.value);
  };

  const changeNumbCheckboxHandler = (value) => setNumbCheckbox(value);

  const onChangeRadio = (e) => {
    if (e.target.value === "Required") {
      setRadioValue(true);
    } else {
      setRadioValue(false);
    }
  };

  return (
    <div>
      <Modal
        title="Edit Configuration"
        visible={visibleEditModalInput}
        onOk={handleOk}
        onCancel={handleCancel}
        width="40%"
      >
        <div>
          <Card>
            {formItem ? (
              <Form name="nest-messages">
                <Form.Item name="name" label="Name">
                  <Input
                    type="name"
                    onChange={changenameHandler}
                    name="name"
                    value={name}
                    defaultValue={name}
                  />
                </Form.Item>
                {menuKey === "simpleInput" ? (
                  <Form.Item
                    name="type"
                    label="Type"
                    rules={[
                      {
                        max: 30,
                        message: "Length must be less then 30 characters!",
                      },
                    ]}
                  >
                    <Select
                      showSearch
                      style={{ width: 200 }}
                      placeholder="Select a type"
                      optionFilterProp="children"
                      onChange={changetypeHandler}
                      defaultValue={type}
                      filterOption={(input, option) =>
                        option.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      <Option value="text">Text</Option>
                      <Option value="email">Email</Option>
                      <Option value="password">Password</Option>
                    </Select>
                  </Form.Item>
                ) : null}
                {menuKey === "inputNumber" ? (
                  <Form.Item
                    name="type"
                    label="Type"
                    rules={[
                      {
                        max: 30,
                        message: "Length must be less then 30 characters!",
                      },
                    ]}
                  >
                    <Select
                      showSearch
                      style={{ width: 200 }}
                      placeholder="Select a type"
                      optionFilterProp="children"
                      onChange={changetypeHandler}
                      defaultValue={type}
                      filterOption={(input, option) =>
                        option.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      <Option value="Number">Number</Option>
                      <Option value="Decimal">Decimal</Option>
                      <Option value="Percentage">Percentage</Option>
                      <OptGroup label="Currency">
                        <Option value="Dollar">Dollar</Option>
                        <Option value="Euro">Euro</Option>
                        <Option value="Pound">Pound</Option>
                      </OptGroup>
                    </Select>
                  </Form.Item>
                ) : null}
                {!(menuKey === "Checkbox") && !(menuKey === "FormTitle") ? (
                  <Form.Item
                    name="placeholder"
                    label="placeholder"
                    rules={[
                      {
                        max: 30,
                        message: "Length must be less then 30 characters!",
                      },
                    ]}
                  >
                    <Input
                      type="placeholder"
                      onChange={changeplaceholderHandler}
                      name="placeholder"
                      value={placeholder}
                      defaultValue={placeholder}
                    />
                  </Form.Item>
                ) : null}
                {!(menuKey === "FormTitle") ? (
                  <Form.Item name="required" label="required">
                    <Radio.Group
                      defaultValue={radioValue ? "Required" : "Optional"}
                      onChange={onChangeRadio}
                      value={radioValue}
                    >
                      <Radio value="Required">Required</Radio>
                      <Radio value="Optional">Optional</Radio>
                    </Radio.Group>
                  </Form.Item>
                ) : null}
                {!(type === "password") &&
                !(menuKey === "Checkbox") &&
                !(menuKey === "FormTitle") ? (
                  <div>
                    {!(type === "email") ? (
                      <Form.Item name="defaultValue" label="Default value">
                        <Input
                          type="defaultValue"
                          onChange={changedefaultValueHandler}
                          name="defaultValue"
                          value={defaultValue}
                          defaultValue={defaultValue}
                        />
                      </Form.Item>
                    ) : (
                      <Form.Item name="defaultValue" label="Default value">
                        <Input
                          type="defaultValue"
                          onChange={changedefaultValueHandler}
                          name="defaultValue"
                          value={defaultValue}
                          defaultValue={defaultValue}
                        />
                      </Form.Item>
                    )}
                  </div>
                ) : null}
                {!(menuKey === "inputNumber") &&
                !(menuKey === "Checkbox") &&
                !(menuKey === "FormTitle") ? (
                  <Form.Item name="maxLength" label="maxLength">
                    <Input
                      type="maxLength"
                      onChange={changemaxLengthHandler}
                      name="maxLength"
                      value={maxLength}
                      defaultValue={maxLength}
                    />
                  </Form.Item>
                ) : null}
                {menuKey === "Checkbox" ? (
                  <Form.Item name="numbCheckbox" label="numbre of choices">
                    <InputNumber
                      onChange={changeNumbCheckboxHandler}
                      name="numbCheckbox"
                      value={numbCheckbox}
                      min="0"
                      max="10"
                      defaultValue={numbCheckbox}
                    />
                  </Form.Item>
                ) : null}
                {numbCheckbox && menuKey === "Checkbox"
                  ? /* <ChoicesForm
                    choicesInputs={choicesInputs}
                    setChoicesInputs={setChoicesInputs}
                    onFinish1={handleOk}
                    setChoicesModal={setChoicesModal}
                    choicesModal={choicesModal}
                  /> */

                    choicesInputs
                  : null}
              </Form>
            ) : null}
          </Card>
        </div>
      </Modal>
    </div>
  );
}

export default EditModalInput;
