import React from "react";
import { useQuery } from "@apollo/react-hooks";
import * as PropTypes from "prop-types";
import { Typography, Button } from "antd";
import { FlagOutlined, LoadingOutlined } from "@ant-design/icons";
import { withRouter, useRouteMatch } from "react-router-dom";
import { useSelector } from "react-redux";
import { FETCH_MISSEDDEAL_QUERY } from "./utile/queries";
import TableMissedDeal from "../../components/TableMissedDeal/TableMissedDeal";
import classes from "./MissedDeal.module.scss";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import { isUserAuthorized } from "../../helpers/authorization/permissionsCheckHelpers";

const { Title } = Typography;

function MissedDeal(props) {
  const user = useSelector((state) => state.AuthReducer.user);
  const { history } = props;
  const match = useRouteMatch();

  const stripTrailingSlash = (str) => {
    if (str.substr(-1) === "/") {
      return str.substr(0, str.length - 1);
    }
    return str;
  };

  const url = stripTrailingSlash(match.url);
  const { loading, data } = useQuery(FETCH_MISSEDDEAL_QUERY);

  const addMissedDealHandler = () => {
    history.push(`${url}/add`);
  };
  const objectToCsv = (list) => {
    const csvRows = [];
    // get the headers
    const headers = Object.keys(list[0]);
    csvRows.push(headers.join(","));
    // loop over the rows
    // eslint-disable-next-line no-restricted-syntax
    for (const row of list) {
      const values = headers.map((header) => {
        const escaped = `${row[header]}`.replace(/"/g, '\\"');
        return `"${escaped}"`;
      });
      csvRows.push(values.join(","));
    }
    return csvRows.join("\n");
  };

  const download = (list) => {
    const blob = new Blob([list], { type: "text/csv" });
    const url1 = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.setAttribute("hidden", "");
    a.setAttribute("href", url1);
    a.setAttribute("download", "MissedDeals.csv");
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };

  const getReport = () => {
    if (data) {
      const list = data.missedDeals.map((missedDeal) => {
        return {
          company: missedDeal.companyName,
          ITConsultingFirm: missedDeal.ITConsultingFirm,
          intermedianryAgency: missedDeal.intermedianryAgency,
          missionStart: missedDeal.missionStart,
          missionEnd: missedDeal.missionEnd,
          skills: missedDeal.skills.map(({ name }) => name),
          jobTitle: missedDeal.jobTitle,
          dailyRate: missedDeal.dailyRate,
          city: missedDeal.city,
          missionProjectDescription: missedDeal.missionProjectDescription,
          creator: missedDeal.creator.email,
          candidat: missedDeal.candidate,
        };
      });

      const csvData = objectToCsv(list);
      download(csvData);
    }
  };

  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Missed Deal
          </Title>
          <FlagOutlined className={classes.icon}> </FlagOutlined>
          {user ? (
            <div>
              {isUserAuthorized(user, { MISSEDDEAL: 6 }) ? (
                <Button
                  type="primary"
                  htmlType="submit"
                  className={classes.button}
                  onClick={addMissedDealHandler}
                >
                  Add Missed Deal
                </Button>
              ) : null}
            </div>
          ) : null}
          <div>
            {isUserAuthorized(user, { MISSEDDEAL: 4 }) ? (
              <div>
                <Button
                  type="primary"
                  htmlType="submit"
                  className={classes.buttonExport}
                  onClick={() => getReport()}
                >
                  Export CSV
                </Button>
              </div>
            ) : null}
          </div>
        </div>

        <br />
        {data ? (
          <TableMissedDeal
            missedDeals={data.missedDeals}
            setSelectedKeys={() => {}}
            selectedKeys=""
            confirm={() => {}}
            clearFilters={() => {}}
            user={user}
          >
            {" "}
          </TableMissedDeal>
        ) : null}
        {loading ? <LoadingOutlined className={classes.icon2} /> : null}
      </ContentContainer>
    </ContentWrapper>
  );
}

MissedDeal.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(MissedDeal);
