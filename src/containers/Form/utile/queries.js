import { gql } from "graphql.macro";

const FETCH_FORM_QUERY = gql`
  {
    forms {
      formID
      subject
      structure {
        name
        key
        type
        placeholder
        defaultValue
        required
        maxLength
        choices
      }
      creator {
        email
      }
      createdAt
    }
  }
`;

const FETCH_ONE_FORM_QUERY = gql`
  query($formID: ID!) {
    form(formID: $formID) {
      formID
      subject
      structure {
        name
        key
        type
        placeholder
        defaultValue
        required
        maxLength
        choices
      }
      creator {
        email
      }
      createdAt
    }
  }
`;
const FETCH_FORM_ANSWERS_QUERY = gql`
  {
    formResults {
      formResultID
      formID
      formTitle
      formCreator
      clientID
      answers {
        question
        answer
      }
      createdAt
    }
  }
`;

const FETCH_ONE_ANSWER_QUERY = gql`
  query($formResultID: ID!) {
    formResult(formResultID: $formResultID) {
      formResultID
      formID
      formTitle
      formCreator
      clientID
      answers {
        question
        answer
      }
      createdAt
    }
  }
`;

const FETCH_ONE_FORM_ANSWERS_QUERY = gql`
  query($formID: ID!) {
    ResultOfOneForm(formID: $formID) {
      formResultID
      formID
      formTitle
      formCreator
      clientID
      answers {
        question
        answer
      }
      createdAt
    }
  }
`;

export {
  FETCH_FORM_QUERY,
  FETCH_ONE_FORM_QUERY,
  FETCH_FORM_ANSWERS_QUERY,
  FETCH_ONE_ANSWER_QUERY,
  FETCH_ONE_FORM_ANSWERS_QUERY,
};
