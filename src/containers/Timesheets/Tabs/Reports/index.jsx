import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { Table, Button, Input, Space } from "antd";
import { CloudDownloadOutlined, SearchOutlined } from "@ant-design/icons";
import { useQuery } from "@apollo/react-hooks";
import moment from "moment";
import REPORTS_QUERY from "./query";
import localStateToQuery from "./localStateToQuery";

function ReportsComponent() {
  const initialFilters = {
    id: null,
    placementID: null,
    timesheetID: null,
    limit: 10,
    skip: 0,
    periodStart: null,
    periodEnd: null,
    name: null,
  };

  const [filters, setFilters] = useState({});
  const [queryFilters, setQueryFilters] = useState(
    localStateToQuery(initialFilters)
  );

  const searchQuery = () => {
    setFilters({ ...filters, searchTrigger: true });
  };
  const clearSearchQuery = () => {
    setFilters({ ...initialFilters, searchTrigger: true });
  };
  const clearSearchQueryItem = (dataIndex) => {
    const newState = { ...filters };
    newState[dataIndex] = null;
    newState.searchTrigger = true;
    setFilters(newState);
  };
  const filterChangeHandler = (e, dataIndex) => {
    const newState = { ...filters };
    newState[dataIndex] = e.target.value;
    setFilters(newState);
  };

  useEffect(() => {
    if (filters.searchTrigger) setQueryFilters(localStateToQuery(filters));
  }, [filters]);

  useEffect(() => {
    const newState = { ...filters };
    newState.searchTrigger = false;
    setFilters(newState);
  }, [queryFilters]);

  const paginate = (page) => {
    const newState = { ...filters };
    newState.skip = (page - 1) * 10;
    newState.searchTrigger = true;
    setFilters(newState);
  };

  const { loading, data, error } = useQuery(REPORTS_QUERY, {
    variables: queryFilters,
  });
  let reports = null;
  let total = null;
  if (data && data.reports) {
    reports = data.reports.map((rp) => ({
      placementID: rp.placementID,
      timesheetID: rp.timesheetID,
      date:
        rp.dateLastUpdate &&
        moment(rp.dateLastUpdate).format("DD/MM/YYYY HH:mm"),
      frFile: rp.frFile,
      enFile: rp.enFile,
    }));
    total = data.reports && data.reports[0] && data.reports[0].total;
  }

  const innerSearch = (dataIndex) => ({
    filterDropdown: () => {
      return (
        <div style={{ padding: 8 }}>
          <Input
            placeholder={`Search by ${dataIndex}`}
            value={filters[dataIndex]}
            onChange={(e) => filterChangeHandler(e, dataIndex)}
            onPressEnter={searchQuery}
            style={{ marginBottom: 10, display: "flex" }}
          />
          <Space>
            <Button
              type="primary"
              onClick={searchQuery}
              icon={<SearchOutlined />}
              size="small"
              style={{ width: 80 }}
            >
              Search
            </Button>
            <Button
              onClick={() => clearSearchQueryItem(dataIndex)}
              size="small"
              style={{ width: 80 }}
            >
              Reset
            </Button>
            <Button
              onClick={() => clearSearchQuery()}
              size="small"
              style={{ width: 80 }}
            >
              Reset All
            </Button>
          </Space>
        </div>
      );
    },
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    render: (text) => text,
  });

  const columns = [
    {
      title: "Placement ID",
      dataIndex: "placementID",
      key: "placementID",
      fixed: "left",
      ...innerSearch("placementID"),
    },
    {
      title: "Timesheet ID",
      dataIndex: "timesheetID",
      key: "date",
      ...innerSearch("timesheetID"),
    },
    {
      title: "Creation Date",
      dataIndex: "date",
      key: "date",
      ...innerSearch("date"),
    },
    {
      title: "FR File",
      dataIndex: "frFile",
      key: "frFile",
      fixed: "right",
      render: (frFile) => {
        if (!frFile) return null;
        return (
          <a href={frFile} target="_blank" download rel="noreferrer">
            <CloudDownloadOutlined />
          </a>
        );
      },
    },
    {
      title: "EN File",
      dataIndex: "enFile",
      key: "enFile",
      fixed: "right",
      render: (enFile) => {
        if (!enFile) return null;
        return (
          <a href={enFile} target="_blank" download rel="noreferrer">
            <CloudDownloadOutlined />
          </a>
        );
      },
    },
  ];

  return (
    <Table
      columns={columns}
      dataSource={!error && reports}
      scroll
      size="small"
      pagination={{
        onChange: paginate,
        total,
      }}
      loading={loading}
    />
  );
}

export default withRouter(ReportsComponent);
