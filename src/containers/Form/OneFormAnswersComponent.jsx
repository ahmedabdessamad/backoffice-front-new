import React from "react";
import { useQuery } from "@apollo/react-hooks";
import * as PropTypes from "prop-types";
import { Typography } from "antd";
import { FlagOutlined, LoadingOutlined } from "@ant-design/icons";
import { withRouter, useRouteMatch } from "react-router-dom";
import { useSelector } from "react-redux";
import { FETCH_ONE_FORM_ANSWERS_QUERY } from "./utile/queries";
import TableFormAnswers from "../../components/Form/TableFormAnswers";
import classes from "./FormListComponent.module.scss";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";

const { Title } = Typography;

function OneFormAnswersComponent() {
  const user = useSelector((state) => state.AuthReducer.user);
  const match = useRouteMatch();
  const formID = match.params.id;

  const { loading, data } = useQuery(FETCH_ONE_FORM_ANSWERS_QUERY, {
    variables: {
      formID,
    },
  });

  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Form Answers
          </Title>
          <FlagOutlined className={classes.icon}> </FlagOutlined>
        </div>

        <br />
        {data ? (
          <TableFormAnswers
            forms={data.ResultOfOneForm}
            setSelectedKeys={() => {}}
            selectedKeys=""
            confirm={() => {}}
            clearFilters={() => {}}
            user={user}
          >
            {" "}
          </TableFormAnswers>
        ) : null}
        {loading ? <LoadingOutlined className={classes.icon2} /> : null}
      </ContentContainer>
    </ContentWrapper>
  );
}

OneFormAnswersComponent.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(OneFormAnswersComponent);
