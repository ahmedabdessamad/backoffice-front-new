/* eslint-disable import/prefer-default-export */
import { gql } from "graphql.macro";

export const GET_ROLES = gql`
  {
    roles {
      id
      name
    }
  }
`;
