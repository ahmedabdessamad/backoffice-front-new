/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from "react";
import classes from "./contentContainer.module.scss";

export default (props) => (
  <div
    className={
      props.className != null
        ? `${props.className} ${classes.contentContainer}`
        : classes.contentContainer
    }
    {...props}
  >
    {props.children}
  </div>
);
