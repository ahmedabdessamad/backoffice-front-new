/* eslint-disable array-callback-return */
import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import moment from "moment";
import {
  Form,
  Input,
  Button,
  Typography,
  Card,
  DatePicker,
  Alert,
  Select,
  message,
} from "antd";
import { FlagOutlined, LoadingOutlined } from "@ant-design/icons";
import * as PropTypes from "prop-types";
import { useSelector } from "react-redux";
import classes from "./AddMissedDeal.module.scss";
import { UPDATE_MISSEDDEAL_MUTATION } from "./utile/mutations";
import {
  FETCH_ONE_MISSEDDEAL_QUERY,
  FETCH_MISSEDDEAL_QUERY,
  GET_SKILLS_QUERY,
  GET_COMPANY_QUERY,
} from "./utile/queries";
import checkSpecificPermission from "../../helpers/authorization/specificPermissionCheckHelpers";
import ModalUpdateCandidate from "../../components/Modal/ModalUpdateCandidate";

const { Title } = Typography;

function EditMissedDeal(props) {
  const user = useSelector((state) => state.AuthReducer.user);
  const [id, setId] = useState("");
  const [company1, setCompany1] = useState("");
  const [ITConsultingFirm1, setITConsultingFirm1] = useState("");
  const [intermedianryAgency1, setIntermedianryAgency1] = useState("");
  const [missionStart1, setMissionStart1] = useState(moment());
  const [missionEnd1, setMissionEnd1] = useState(moment());
  const [skills1, setSkills1] = useState([{}]);
  const [jobTitle1, setJobTitle1] = useState("");
  const [dailyRate1, setDailyRate1] = useState({});
  const [city1, setCity1] = useState("");
  const [missionProjectDescription1, setMissionProjectDescription1] = useState(
    ""
  );
  const [candidate1, setCandidate1] = useState("");

  const [errors, setErrors] = useState({});
  const [visible, setVisible] = useState(false);
  const [missedDeal, setMissedDeal] = useState({});
  const MutationType = "update";

  const { history, match } = props;
  const missedDealIDfromUrl = match.params.id;
  const { data: dataComp } = useQuery(GET_COMPANY_QUERY);
  let companyList = null;
  const tab1 = [];
  if (dataComp) {
    companyList = dataComp.clientCorporations.map((comp) => comp.name);

    companyList.map((comp) => {
      tab1.push({ value: comp });
    });
  }
  const { data: dataK } = useQuery(GET_SKILLS_QUERY);
  let skillsList = null;
  const tab = [];
  if (dataK) {
    skillsList = dataK.skills.map((skill) => skill.name);

    skillsList.map((skill) => {
      tab.push({ value: skill });
    });
  }
  const { loading, data, error } = useQuery(FETCH_ONE_MISSEDDEAL_QUERY, {
    variables: {
      missedDealID: missedDealIDfromUrl,
    },
    fetchPolicy: "network-only",
  });
  if (data) {
    const {
      missedDealID,
      companyName,
      ITConsultingFirm,
      intermedianryAgency,
      missionStart,
      missionEnd,
      skills,
      jobTitle,
      dailyRate,
      city,
      missionProjectDescription,
      candidate,
      creator,
    } = data.missedDeal;
    console.log(
      "creator.id",
      checkSpecificPermission({ MISSEDDEAL: 6 }, user, creator.userID)
    );
    if (
      !checkSpecificPermission({ MISSEDDEAL: 6 }, user, creator.userID) ||
      error
    ) {
      history.push(`/home/missedDeal`);
    } else if (!id) {
      setId(missedDealID);
      setCompany1(companyName);
      setITConsultingFirm1(ITConsultingFirm);
      setIntermedianryAgency1(intermedianryAgency);
      setMissionStart1(missionStart);
      setMissionEnd1(missionEnd);
      const tabskills = [];
      for (let i = 0; i < skills.length; i += 1) {
        tabskills.push({ name: skills[i].name });
      }
      setSkills1(tabskills);
      setJobTitle1(jobTitle);
      setDailyRate1(dailyRate);
      setCity1(city);
      setMissionProjectDescription1(missionProjectDescription);
      setCandidate1(candidate);
    }
  }

  const valideCompany = (rule, value) => {
    if (value) {
      if (value.length < 2 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };

  const valideItConsultingFirm = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const valideIntermedianryAgency = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };

  const validejobTitle = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const valideCity = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const valideMissionProjectDescription = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const valideMissionEnd = (rule, value) => {
    if (value) {
      if (value < missionStart1) {
        return Promise.reject(
          new Error(
            "The mission end date must be after the mission start date!"
          )
        );
      }
    }
    return Promise.resolve();
  };

  const changecompanyHandler = (value) => {
    // console.log("value===========>", value[0]);
    setCompany1(value[0]);
  };
  const changeITConsultingFirmHandler = (event) => {
    setITConsultingFirm1(event.target.value);
  };
  const changeintermedianryAgencyHandler = (event) => {
    setIntermedianryAgency1(event.target.value);
  };
  const changemissionStartHandler = (date) => {
    setMissionStart1(date);
  };
  const changemissionEndHandler = (date) => {
    setMissionEnd1(date);
  };
  const changeskillsHandler = (value) => {
    const tabskills = [];
    for (let i = 0; i < value.length; i += 1) {
      tabskills.push({ name: value[i] });
    }
    setSkills1(tabskills);
  };
  const changejobTitleHandler = (event) => {
    setJobTitle1(event.target.value);
  };
  const changedailyRateHandler = (event) => {
    setDailyRate1(event.target.value);
  };
  const changecityHandler = (event) => {
    setCity1(event.target.value);
  };
  const changemissionProjectDescriptionHandler = (event) => {
    setMissionProjectDescription1(event.target.value);
  };
  const changecandidateHandler = (event) => {
    setCandidate1(event.target.value);
  };
  const success = () => {
    message.success("Missed Deal updated successfully");
  };
  const [editMissedDeal] = useMutation(UPDATE_MISSEDDEAL_MUTATION, {
    variables: {
      missedDealID: id,
      input: {
        company: company1,
        candidateID: candidate1,
        ITConsultingFirm: ITConsultingFirm1,
        intermedianryAgency: intermedianryAgency1,
        missionStart: missionStart1,
        missionEnd: missionEnd1,
        skills: skills1,
        jobTitle: jobTitle1,
        dailyRate: dailyRate1,
        city: city1,
        missionProjectDescription: missionProjectDescription1,
      },
    },
    refetchQueries: () => [{ query: FETCH_MISSEDDEAL_QUERY }],
    onCompleted() {
      history.push("/home/missedDeal");
      success();
    },

    onError(err) {
      setErrors(err.graphQLErrors[0].message);
    },
  });

  const onFinish = () => {
    // editMissedDeal();
    setMissedDeal({
      company: company1,
      candidate: candidate1,
      ITConsultingFirm: ITConsultingFirm1,
      intermedianryAgency: intermedianryAgency1,
      missionStart: missionStart1,
      missionEnd: missionEnd1,
      skills: skills1,
      jobTitle: jobTitle1,
      dailyRate: dailyRate1,
      city: city1,
      missionProjectDescription: missionProjectDescription1,
    });
    setVisible(true);
  };

  return (
    <div>
      <ModalUpdateCandidate
        visible={visible}
        missedDeal={missedDeal}
        setVisible={setVisible}
        history={history}
        id={id}
        MutationType={MutationType}
      />
      <Card className={classes.card}>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Edit Missed Deal
          </Title>
          <FlagOutlined className={classes.icon}> </FlagOutlined>
        </div>
        <br />
        {errors === "Error: This candidate does not exist!" ? (
          <Alert message={errors.toString()} type="error" showIcon />
        ) : null}
        <br />
        <br />
        {data ? (
          <Form name="nest-messages" onFinish={onFinish}>
            <Form.Item name="company1" label="Company">
              {/* <Select
                showSearch
                style={{ width: "100%" }}
                optionFilterProp="children"
                onChange={changecompanyHandler}
                defaultValue={company1}
                options={tab1}
                filterOption={(input, options) =>
                  options.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              /> */}
              <Select
                mode="tags"
                style={{ width: "100%" }}
                onChange={changecompanyHandler}
                tokenSeparators={[","]}
                defaultValue={company1}
                options={tab1}
                filterOption={(input, options) =>
                  options.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              />
            </Form.Item>
            <Form.Item
              name="candidate"
              label="Candidate ID"
              rules={[
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="candidate"
                onChange={changecandidateHandler}
                name="candidate"
                value={candidate1}
                defaultValue={candidate1}
              />
            </Form.Item>
            <Form.Item
              name="ITConsultingFirm"
              label="IT Consulting Firm"
              rules={[
                {
                  validator: valideItConsultingFirm,
                },
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="ITConsultingFirm"
                onChange={changeITConsultingFirmHandler}
                name="ITConsultingFirm"
                value={ITConsultingFirm1}
                defaultValue={ITConsultingFirm1}
              />
            </Form.Item>
            <Form.Item
              name="intermedianryAgency"
              label="Intermediary Agency"
              rules={[
                {
                  validator: valideIntermedianryAgency,
                },
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="intermedianryAgency"
                onChange={changeintermedianryAgencyHandler}
                name="intermedianryAgency"
                value={intermedianryAgency1}
                defaultValue={intermedianryAgency1}
              />
            </Form.Item>
            <Form.Item name="missionStart" label="Mission Start" rules={[]}>
              <DatePicker
                initialValues={moment("2020/01/01", "YYYY/MM/DD")}
                format="YYYY/MM/DD"
                onChange={changemissionStartHandler}
                name="missionStart"
                value={missionStart1}
                defaultValue={moment(missionStart1)}
              />
            </Form.Item>
            <Form.Item
              name="missionEnd"
              label="Mission End"
              rules={[
                {
                  validator: valideMissionEnd,
                },
              ]}
            >
              <DatePicker
                initialValues={moment("2020/01/01", "YYYY/MM/DD")}
                format="YYYY/MM/DD"
                onChange={changemissionEndHandler}
                name="missionEnd"
                value={missionEnd1}
                defaultValue={moment(missionEnd1)}
              />
            </Form.Item>
            <Form.Item name="skills" label="Skills">
              <Select
                mode="multiple"
                style={{ width: "100%" }}
                onChange={changeskillsHandler}
                defaultValue={skills1.map(({ name }) => name)}
                optionLabelProp="label"
                options={tab}
              />
            </Form.Item>
            <Form.Item
              name="jobTitle"
              label="JobTitle"
              rules={[
                {
                  validator: validejobTitle,
                },
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="jobTitle"
                onChange={changejobTitleHandler}
                name="jobTitle"
                value={jobTitle1}
                defaultValue={jobTitle1}
              />
            </Form.Item>
            <Form.Item
              name="dailyRate"
              label="Daily Rate"
              rules={[
                {
                  type: "number",
                  min: 0,
                  max: 9999,
                  required: true,
                  message: "the daily rate must be a number!",
                },
              ]}
            >
              <Input
                onChange={changedailyRateHandler}
                name="dailyRate"
                value={dailyRate1}
                defaultValue={dailyRate1}
              />
            </Form.Item>
            <Form.Item
              name="city"
              label="City"
              rules={[
                {
                  validator: valideCity,
                },
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="city"
                onChange={changecityHandler}
                name="city"
                value={city1}
                defaultValue={city1}
              />
            </Form.Item>
            <Form.Item
              name="missionProjectDescription"
              label="Mission Project Description"
              rules={[
                {
                  validator: valideMissionProjectDescription,
                },
                {
                  max: 3000,
                  message: "Length must be less then 3000 characters!",
                },
              ]}
            >
              <Input
                type="missionProjectDescription"
                onChange={changemissionProjectDescriptionHandler}
                name="missionProjectDescription"
                value={missionProjectDescription1}
                defaultValue={missionProjectDescription1}
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Save edit
              </Button>
            </Form.Item>
          </Form>
        ) : null}
        {loading ? <LoadingOutlined className={classes.icon2} /> : null}
        {!loading && !data ? (
          <Alert message="Please check the ID!" type="error" showIcon />
        ) : null}
      </Card>
    </div>
  );
}
EditMissedDeal.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
};
EditMissedDeal.defaultProps = {
  match: () => {},
};
export default withRouter(EditMissedDeal);
