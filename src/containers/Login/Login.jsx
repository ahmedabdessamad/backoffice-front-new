/* eslint-disable no-use-before-define */
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import Cookies from "universal-cookie";
import { Form, Input, Button, Checkbox, message } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/react-hooks";
import useForm from "../../hooks/handlers";
import Spinner from "../../components/Spinner/Spinner";
import loginMutation from "./loginMutation";
import authAction from "../../redux/auth/actions";
import classes from "./components.module.scss";
import { cookieSaveOpt } from "../../configs/domainConfigs";
import runtimeVars from "../../configs/runTimeVars";

const { login } = authAction;

const cookie = new Cookies();

function Login() {
  const dispatch = useDispatch();

  const isLoggedIn = useSelector((state) => state.AuthReducer.token);

  const [values, handleChange, handleSubmit] = useForm(loginUserCallback, {
    email: "",
    password: "",
  });

  const [loginUser, { called, loading }] = useMutation(loginMutation, {
    onCompleted: ({ login: loginData }) => {
      localStorage.setItem("token", loginData.token);
      cookie.set("AUTH_TOKEN", loginData.token, cookieSaveOpt);
      dispatch(login(loginData));
    },
    onError: (error) => {
      if (error.graphQLErrors[0].message === "wrong password!") {
        message.error("wrong password");
      } else {
        message.error("Oops Something went wrong");
      }
    },
    variables: values,
  });

  function loginUserCallback() {
    loginUser();
  }

  const [redirectToReferrer, setRedirectToReferrer] = React.useState(false);
  React.useEffect(() => {
    if (isLoggedIn) {
      setRedirectToReferrer(true);
    }
  }, [isLoggedIn]);

  if (redirectToReferrer) {
    return <Redirect to={{ pathname: "/home" }} />;
  }

  if (called && loading) return <Spinner />;
  // if (error) return <p>Error...</p>;

  return (
    <div className={classes.form}>
      <h1> Login to access your dashboard</h1>
      <Form
        name="normal_login"
        initialValues={{ remember: true }}
        onFinish={() => handleSubmit()}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              type: "email",
              message: "Please input your email!",
            },
          ]}
          onChange={handleChange}
        >
          <Input
            name="email"
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Email"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Please input your Password!" }]}
          onChange={handleChange}
        >
          <Input
            name="password"
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>

        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <span>
              <Checkbox>Remember me</Checkbox>
            </span>
          </Form.Item>

          <Link to="/forgotPassword" className="login-form-forgot" href="#">
            Forgot Password ?
          </Link>
        </Form.Item>

        <Form.Item>
          <div>
            <Button type="primary" htmlType="submit">
              Log in
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Login;
