import React from "react";
import { useQuery } from "@apollo/react-hooks";
import * as PropTypes from "prop-types";
import { Typography } from "antd";
import { FlagOutlined, LoadingOutlined } from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import { useSelector } from "react-redux";
import { FETCH_FORM_QUERY } from "./utile/queries";
import classes from "./FormListComponent.module.scss";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import TableFormAnswersGlobal from "../../components/Form/TableFormAnswersGlobal";

const { Title } = Typography;

function FormAnswersListComponent() {
  const user = useSelector((state) => state.AuthReducer.user);

  const { loading, data } = useQuery(FETCH_FORM_QUERY);
  let data1 = [];
  if (data) {
    data1 = data.forms.map((form) => {
      const splitDate3 = form.createdAt.split("T");
      const createDate = splitDate3[0];
      return {
        key: form.formID,
        formID: form.formID,
        subject: form.subject,
        creator: form.creator,
        createdAt: createDate,
      };
    });
  }

  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Forms Answers List
          </Title>
          <FlagOutlined className={classes.icon}> </FlagOutlined>
        </div>

        <br />
        {data ? (
          <TableFormAnswersGlobal
            forms={data1}
            setSelectedKeys={() => {}}
            selectedKeys=""
            confirm={() => {}}
            clearFilters={() => {}}
            user={user}
          />
        ) : /* <Card style={{ background: "#d9d9d9" }}>
            <div className="site-card-wrapper">
              <Row gutter={16}>
                {data1.map((x, i) => {
                  console.log("xxxxxxxxxxxxxxx", x);
                  return (
                    <Col span={8}>
                      <Card
                        title={x.subject}
                        bordered={false}
                        className={classes.card}
                      >
                        <div>
                          <h1>Created at: </h1>
                          {x.createdAt}
                          <h1>Creator: </h1>
                          {x.creator.email}
                          <h1>Number of Answers: </h1>00
                        </div>
                        <h1> Answers: </h1>
                        <Link to={`${url}/oneFormAnswers/${x.formID}`}>
                          <SendOutlined className={classes.icon3} />
                        </Link>
                      </Card>
                    </Col>
                  );
                })}
              </Row>
            </div>
          </Card> */
        null}
        {loading ? <LoadingOutlined className={classes.icon2} /> : null}
      </ContentContainer>
    </ContentWrapper>
  );
}

FormAnswersListComponent.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(FormAnswersListComponent);
