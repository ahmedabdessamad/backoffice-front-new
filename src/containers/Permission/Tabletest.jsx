import React, { useState } from "react";
import {
  Table,
  Tag,
  message,
  Popconfirm,
  Modal,
  Form,
  Input,
  Badge,
  Button,
} from "antd";
import { EditOutlined, PlusOutlined, ZoomInOutlined } from "@ant-design/icons";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import PermissionDetails from "./PermissionDetails";
import EditPermission from "./EditPermission";
import AddEditPermission from "./AddEditPermission";
import classes from "./Permission.module.scss";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import Spinner from "../../components/Spinner/Spinner";
import GetUsersNumber from "../User/GetUsersNumber";
import { GET_PERMISSIONS } from "./queries";
import { deletePermissionMutation, editPermissionMutation } from "./mutations";
import ModalDetails from "../../components/Modal/ModalDetails";
import ModalUpdateCandidate from "../../components/Modal/ModalUpdateCandidate";
import permissionsAction from "../../redux/permissions/actions";

const { togglePermissionModal } = permissionsAction;

// import AddPermission from "./AddPermission";

export default function Permissions() {
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);
  const [visibleEdit, setVisibleEdit] = useState(false);
  const [permission, setPermission] = useState({});
  const { loading, data, error } = useQuery(GET_PERMISSIONS);
  let permissions = null;
  const total = null;
  if (data) {
    permissions = data.permissions.map((pr) => ({
      id: pr.id,
      name: pr.name,
      tabs: pr.tabs,
    }));
    console.log("permissionspermissionspermissions", permissions);
    // total = data.timesheets && data.timesheets[0] && data.timesheets[0].total;
  }
  const getDetail = (per) => {
    setPermission(per);
    setVisible(true);
  };
  const getEdit = (per) => {
    setPermission(per);
    setVisibleEdit(true);
  };
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      dataIndex: "key",
      key: "key",
      render: (key, permission) => (
        <div style={{ display: "flex" }}>
          <ZoomInOutlined
            className={classes.icon2}
            onClick={() => getDetail(permission)}
          />

          <EditOutlined
            onClick={() => getEdit(permission)}
            className={classes.icon}
          />
        </div>
      ),
    },
  ];
  return (
    <div>
      <PermissionDetails
        visible={visible}
        permission={permission}
        setVisible={setVisible}
      />
      <EditPermission
        visibleEdit={visibleEdit}
        permission={permission}
        setVisibleEdit={setVisibleEdit}
      />
      <AddEditPermission />
      <ContentWrapper style={{ height: "100vh" }}>
        <ContentContainer>
          <div className={classes.title}>
            <h3>Permissions </h3>
            <div>
              <Button
                type="primary"
                icon={<PlusOutlined />}
                onClick={() => dispatch(togglePermissionModal())}
              >
                Add New Permission
              </Button>
            </div>
          </div>
          <Table
            columns={columns}
            dataSource={!error && permissions}
            scroll
            size="big"
            loading={loading}
          />
        </ContentContainer>
      </ContentWrapper>
    </div>
  );
}
