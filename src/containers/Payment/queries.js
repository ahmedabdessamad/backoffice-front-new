import { gql } from "graphql.macro";

export const GET_BATCHPAYMENTS = gql`
  query batchPayments($validationStatus: [validation!]) {
    batchPayments(validationStatus: $validationStatus) {
      account {
        accountID
      }
      batchPaymentID
      dateString
      date
      payments {
        paymentID
        invoice {
          invoiceID
        }
      }
      type
      status
      validationStatus
      totalAmount
      updatedDateUTC
      isReconciled
      updatedAt
    }
  }
`;
export const BATCHPAYMENT_DETAILS = gql`
  query batchPayment($batchPaymentID: ID!) {
    batchPayment(batchPaymentID: $batchPaymentID) {
      account {
        accountID
      }
      batchPaymentID
      dateString
      date
      payments {
        paymentID
        invoice {
          invoiceID
        }
        amount
      }
      type
      status
      validationStatus
      totalAmount
      updatedDateUTC
      isReconciled
    }
  }
`;
export const VERIF_DATA = gql`
  query verifData {
    verifData
  }
`;
export const GET_BATCHPAYMENTS_BY_FILTER = gql`
  query getBatchPaymentByFilter($input: batchPaymentFilter!) {
    getBatchPaymentByFilter(filter: $input) {
      account {
        accountID
      }
      batchPaymentID
      dateString
      date
      payments {
        paymentID
        invoice {
          invoiceID
        }
        amount
      }
      type
      status
      validationStatus
      totalAmount
      updatedDateUTC
      isReconciled
      organisation
    }
  }
`;
export const GET_BATCHPAYMENTS_PER_PAGE = gql`
  query getBatchPaymentsPerPage($page: Int, $limit: Int) {
    getBatchPaymentsPerPage(page: $page, limit: $limit) {
      account {
        accountID
      }
      batchPaymentID
      dateString
      date
      payments {
        paymentID
        invoice {
          invoiceID
        }
        amount
      }
      type
      status
      validationStatus
      totalAmount
      updatedDateUTC
      isReconciled
    }
  }
`;
export const GET_LAST_UPDATE = gql`
  query getLastUpdate {
    getLastUpdate
  }
`;
