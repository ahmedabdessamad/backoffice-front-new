import { gql } from "graphql.macro";

const STOP_MAILING = gql`
  mutation StopMailing($email: String!, $services: [String]) {
    stopMailing(email: $email, services: $services)
  }
`;

const RESUME_MAILING = gql`
  mutation ResumeMailing($email: String!) {
    resumeMailing(email: $email)
  }
`;

const SEND_TIMESHEET_EMAIL = gql`
  mutation sendTimesheetEmail(
    $service: String!
    $action: String!
    $placementId: Int
    $month: Int
    $year: Int
    $timesheetId: Int
  ) {
    sendTimesheetEmail(
      service: $service
      action: $action
      placementId: $placementId
      month: $month
      year: $year
      timesheetId: $timesheetId
    )
  }
`;

export { STOP_MAILING, RESUME_MAILING, SEND_TIMESHEET_EMAIL };
