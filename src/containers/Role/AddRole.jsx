import React from "react";
import { message, Modal, Form, Input, Button } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/react-hooks";
import { GET_ROLES } from "./queries";
import { addRoleMutation } from "./mutations";

export default function AddRole() {
  const [form] = Form.useForm();
  const [visible, setVisible] = React.useState(false);
  const [role, setRole] = React.useState("");
  const [addRole] = useMutation(addRoleMutation, {
    onCompleted: () => message.success("Role added"),
    refetchQueries: [{ query: GET_ROLES }],
  });
  function handleOk() {
    setVisible(false);
    addRole({
      variables: {
        role,
      },
    });
  }
  return (
    <div>
      <Button
        type="primary"
        icon={<PlusOutlined />}
        onClick={() => setVisible(true)}
      >
        Add Role
      </Button>
      <Modal
        title="ADD ROLE"
        visible={visible}
        onOk={handleOk}
        onCancel={() => setVisible(false)}
      >
        <Form form={form}>
          <Form.Item label="Role Name">
            <Input
              placeholder="enter a Role"
              onChange={(e) => setRole(e.target.value)}
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
