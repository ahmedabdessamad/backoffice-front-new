import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Popover, Avatar } from "antd";
import { useApolloClient } from "@apollo/react-hooks";
import authActions from "../../redux/auth/actions";
import classes from "./header.module.scss";
import Settings from "./Settings";

const { logout } = authActions;

export default function UserMenu() {
  const dispatch = useDispatch();
  const { cache } = useApolloClient();
  const user = useSelector((state) => state.AuthReducer.user);
  const [visible, setVisibility] = React.useState(false);

  const logoutHandler = () => {
    dispatch(logout(cache));
  };

  const handleVisibleChange = () => {
    setVisibility(!visible);
  };

  const menu = (
    <div className={classes.userMenuDropDown}>
      <Settings />
      <a className={classes.userMenuLink} href="#" onClick={logoutHandler}>
        Logout
      </a>
    </div>
  );

  return (
    <div>
      <Popover
        trigger="click"
        content={menu}
        visible={visible}
        onVisibleChange={handleVisibleChange}
        arrowPointAtCenter
        placement="bottomLeft"
      >
        <Avatar>{user.username[0].toUpperCase()}</Avatar>
      </Popover>
    </div>
  );
}
