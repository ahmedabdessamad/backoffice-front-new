import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { useParams } from "react-router-dom";
import { message, Button } from "antd";
import { DownloadOutlined } from "@ant-design/icons";
import { generateSepaMutation } from "./mutations";
import { GET_BATCHPAYMENTS_BY_FILTER } from "./queries";

function GenerateSepa() {
  const { batchPaymentID } = useParams();

  const [generateSepa, { loading: sepaLoading }] = useMutation(
    generateSepaMutation,
    {
      variables: { batchPaymentID },
      onCompleted: (data) => {
        message.success("XML file generated");

        window.open(data.generateSepa.url, "_blank");
      },
      onError: (error) => {
        if (error.message === "GraphQL error: NO BATCHPAYMENT EXISTS") {
          message.error(
            "this batch payment is only available for rejection due to invalid payment"
          );
        } else {
          message.error("Oops Something went wrong");
        }
      },
      refetchQueries: [
        {
          query: GET_BATCHPAYMENTS_BY_FILTER,
          variables: {
            input: {
              validationStatus: ["PENDING"],
            },
          },
        },
        {
          query: GET_BATCHPAYMENTS_BY_FILTER,
          variables: {
            input: {
              validationStatus: ["EXPORTED"],
            },
          },
        },
      ],
    }
  );

  const handleDownload = () => generateSepa();

  return (
    <Button
      danger
      icon={<DownloadOutlined />}
      loading={sepaLoading}
      style={{ marginRight: 40 }}
      onClick={handleDownload}
    >
      Export to XML
    </Button>
  );
}
export default GenerateSepa;
