/* eslint-disable consistent-return */
/* eslint-disable no-extra-boolean-cast */
/* eslint-disable no-shadow */
/* eslint-disable no-plusplus */
import React, { useState } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { useParams, useHistory } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import {
  Typography,
  Table,
  Button,
  Modal,
  message,
  Divider,
  Checkbox,
} from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import ContentWrapper from "../../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../../components/ContentContainer/ContentContainer";
// import spinner from "../../components/Spinner";
import { BATCHPAYMENT_DETAILS, GET_BATCHPAYMENTS_BY_FILTER } from "../queries";
import GenerateSepa from "../GenerateSepa";
import {
  validateBatchPaymentMutation,
  DISCARD_BATCH_PAYMENT,
} from "../mutations";
import classes from "../batchPayment.module.scss";

const HIGH_AMOUNT = 20000;
const { Text } = Typography;

export default function Details(props) {
  const { title, path } = props;
  const history = useHistory();
  const { batchPaymentID } = useParams();
  const [visible, setVisible] = useState(false);
  const [visibleHighAmountModal, setVisibleHighAmountModal] = useState(false);
  const [checked, setChecked] = useState(false);

  const [validatedAmounts, setValidatedAmounts] = useState([]);

  const [validateBatchPayment, { loading: vloading }] = useMutation(
    validateBatchPaymentMutation,
    {
      variables: { batchPaymentID },
      onCompleted: (data) => {
        return window.open(data.validateBatchPayment, "_self");
      },
    }
  );
  const [discardBatchPayment] = useMutation(DISCARD_BATCH_PAYMENT, {
    variables: { batchPaymentID },
    onCompleted: () => {
      history.push(path);
    },
    refetchQueries: [
      {
        query: GET_BATCHPAYMENTS_BY_FILTER,
        variables: {
          input: {
            validationStatus: ["PENDING"],
          },
        },
      },
      {
        query: GET_BATCHPAYMENTS_BY_FILTER,
        variables: {
          input: {
            validationStatus: ["REJECTED"],
          },
        },
      },
    ],
  });

  const { loading, error, data } = useQuery(BATCHPAYMENT_DETAILS, {
    variables: {
      batchPaymentID,
    },
    fetchPolicy: "cache-and-network",
  });

  if (loading) return <spinner />;
  if (error) return <p>Error...</p>;

  const dataSource = data.batchPayment.payments.map((payment) => {
    return {
      key: payment.paymentID,
      Invoice_ID: payment.invoice.invoiceID,
      IBAN: "GB69HBUK40127685143535",
      BIC: "HBUKGB4B",
      Country_Code: "GB",
      Amount: payment.amount,
    };
  });

  const highAmountsData = dataSource.filter((elm) => elm.Amount >= HIGH_AMOUNT);

  const columns = [
    {
      title: "Invoice_ID",
      dataIndex: "Invoice_ID",
      key: "Invoice_ID",
      width: 150,
    },
    {
      title: "IBAN",
      dataIndex: "IBAN",
      key: "IBAN",
      // width: 250,
    },
    {
      title: "BIC",
      dataIndex: "BIC",
      key: "BIC",
    },
    {
      title: "Country_Code",
      dataIndex: "Country_Code",
      key: "Country_Code",
    },
    {
      title: "Amount",
      dataIndex: "Amount",
      key: "Amount",
    },
  ];
  function onChange(e) {
    setChecked(e.target.checked);
  }

  const validateHighAmounts = (highAmountsData, validatedAmounts) => {
    return highAmountsData.every(({ key }) => validatedAmounts.includes(key));
  };

  const renderHighAmountsModal = () => (
    <Modal
      visible={visibleHighAmountModal}
      title="We notice some hight amounts "
      icon={<ExclamationCircleOutlined />}
      okText="Proceed"
      width={800}
      onOk={() => {
        if (validateHighAmounts(highAmountsData, validatedAmounts)) {
          validateBatchPayment();
          setValidatedAmounts([]);
          setVisibleHighAmountModal(false);
        } else {
          message.warning("Please check all high amounts");
        }
      }}
      onCancel={() => {
        setVisibleHighAmountModal(false);
        setValidatedAmounts([]);
      }}
    >
      <div>
        <p>Please make sure it is all Correct</p>

        <Table
          rowSelection={{
            type: "checkbox",
            onChange: (selectedAmounts) => {
              setValidatedAmounts(selectedAmounts);
            },
          }}
          dataSource={highAmountsData}
          columns={columns}
          pagination={false}
          scroll={{ y: 240 }}
        />
      </div>
    </Modal>
  );

  const proceedHandler = () => {
    if (!checked) {
      return message.warning("Please confirm that you want to procees");
    }

    if (!!highAmountsData.length) {
      setVisible(false);
      setVisibleHighAmountModal(true);
      return;
    }

    setVisible(false);
    return validateBatchPayment();
  };

  const renderElement = () => {
    const status = data.batchPayment.validationStatus;
    console.log(status);

    if (status === "EXPORTED") {
      return (
        <div className={classes.centredBtns}>
          <div>
            <GenerateSepa />
          </div>
        </div>
      );
    }
    if (status === "REJECTED") {
      return null;
    }
    if (status === "PENDING") {
      return (
        <div className={classes.centredBtns}>
          <div>
            <Button
              type="danger"
              className={classes.btn_elm}
              loading={vloading}
              onClick={() => {
                setVisible(true);
              }}
            >
              Send to HSBC
            </Button>

            <GenerateSepa />
            <div style={{ marginTop: "15px" }} className={classes.centredBtns}>
              <Button type="link" onClick={discardBatchPayment}>
                Discard payment
              </Button>
            </div>
          </div>
        </div>
      );
    }
  };

  return (
    <ContentWrapper>
      <ContentContainer>
        <h1> {title}</h1>
        <Modal
          visible={visible}
          onOk={proceedHandler}
          onCancel={() => setVisible(false)}
          okText="Proceed"
        >
          <h1 style={{ color: "rgb(178,34,34)" }}>
            Are you sure to proceed payment
          </h1>
          <p style={{ textAlign: "center" }}>There is no turnning back...</p>

          <h3> Payment Recap</h3>
          <Text strong>Total Amount:</Text>
          {data.batchPayment.totalAmount}
          <br />
          <Text strong>
            Number of Transactions: {data.batchPayment.payments.length}
          </Text>
          <br />
          <Text strong>Details:</Text>
          <br />
          <Table
            dataSource={dataSource}
            columns={columns}
            pagination={false}
            scroll={{ y: 200 }}
          />

          <Checkbox onChange={onChange}>
            I confirm I want to proceed payment
          </Checkbox>
        </Modal>
        {renderHighAmountsModal()}
        <div>
          <Text strong> Batch payment created on:</Text>

          {moment(data.batchPayment.dateString).format("YYYY-MM-DD")}

          <br />
          <Text strong> Source Company :</Text>
          <Text type="danger"> Club Freelance SaS</Text>
          <br />
          <Text strong> Message ID :</Text>
          {data.batchPayment.batchPaymentID}
          <br />
          <Text strong>
            Number of Transactions : {data.batchPayment.payments.length}
          </Text>
          <br />
          <Text strong> Total Amount : </Text>
          {data.batchPayment.totalAmount}
          <br />
        </div>
        <Divider orientation="left">Details</Divider>
        <div
          style={{
            paddingRight: 40,
            paddingBottom: 50,
            paddingLeft: 50,
          }}
        >
          <Table
            dataSource={dataSource}
            columns={columns}
            pagination={false}
            scroll={{ y: 240 }}
          />
        </div>
        <div>{renderElement()}</div>
      </ContentContainer>
    </ContentWrapper>
  );
}
Details.propTypes = {
  title: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};
