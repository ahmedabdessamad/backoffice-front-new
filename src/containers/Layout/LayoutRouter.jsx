import React, { lazy, Suspense } from "react";
import { Route, useRouteMatch, Switch, Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "../../components/Spinner/Spinner";
import { isUserAuthorized } from "../../helpers/authorization/permissionsCheckHelpers";
import bhEntityActions from "../../redux/bhEntity/actions";

const { setBhEntity } = bhEntityActions;
const routes = [
  {
    path: "",
    component: lazy(() => import("../HomePage/HomePage")),
    exact: true,
  },
  {
    path: "users",
    component: lazy(() => import("../User/Users")),
    exact: true,
    permissions: { USERS: 4 },
  },
  {
    path: "permissions",
    component: lazy(() => import("../Permission/Permissions")),
    exact: true,
    permissions: { PERMISSIONS: 4 },
  },
  {
    path: "xeroLogin",
    component: lazy(() => import("../Payment/XeroLogin")),
    exact: true,
    permissions: { HSBCCONNECTOR: 4 },
    /* permissions: [
      { group: "Finance", role: "Admin" },
      { group: "SuperAdmin", role: "Admin" },
    ], */
  },
  {
    path: "sas_validation",
    component: lazy(() => import("../Payment/sas/BatchPayments")),
    exact: true,
    permissions: { SAS: 4 },
    /* permissions: [
      { group: "Finance", role: "Admin" },
      { group: "SuperAdmin", role: "Admin" },
    ], */
  },
  {
    path: "sas_validation/:batchPaymentID",
    component: lazy(() => import("../Payment/sas/BatchPaymentDetails")),
    exact: true,
    permissions: { SAS: 4 },
    /* permissions: [
      { group: "Finance", role: "Admin" },
      { group: "SuperAdmin", role: "Admin" },
    ], */
  },
  {
    path: "sas_logs",
    component: lazy(() => import("../Payment/sas/Logs")),
    exact: true,
    permissions: { SAS: 4 },
    /* permissions: [
      { group: "Finance", role: "Admin" },
      { group: "SuperAdmin", role: "Admin" },
    ], */
  },
  {
    path: "sas_logs/:batchPaymentID",
    component: lazy(() => import("../Payment/sas/LogsDetails")),
    exact: true,
    permissions: { SAS: 4 },
    /* permissions: [
      { group: "Finance", role: "Admin" },
      { group: "SuperAdmin", role: "Admin" },
    ], */
  },
  {
    path: "limited_validation",
    component: lazy(() => import("../Payment/limited/LimitedBatchPayments")),
    exact: true,
    permissions: { LIMITED: 4 },
    /* permissions: [
      { group: "Finance", role: "Admin" },
      { group: "SuperAdmin", role: "Admin" },
    ], */
  },
  {
    path: "limited_validation/:batchPaymentID",
    component: lazy(() =>
      import("../Payment/limited/LimitedBatchPaymentDetails")
    ),
    exact: true,
    permissions: { LIMITED: 4 },

  },

  {
    path: "limited_logs",
    component: lazy(() => import("../Payment/limited/LimitedLogs")),
    exact: true,
    permissions: { LIMITED: 4 },

  },
  {
    path: "limited_logs/:batchPaymentID",
    component: lazy(() => import("../Payment/limited/LimitedLogsDetails")),
    exact: true,
    permissions: { LIMITED: 4 },

  },

  {
    path: "pending_sendouts/:sendoutId",
    component: lazy(() => import("../Sendouts/SendoutDetails")),
    exact: true,
    permissions: { PENDINGSENDOUTS: 6 },

  },
  {
    path: "archived_sendouts/:sendoutId",
    component: lazy(() => import("../Sendouts/SendoutDetails")),
    exact: true,
    permissions: { ARCHIVEDSENDOUTS: 6 },

  },
  {
    path: "pending_sendouts",
    component: lazy(() => import("../Sendouts/PendingSendouts")),
    exact: true,
    permission: { PENDINGSENDOUTS: 6 },

  },
  {
    path: "archived_sendouts",
    component: lazy(() => import("../Sendouts/ArchivedSendouts")),
    exact: true,
    permissions: { ARCHIVEDSENDOUTS: 6 },

  },
  {
    path: "missedDeal",
    component: lazy(() => import("../MissedDeal/MissedDeal")),
    exact: true,
    permissions: { MISSEDDEAL: 7 },

  },
  {
    path: "missedDeal/add",
    component: lazy(() => import("../MissedDeal/AddMissedDeal")),
    exact: true,
    permissions: { MISSEDDEAL: 6 },

  },
  {
    path: "missedDeal/edit/:id",
    component: lazy(() => import("../MissedDeal/EditMissedDeal")),
    exact: true,
    permissions: { MISSEDDEAL: 6 },

  },
  {
    path: "marketInfo",
    component: lazy(() => import("../MarketInfo/MarketInfo")),
    exact: true,
    permissions: { MARKETINFO: 7 },

  },
  {
    path: "marketInfo/add",
    component: lazy(() => import("../MarketInfo/AddMarketInfo")),
    exact: true,
    permissions: { MARKETINFO: 6 },

  },
  {
    path: "marketInfo/edit/:id",
    component: lazy(() => import("../MarketInfo/EditMarketInfo")),
    exact: true,
    permissions: { MARKETINFO: 6 },

  },
  {
    path: "timesheets",
    component: lazy(() => import("../Timesheets")),
    exact: true,
    permissions: { TIMESHEET: 7 },
  },
  {
    path: "timesheets/timesheets",
    component: lazy(() => import("../Timesheets")),
    exact: true,
    permissions: { TIMESHEETS: 7, TIMESHEET: 7 },
  },
  {
    path: "timesheets/timesheets/:id",
    component: lazy(() =>
      import("../Timesheets/timesheet-single-page/SingleTimesheet")
    ),
    exact: true,
    permissions: { TIMESHEETS: 7 },
    // permissions: [{ group: "SuperAdmin", role: "Admin" }],
  },
  {
    path: "timesheets/reports",
    component: lazy(() => import("../Timesheets")),
    exact: true,
    permissions: { REPORTS: 6 },
  },
  {
    path: "timesheets/placements",
    component: lazy(() => import("../Timesheets")),
    exact: true,
    permissions: { PLACEMENTS: 6 },
  },
  {
    path: "timesheets/logs",
    component: lazy(() => import("../Timesheets")),
    exact: true,
    permissions: { TIMESHEETLOGS: 6 },
  },
  {
    path: "timesheets/mailing",
    component: lazy(() => import("../Timesheets")),
    exact: true,
    permissions: { TIMESHEETEMAILS: 6 },
  },
  {
    path: "form",
    component: lazy(() => import("../Form/FormComponent")),
    exact: true,
    permissions: { FORM: 6 },
  },
  {
    path: "formList",
    component: lazy(() => import("../Form/FormListComponent")),
    exact: true,
    permissions: { FORM: 6 },
  },
  {
    path: "formList/edit/:id",
    component: lazy(() => import("../Form/EditFormComponent")),
    exact: true,
    permissions: { FORM: 6 },
  },
  {
    path: "formList/tobeSent/:id",
    component: lazy(() => import("../Form/FormToBeSentComponent")),
    exact: true,
    permissions: { FORM: 6 },
  },
  {
    path: "formAnswersList",
    component: lazy(() => import("../Form/FormAnswersListComponent")),
    exact: true,
    permissions: { FORM: 6 },
  },
  {
    path: "formAnswersList/oneFormAnswers/:id",
    component: lazy(() => import("../Form/OneFormAnswersComponent")),
    exact: true,
    permissions: { FORM: 6 },
  },
  {
    path: "mailing",
    component: lazy(() => import("../Mailing/index")),
    exact: true,
    permissions: { MAILING: 4 },
  },
  {
    path: "mailing/excludedEmails",
    component: lazy(() => import("../Mailing/index")),
    exact: true,
    permissions: { MAILING: 4 },
  },
  {
    path: "mailing/sentEmails",
    component: lazy(() => import("../Mailing/index")),
    exact: true,
    permissions: { MAILING: 4 },
  },
  {
    path: "logs",
    component: lazy(() => import("../Logs/index")),
    exact: true,
    permissions: { LOGS: 6 },
  },
];

function RestrictedRoute({ children, permissions, ...rest }) {
  const dispatch = useDispatch();
  const BhUrl = window.location.href;
  // console.log("urlllll========>",BhUrl);
  const regex = new RegExp("EntityType");
  const results = regex.exec(BhUrl);
  if (results !== null) {
    const BhUrlSplit1 = BhUrl.split("EntityType=");
    const BhUrlSplit2 = BhUrlSplit1[1].split("&");
    const BhUrlSplit3 = BhUrl.split("EntityID=");
    const BhUrlSplit4 = BhUrlSplit3[1].split("&");

    if (BhUrlSplit2[0] === "Candidate" || BhUrlSplit2[0] === "Client") {
      const ob = { entityType: BhUrlSplit2[0], entityId: BhUrlSplit4[0] };
      dispatch(setBhEntity(ob));
    }
  }

  const { user } = useSelector((state) => state.AuthReducer);
  if (!user) {
    return (
      <Redirect
        to={{
          pathname: "/login",
        }}
      />
    );
  }

  if (!permissions) {
    return <Route {...rest}>{children}</Route>;
  }

  if (permissions && isUserAuthorized(user, permissions)) {
    return <Route {...rest}>{children}</Route>;
  }
  return (
    <Redirect
      to={{
        pathname: "",
      }}
    />
  );
}

export default function LayoutRouter() {
  const { url } = useRouteMatch();
  return (
    <Suspense fallback={<Spinner />}>
      <Switch>
        {routes.map((route) => (
          <RestrictedRoute
            exact={route.exact}
            key={route.path}
            path={`${url}/${route.path}`}
            permissions={route.permissions}
          >
            <route.component />
          </RestrictedRoute>
        ))}
      </Switch>
    </Suspense>
  );
}
