/* eslint-disable import/prefer-default-export */
import { gql } from "graphql.macro";

export const validateBatchPaymentMutation = gql`
  mutation validateBatchPayment($batchPaymentID: ID!) {
    validateBatchPayment(batchPaymentID: $batchPaymentID)
  }
`;
export const generateSepaMutation = gql`
  mutation generateSepa($batchPaymentID: ID!) {
    generateSepa(batchPaymentID: $batchPaymentID) {
      url
    }
  }
`;

export const DISCARD_BATCH_PAYMENT = gql`
  mutation DiscardBatchPayment($batchPaymentID: ID!) {
    discardBatchPayment(batchPaymentID: $batchPaymentID) {
      account {
        accountID
      }
      batchPaymentID
      dateString
      date
      payments {
        paymentID
        invoice {
          invoiceID
        }
        amount
      }
      type
      status
      validationStatus
      totalAmount
      updatedDateUTC
      isReconciled
    }
  }
`;
export const BUILD_CONSENT_URL = gql`
  mutation buildConsentUrl {
    buildConsentUrl
  }
`;
