import React from "react";
import { useSelector } from "react-redux";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../components/ContentContainer/ContentContainer";

export default function HomePage() {
  const user = useSelector((state) => state.AuthReducer.user);
  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <h1>Hello {user.username}</h1>
      </ContentContainer>
    </ContentWrapper>
  );
}
