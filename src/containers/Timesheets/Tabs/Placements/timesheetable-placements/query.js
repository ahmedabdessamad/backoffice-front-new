import { gql } from "graphql.macro";

const TIMESHEETABLE_PLACEMENTS = gql`
  query TimesheetablePlacements(
    $clientID: Int
    $candidateID: Int
    $limit: Int
    $skip: Int
    $ids: [Int]
    $name: String
    $periodStart: Float
    $periodEnd: Float
  ) {
    timesheetablePlacements(
      filter: {
        clientID: $clientID
        candidateID: $candidateID
        limit: $limit
        skip: $skip
        ids: $ids
        name: $name
        withSignataire: true
        periodStart: $periodStart
        periodEnd: $periodEnd
      }
    ) {
      id
      dateAdded
      dateBegin
      dateEnd
      clientCorporation {
        id
        name
      }
      clientContact {
        id
        firstName
        lastName
      }
      candidate {
        id
        firstName
        lastName
      }
      jobOrder {
        id
        title
      }
      signataire {
        placementID
        principalSignataire {
          id
          firstName
          lastName
          email
        }
        secondarySignataires {
          id
          firstName
          lastName
          email
        }
      }
      owner {
        id
        firstName
        lastName
      }
      status
      clientBillRate
      correlatedCustomText1
      payRate
      total
      signataireOnHold
      consultantEmail
      ownerEmail
      project
    }
  }
`;

export default TIMESHEETABLE_PLACEMENTS;
