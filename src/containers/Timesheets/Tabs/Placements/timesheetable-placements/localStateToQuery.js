/**
 *
 * @param obj
 * @returns {{}}
 */
const localStateToQuery = (obj) => {
  const res = {};
  if (!obj || !Object.keys(obj).length) return {};

  if (obj.clientID) res.clientID = Number.parseInt(obj.clientID, 10);
  if (obj.candidateID) res.candidateID = Number.parseInt(obj.candidateID, 10);
  if (obj.limit || obj.limit === 0) res.limit = Number.parseInt(obj.limit, 10);
  else res.limit = 10;
  if (obj.skip || obj.skip === 0) res.skip = Number.parseInt(obj.skip, 10);
  if (obj.id) {
    const values = obj.id.split(" ");
    res.ids = values.map((v) => Number.parseInt(v, 10));
  }

  // if (obj.date) {
  //   const splitted = obj.date.split("/");
  //   if (splitted.length === 2) {
  //     const month = splitted[0];
  //     const year = splitted[1];
  //     res.month = month && Number.parseInt(month, 10);
  //     res.year = year && Number.parseInt(year, 10);
  //   }
  // }

  return res;
};

export default localStateToQuery;
