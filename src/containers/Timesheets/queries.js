import { gql } from "graphql.macro";

const SINGLE_TIMESHEET_QUERY = gql`
  query Timesheet($id: ID!) {
    timesheet(id: $id) {
      id
      title
      client {
        id
        firstName
        lastName
        name
        email
        companyName
      }
      candidate {
        id
        firstName
        lastName
        name
        email
        city
      }
      placementID
      dateAdded
      month
      year
      dateLastUpdate
      status
      selectedDays {
        day
        hours
        minutes
      }
      specialHours {
        date
        hours
        minutes
        type
      }
      total
      signature
      conversation {
        timesheetID
        dateAdded
        dateLastUpdate
        messages {
          _id
          text
          date
          speaker
        }
        comment
      }
    }
  }
`;

const HOLIDAYS = gql`
  query Holidays($year: Int!, $month: Int!, $country: String!) {
    holidays(filter: { year: $year, month: $month, country: $country }) {
      month
      year
      country
      holidays {
        day
        name
      }
    }
  }
`;

export { SINGLE_TIMESHEET_QUERY, HOLIDAYS };
