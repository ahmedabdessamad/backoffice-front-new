import React, { useEffect, useState } from "react";
import { Modal, Checkbox, Form, Space, Select } from "antd";
import classes from "./Permission.module.scss";
import { SUBTABS_NAMES, TAB_NAMES } from "../../configs/constant";
import "./style.css";

function PermissionDetails({ permission, visible, setVisible }) {
  const [form] = Form.useForm();
  const [tabsOptions, setTabsOptions] = useState(Object.keys(SUBTABS_NAMES));
  const [fieldsTabs, setFieldsTabs] = useState({});
  const [subTabsPermission, setSubTabsPermission] = useState([]);
  const [thereIsEdit, setThereIsEdit] = useState(false);

  const setInitFieldsTabs = () => {
    const { tabs } = permission;
    let initState = {};

    tabs
      .filter((tab) => tab.parent === null)
      .forEach(({ name }, index) => {
        initState = {
          ...initState,
          [index]: TAB_NAMES[name],
        };
      });

    setFieldsTabs(initState);
  };

  const permissionType = (perm) => {
    const permissionTypeTab = [];
    if ((perm & 4) === 4) permissionTypeTab.push("Admin");
    if ((perm & 2) === 2) permissionTypeTab.push("Editor");
    if ((perm & 1) === 1) permissionTypeTab.push("Reader");
    return permissionTypeTab;
  };

  const getChildren = (tabs, parentName) => {
    const children = {};
    tabs
      .filter(({ parent }) => parent === parentName)
      .forEach((child) => {
        const childName = SUBTABS_NAMES[TAB_NAMES[parentName]][child.name];
        children[childName] = permissionType(child.permission);
      });
    return children;
  };

  const getDefaultTabs = (tabs = []) => {
    const t = tabs
      .filter((tab) => tab.parent === null)
      .map(({ name, permission: parentPermission }) => {
        const children = getChildren(tabs, name);
        if (Object.keys(children).length) {
          // has children
          return {
            tabName: TAB_NAMES[name],
            ...children,
          };
        }
        // has no children
        return {
          tabName: TAB_NAMES[name],
          [TAB_NAMES[name]]: permissionType(parentPermission),
        };
      });
    return t;
  };

  const renderSubTabs = (tab, key) => {
    if (Object.keys(SUBTABS_NAMES[tab]).length !== 0) {
      return Object.values(SUBTABS_NAMES[tab]).map((subtab) => {
        return (
          <div style={{ display: "flex" }} key={subtab}>
            <div className="labelCheckboxEdit">
              <Form.Item name={[key, subtab]} label={subtab}>
                <Checkbox.Group
                  options={["Admin", "Editor", "Reader"]}
                  disabled
                />
              </Form.Item>
            </div>
          </div>
        );
      });
    }
    return (
      <div className="labelCheckboxEdit" key={tab}>
        <Form.Item name={[key, tab]}>
          <Checkbox.Group options={["Admin", "Editor", "Reader"]} disabled />
        </Form.Item>
      </div>
    );
  };

  useEffect(() => {
    if (permission && thereIsEdit === false) {
      form.resetFields();
      // set initial fields
      if (Array.isArray(permission.tabs)) setInitFieldsTabs();
      // set initial subTabs
      setSubTabsPermission(permission.tabs);
    }

    if (subTabsPermission) {
     // console.log("subTabsPermission edit =====>", subTabsPermission);
    }
  }, [permission, subTabsPermission, thereIsEdit]);

  return (
    <div>
      <Modal
        title="Permission"
        visible={visible}
        onOk={() => {
          form.validateFields().then(() => {
            form.resetFields();
            // reset state too
            setFieldsTabs({});
            setTabsOptions(Object.keys(SUBTABS_NAMES));
            setVisible(false);
          });
        }}
        onClose={() => {
          setVisible(false);
          setThereIsEdit(false);
        }}
        onCancel={() => {
          setVisible(false);
          setThereIsEdit(false);
        }}
        width="60%"
      >
        <Form
          form={form}
          initialValues={{
            name: permission.name,
            tabs: getDefaultTabs(permission.tabs), // tabs: [{ tabName: "User management" , User: ["Admin"] , Permission: ["Admin", "Reader"] }]
          }}
        >
          <div className={classes.field}>
            <Form.Item label="Name" name="name">
              {permission.name}
            </Form.Item>
          </div>
          <Form.List name="tabs">
            {(fields) => {
              return (
                <div>
                  {fields.map((field) => {
                    return (
                      <Space
                        key={field.key}
                        style={{ display: "flex", marginBottom: 8 }}
                        align="start"
                      >
                        <div>
                          <Form.Item
                            {...field}
                            label="Tab"
                            name={[field.name, "tabName"]}
                            fieldKey={field.fieldKey}
                            className="permission_add_label edit"
                          >
                            <Select style={{ width: 200 }} disabled />
                          </Form.Item>
                          {fieldsTabs[field.name]
                            ? renderSubTabs(
                                fieldsTabs[field.name],
                                field.fieldKey
                              )
                            : null}
                        </div>
                      </Space>
                    );
                  })}
                </div>
              );
            }}
          </Form.List>
        </Form>
      </Modal>
    </div>
  );
}
export default PermissionDetails;
