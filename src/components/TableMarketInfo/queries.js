import { gql } from "graphql.macro";

const FETCH_MARKETINFO_BY_COLUMN_QUERY = gql`
  query findMarket($searched: [arrayOfSearchMarket]) {
    findMarket(searched: $searched) {
      marketInfoID
      companyId
      companyName
      jobTitle
      contact
      contactName
      email
      phone
      industry {
        name
      }
      categories {
        name
      }
      description
      createdAt
      creator {
        email
      }
    }
  }
`;
export default FETCH_MARKETINFO_BY_COLUMN_QUERY;
