import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Switch } from "antd";
import sendoutAction from "../../redux/sendout/actions";
import EditableText from "../../components/EditableText/EditableText";

const { setTextField, setDisplayTJM } = sendoutAction;

export default function () {
  const { sendout: currentSendout, displayTJM } = useSelector(
    (state) => state.SendoutReducer
  );
  const dispatch = useDispatch();

  const saveHandler = (value) => {
    dispatch(setTextField({ fieldName: "payRate", value }));
  };

  const switchChangeHandler = (checked) => {
    dispatch(setDisplayTJM(checked));
  };

  return (
    <div style={{ display: "flex" }}>
      <p style={{ padding: "5px 12px" }}>TJM:</p>
      <EditableText
        editable={displayTJM}
        text={currentSendout.payRate || "N/A"}
        inputType="number"
        onSave={(value) => saveHandler(value)}
      />
      <div>
        <Switch
          checkedChildren="Display"
          unCheckedChildren="Hide"
          defaultChecked
          onChange={switchChangeHandler}
        />
      </div>
    </div>
  );
}
