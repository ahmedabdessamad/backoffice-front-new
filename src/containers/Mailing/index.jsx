import React from "react";
import * as PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { Typography, Tabs } from "antd";
import classes from "./Mailing.module.scss";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import Excluded from "./Tabs/Exluded";
import Sent from "./Tabs/Sent";

const { Title } = Typography;
const { TabPane } = Tabs;

function Mailing(props) {
  // set up url
  const { location, history } = props;
  const { pathname } = location;
  const splitted = pathname.split("/");

  let defaultLocation = "excludedEmails";
  if (splitted.length === 4) defaultLocation = splitted[3];
  else history.replace("/home/mailing/excludedEmails");

  const navigate = (key) => {
    history.push(`/home/mailing/${key}`);
  };

  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Mailing
          </Title>
        </div>
        <Tabs defaultActiveKey={defaultLocation} onChange={navigate}>
          <TabPane tab="Excluded Emails" key="excludedEmails">
            <Excluded />
          </TabPane>
          <TabPane tab="Sent Emails" key="sentEmails">
            <Sent />
          </TabPane>
        </Tabs>
      </ContentContainer>
    </ContentWrapper>
  );
}

Mailing.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
};
export default withRouter(Mailing);
