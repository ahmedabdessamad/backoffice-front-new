import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_USERSNUMBER } from "./queries";
import Spinner from "../../components/Spinner/Spinner";

export default function GetUsersNumber(id, field) {
  const { loading, error, data } = useQuery(GET_USERSNUMBER, {
    variables: { id, field },
  });
  if (loading) return <Spinner />;
  if (error) return <p>...error</p>;
  return data.usersNumber;
}
