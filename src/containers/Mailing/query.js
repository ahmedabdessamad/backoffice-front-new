import { gql } from "graphql.macro";

const EXCLUDED_EMAILS = gql`
  query ExcludedEmails(
    $limit: Int
    $skip: Int
    $startDate: Float
    $endDate: Float
  ) {
    excludedEmails(
      filter: {
        limit: $limit
        skip: $skip
        startDate: $startDate
        endDate: $endDate
      }
    ) {
      excludes {
        email
        services
        excludedAt
        platform
      }
      total
    }
  }
`;

const SENT_EMAILS = gql`
  query SentEmails(
    $limit: Int
    $skip: Int
    $startDate: Float
    $endDate: Float
    $services: [String]
  ) {
    sentEmails(
      filter: {
        limit: $limit
        skip: $skip
        startDate: $startDate
        endDate: $endDate
        services: $services
      }
    ) {
      sent {
        mailTo {
          email
          name
        }
        subject
        action
        service
        createdAt
        param
      }
      total
    }
  }
`;

const GET_SERVICES = gql`
  {
    getServices
  }
`;

export { EXCLUDED_EMAILS, SENT_EMAILS, GET_SERVICES };
