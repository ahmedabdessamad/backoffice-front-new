/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import * as PropTypes from "prop-types";
import { Typography, Form, Button, Card } from "antd";
import { withRouter } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { useQuery } from "@apollo/react-hooks";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import classes from "./FormToBeSent.module.scss";
import InputComponent from "../../components/Form/InputComponent";
import CheckboxComponent from "../../components/Form/CheckboxComponent";
import ModalInput from "../../components/Form/ModalInput";
import EditModalInput from "../../components/Form/EditModalInput";
import checkboxChoicesActions from "../../redux/checkboxChoices/actions";
import { FETCH_ONE_FORM_QUERY } from "./utile/queries";

const { Title } = Typography;
const { setInputList } = checkboxChoicesActions;

function FormToBeSentComponent(props) {
  const dispatch = useDispatch();
  const { inputList } = useSelector((state) => state.CheckboxChoicesReducer);
  const [id, setId] = useState("");
  const { match } = props;
  // const [inputList_old, setInputList] = useState([]);
  const [visibleModalInput, setVisibleModalInput] = useState(false);
  const [visibleEditModalInput, setVisibleEditModalInput] = useState(false);
  const [formItem, setFormItem] = useState(null);
  const [formItemIndex, setFormItemIndex] = useState(null);
  const [menuKey, setMenuKey] = useState("");
  const [formSubject, setFormSubject] = useState("");

  const formIDfromUrl = match.params.id;

  useEffect(() => {
    for (let i = 0; i < inputList.length; i++) {
      if (inputList[i].key === "FormTitle") {
        setFormSubject(inputList[i].name);
      }
    }
  }, [inputList]);

  const { data } = useQuery(FETCH_ONE_FORM_QUERY, {
    variables: {
      formID: formIDfromUrl,
    },
    fetchPolicy: "network-only",
  });

  if (data) {
    const { formID, structure } = data.form;
    if (!id) {
      setId(formID);
      dispatch(setInputList(structure));
    }
  }

  const onFinish = () => {};

  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <ModalInput
          visibleModalInput={visibleModalInput}
          setVisibleModalInput={setVisibleModalInput}
          inputList={inputList}
          menuKey={menuKey}
        />
        <EditModalInput
          visibleEditModalInput={visibleEditModalInput}
          setVisibleEditModalInput={setVisibleEditModalInput}
          formItem={formItem}
          setFormItem={setFormItem}
          formItemIndex={formItemIndex}
          setFormItemIndex={formItemIndex}
          menuKey={menuKey}
        />
        <div className={classes.content}>
          <div className="App">
            <Card className={classes.card}>
              {inputList.map((x, i) => {
                if (x.key === "FormTitle") {
                  return (
                    <Title level={3} className={classes.title}>
                      {x.name}
                    </Title>
                  );
                }
              })}
              <Form layout="vertical" onFinish={onFinish}>
                {inputList.map((x, i) => {
                  if (x.key === "Checkbox") {
                    console.log("CheckBox input list =", x);
                    return (
                      <div>
                        <Form.Item
                          name={x.name}
                          label={x.name}
                          rules={[
                            {
                              required: x.required,
                              message: "This field is required!",
                            },
                          ]}
                        >
                          <div className="btn-box">
                            <div style={{ display: "flex" }}>
                              <CheckboxComponent
                                name={x.name}
                                choices={x.choices}
                              />

                              {inputList.length - 1 === i}
                            </div>
                          </div>
                        </Form.Item>
                      </div>
                    );
                  }
                  if (
                    x.key === "simpleInput" ||
                    x.key === "inputText" ||
                    x.key === "inputNumber"
                  ) {
                    return (
                      <div>
                        <Form.Item
                          name={x.name}
                          label={x.name}
                          rules={[
                            {
                              required: x.required,
                              message: "This field is required!",
                            },
                          ]}
                        >
                          <div className="btn-box">
                            <div style={{ display: "flex" }}>
                              <InputComponent x={x} i={i} />
                              {inputList.length - 1 === i}
                            </div>
                          </div>
                        </Form.Item>
                      </div>
                    );
                  }
                })}

                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </Form>
            </Card>

            {/* <div style={{ marginTop: 20 }}>{JSON.stringify(inputList)}</div> */}
          </div>
        </div>
      </ContentContainer>
    </ContentWrapper>
  );
}

FormToBeSentComponent.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(FormToBeSentComponent);
