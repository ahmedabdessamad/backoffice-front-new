import { gql } from "graphql.macro";

const FETCH_MARKETINFO_QUERY = gql`
  {
    marketInfos {
      marketInfoID
      companyId
      companyName
      jobTitle
      contact
      contactName
      email
      phone
      industry {
        name
      }
      categories {
        name
      }
      description
      createdAt
      creator {
        email
        userID
      }
    }
  }
`;

const FETCH_ONE_MARKETINFO_QUERY = gql`
  query($marketInfoID: ID!) {
    marketInfo(marketInfoID: $marketInfoID) {
      marketInfoID
      companyId
      companyName
      jobTitle
      contact
      contactName
      email
      phone
      industry {
        name
      }
      categories {
        name
      }
      description
      createdAt
      creator {
        email
        userID
      }
    }
  }
`;

const GET_CANDIDATE_EMAIL_PHONE_QUERY = gql`
  query($candidateID: ID!) {
    candidateEmailPHone(candidateID: $candidateID) {
      email
      mobile
    }
  }
`;
const GET_INDUSTRY_QUERY = gql`
  {
    businessSectors {
      name
    }
  }
`;
const GET_COMPANY_QUERY = gql`
  {
    clientCorporations {
      id
      name
    }
  }
`;
const GET_CATEGORIE_QUERY = gql`
  {
    categories {
      id
      name
    }
  }
`;
export {
  FETCH_MARKETINFO_QUERY,
  FETCH_ONE_MARKETINFO_QUERY,
  GET_INDUSTRY_QUERY,
  GET_COMPANY_QUERY,
  GET_CATEGORIE_QUERY,
  GET_CANDIDATE_EMAIL_PHONE_QUERY,
};
