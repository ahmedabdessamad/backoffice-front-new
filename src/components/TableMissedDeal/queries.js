import { gql } from "graphql.macro";

const FETCH_MISSEDDEAL_BY_COLUMN_QUERY = gql`
  query find($searched: [arrayOfSearch]) {
    find(searched: $searched) {
      missedDealID
      companyId
      companyName
      ITConsultingFirm
      intermedianryAgency
      missionStart
      missionEnd
      createdAt
      skills {
        id
        name
      }
      jobTitle
      dailyRate
      city
      missionProjectDescription
      creator {
        email
      }
      candidate
    }
  }
`;
export default FETCH_MISSEDDEAL_BY_COLUMN_QUERY;
