import React, { useState } from "react";
import { Modal, Form, Input, Button, message, Select } from "antd";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks/lib/index";
import classes from "./header.module.scss";
import CHANGE_PASSWORD from "./mutations";

/* const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
}; */

export default function Settings() {
  const user = useSelector((state) => state.AuthReducer.user);
  const [settingsModalVisible, setSettingsModalVisible] = useState(false);
  const [form] = Form.useForm();
  const [updatePassword] = useMutation(CHANGE_PASSWORD, {
    onCompleted: () => message.success("password successufully Updated!"),
    onError: (error) => {
      if (error.graphQLErrors[0].message === "unmatched passwords") {
        message.error("passwords don't match ");
      } else {
        message.error("Oops Something went wrong");
      }
    },
  });
  const getDefaultPermissions = (arr) => {
    if (arr) {
      return arr.map((elm) => elm.name);
    }
    return [];
  };
  return (
    <div>
      <Link
        className={classes.userMenuLink}
        onClick={() => setSettingsModalVisible(true)}
      >
        Settings
      </Link>
      <Modal
        title="Settings"
        visible={settingsModalVisible}
        footer={null}
        onCancel={() => {
          setSettingsModalVisible(false);
          form.resetFields();
        }}
      >
        <Form
          form={form}
          // {...layout}
          fields={[
            {
              name: ["email"],
              value: user.email,
            },
            {
              name: ["permissions"],
            },
            {
              name: ["password"],
              value: user.password,
            },
            {
              name: ["confirmPassword"],
              value: user.password,
            },
          ]}
        >
          <div className={classes.field}>
            <Form.Item label="E-Mail" name="email">
              <Input disabled={user.userID} style={{ width: "100%" }} />
            </Form.Item>
          </div>
          <div className={classes.field}>
            <Form.Item label="Permissions" name="permissions">
              <Select
                mode="multiple"
                disabled
                style={{ width: "100%" }}
                placeholder="Please select"
                defaultValue={getDefaultPermissions(user.permissions)}
                // onChange={handleChange}
              />
            </Form.Item>
          </div>

          <div className={classes.field}>
            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please enter a complex password!" },
              ]}
            >
              <Input.Password style={{ width: "100%" }} />
            </Form.Item>
          </div>
          <div className={classes.field}>
            <Form.Item
              label="Confirm Password"
              name="confirmPassword"
              rules={[{ required: true }]}
            >
              <Input.Password style={{ width: "100%" }} />
            </Form.Item>
          </div>

          <Button
            type="primary"
            onClick={() => {
              form.validateFields().then((values) => {
                updatePassword({
                  variables: {
                    ...values,
                  },
                });
              });
            }}
          >
            Send Password Reset
          </Button>
        </Form>
      </Modal>
    </div>
  );
}
