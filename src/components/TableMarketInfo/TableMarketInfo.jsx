import React, { useState } from "react";
import { Table, Tag, Input, Space, Button, DatePicker } from "antd";
import * as PropTypes from "prop-types";
import {
  EditOutlined,
  SearchOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import { Link, useRouteMatch } from "react-router-dom";
import Highlighter from "react-highlight-words";
import { useQuery } from "@apollo/react-hooks";
import moment from "moment";
import { useSelector } from "react-redux";
import { isUserAuthorized } from "../../helpers/authorization/permissionsCheckHelpers";
import classes from "./TableMarketInfo.module.scss";
import ModalMarketDetails from "../Modal/ModalMarketDetails";
import useWindowSize from "../../hooks/useWindowSize";
import FETCH_MARKETINFO_BY_COLUMN_QUERY from "./queries";

function TableMarketInfo(props) {
  const { marketInfos, user } = props;
  const bhEntityId = useSelector(
    (state) => state.BhEntityReducer.bhEntity.entityId
  );
  const bhEntityType = useSelector(
    (state) => state.BhEntityReducer.bhEntity.entityType
  );

  console.log("bhEntity", bhEntityId);

  const [{ width, height }] = useWindowSize();
  let size;
  if (width < 450) size = 600;
  else if (width >= 450 && width < 550) size = 450;
  else if (width >= 550 && width < 700) size = 320;
  else if (width >= 700 && width < 900) size = 300;
  else if (width >= 900 && width < 1000) size = 250;
  else if (width >= 1000 && width < 1250) size = 200;
  else size = 110;

  let sizeHeight;
  if (height < 800) sizeHeight = 350;
  else sizeHeight = 550;

  let data1 = [];
  const [searched, setSearched] = useState([]);
  const [column, setColumn] = useState("");
  const match = useRouteMatch();

  const stripTrailingSlash = (str) => {
    if (str.substr(-1) === "/") {
      return str.substr(0, str.length - 1);
    }
    return str;
  };

  const url = stripTrailingSlash(match.url);
  const { data, loading } = useQuery(FETCH_MARKETINFO_BY_COLUMN_QUERY, {
    variables: {
      searched,
    },
  });
  if (searched.length > 0) {
    if (data) {
      data1 = data.findMarket.map((marketInfo) => {
        const splitDate = marketInfo.createdAt.split("T");
        const createDate = splitDate[0];
        return {
          key: marketInfo.marketInfoID,
          marketInfoID: marketInfo.marketInfoID,
          company: marketInfo.companyName,
          jobTitle: marketInfo.jobTitle,
          contact: marketInfo.contact,
          contactName: marketInfo.contactName,
          email: marketInfo.email,
          phone: marketInfo.phone,
          industry: marketInfo.industry,
          categories: marketInfo.categories,
          description: marketInfo.description,
          creator: marketInfo.creator.email,
          createdAt: createDate,
        };
      });
    }
  } else if (bhEntityId !== null) {
    if (bhEntityType === "Candidate") {
      const tabExist = marketInfos.filter(
        (market) => market.contact === bhEntityId
      );

      if (tabExist.length > 0) {
        data1 = tabExist.map((marketInfo) => {
          const splitDate = marketInfo.createdAt.split("T");
          const createDate = splitDate[0];
          return {
            key: marketInfo.marketInfoID,
            marketInfoID: marketInfo.marketInfoID,
            company: marketInfo.companyName,
            jobTitle: marketInfo.jobTitle,
            contact: marketInfo.contact,
            contactName: marketInfo.contactName,
            email: marketInfo.email,
            phone: marketInfo.phone,
            industry: marketInfo.industry,
            categories: marketInfo.categories,
            description: marketInfo.description,
            creator: marketInfo.creator.email,
            createdAt: createDate,
          };
        });
      }
    } else if (bhEntityType === "Client") {
      const tabExist = marketInfos.filter((market) => {
        console.log("marketInfo.companyId", market.companyId);
        return market.companyId === bhEntityId;
      });

      if (tabExist.length > 0) {
        data1 = tabExist.map((marketInfo) => {
          const splitDate = marketInfo.createdAt.split("T");
          const createDate = splitDate[0];
          return {
            key: marketInfo.marketInfoID,
            marketInfoID: marketInfo.marketInfoID,
            company: marketInfo.companyName,
            jobTitle: marketInfo.jobTitle,
            contact: marketInfo.contact,
            contactName: marketInfo.contactName,
            email: marketInfo.email,
            phone: marketInfo.phone,
            industry: marketInfo.industry,
            categories: marketInfo.categories,
            description: marketInfo.description,
            creator: marketInfo.creator.email,
            createdAt: createDate,
          };
        });
      }
    }
  } else {
    data1 = marketInfos.map((marketInfo) => {
      const splitDate = marketInfo.createdAt.split("T");
      const createDate = splitDate[0];
      return {
        key: marketInfo.marketInfoID,
        marketInfoID: marketInfo.marketInfoID,
        company: marketInfo.companyName,
        jobTitle: marketInfo.jobTitle,
        contact: marketInfo.contact,
        contactName: marketInfo.contactName,
        email: marketInfo.email,
        phone: marketInfo.phone,
        industry: marketInfo.industry,
        categories: marketInfo.categories,
        description: marketInfo.description,
        creator: marketInfo.creator.email,
        createdAt: createDate,
      };
    });
  }
  const Compare = (skill1, searched1) => {
    const splitSerched = searched1.toLowerCase().split(" ");
    const skill2 = skill1.toLowerCase();
    let exist = -1;
    splitSerched.map((serched) => {
      if (serched === skill2) {
        exist = 1;
      }
      return exist;
    });
    return exist;
  };

  const getDetail = (marketInfo) => {
    ModalMarketDetails(marketInfo);
  };
  const exist = (searched1, dataIndex) => {
    let i = 0;
    for (i = 0; i < searched1.length; i += 1) {
      if (dataIndex in searched1[i]) {
        return i;
      }
    }
    return -1;
  };
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    setColumn([...column, dataIndex]);
    if (dataIndex === "createdAt") {
      if (selectedKeys[0]) {
        const time = moment(selectedKeys[0]._d);
        const ob1 = { [dataIndex]: time.format("YYYY-MM-DDTHH:MM:ss") };
        const isExist1 = exist(searched, dataIndex);
        if (searched.length === 0 || isExist1 === -1)
          setSearched([...searched, ob1]);
        else {
          const newArr3 = [...searched];
          newArr3[isExist1] = ob1;
          setSearched(newArr3);
        }
      }
    } else if (dataIndex === "industry" || dataIndex === "categories") {
      if (selectedKeys[0]) {
        const splitIndustry = selectedKeys[0].split(";");
        const searchedS = [];
        for (let i = 0; i < splitIndustry.length; i += 1) {
          searchedS.push({ name: splitIndustry[i] });
        }
        const ob3 = { [dataIndex]: searchedS };
        const isExist = exist(searched, dataIndex);
        if (searched.length === 0 || isExist === -1)
          setSearched([...searched, ob3]);
        else {
          const newArr = [...searched];
          newArr[isExist] = ob3;
          setSearched(newArr);
        }
      }
    } else if (selectedKeys[0]) {
      const ob = { [dataIndex]: selectedKeys[0] };
      const isExist2 = exist(searched, dataIndex);
      if (searched.length === 0 || isExist2 === -1)
        setSearched([...searched, ob]);
      else {
        const newArr1 = [...searched];
        newArr1[isExist2] = ob;
        setSearched(newArr1);
      }
    }
  };

  const handleReset = () => {
    setColumn("");
    setSearched([]);
  };
  const handleOneReset = (dataIndex) => {
    setColumn("");
    const items = searched.filter((search) => !(dataIndex in search));
    setSearched(items);
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
      <div key={dataIndex} style={{ padding: 8 }}>
        {dataIndex === "createdAt" ? (
          <DatePicker
            initialValues={moment("2020/01/01", "YYYY/MM/DD")}
            format="YYYY/MM/DD"
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={(date) => setSelectedKeys(date ? [date] : [])}
            onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
        ) : (
          <Input
            placeholder={
              dataIndex === "categories" || dataIndex === "industry"
                ? "item;item;item..."
                : `Search ${dataIndex}`
            }
            value={selectedKeys[0]}
            onChange={(e) =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
        )}
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 30 }}
          />
          <Button
            onClick={() => handleOneReset(dataIndex)}
            size="small"
            style={{ width: 70 }}
          >
            Reset
          </Button>
          <Button
            onClick={() => handleReset()}
            size="small"
            style={{ width: 70 }}
          >
            Reset All
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#e6f7ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    render: (text) => {
      const tabS = [];
      for (let i = 0; i < searched.length; i += 1) {
        if (!("industry" in searched[i]) && !("categories" in searched[i])) {
          const arrayTemp = Object.values(searched[i]);
          tabS.push(arrayTemp[0]);
        }
      }

      if (tabS)
        return (
          <Highlighter
            highlightStyle={{ backgroundColor: "#40a9ff", padding: 0 }}
            searchWords={tabS}
            autoEscape
            textToHighlight={text.toString()}
          />
        );
      return text;
    },
  });
  const columns = [
    {
      title: "Date",
      dataIndex: "createdAt",
      key: "createdAt",
      ...getColumnSearchProps("createdAt"),
      sorter: (a, b) => moment(a.createdAt).unix() - moment(b.createdAt).unix(),
    },
    {
      title: "Company",
      dataIndex: "company",
      key: "company",
      ...getColumnSearchProps("company"),
    },
    {
      title: "Candidate",
      dataIndex: "contactName",
      key: "contactName",
      ...getColumnSearchProps("contactName"),
    },
    {
      title: "Job Title",
      dataIndex: "jobTitle",
      key: "jobTitle",
      ...getColumnSearchProps("jobTitle"),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      ...getColumnSearchProps("email"),
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
      ...getColumnSearchProps("phone"),
    },
    {
      title: "Industry",
      dataIndex: "industry",
      key: "industry",
      ...getColumnSearchProps("industry"),
      render: (industry) => {
        return (
          <span>
            {industry.map((sector) => {
              return (
                <div key={sector.name}>
                  {column === "industry" ? (
                    <div>
                      {Compare(sector, searched) === 1 ? (
                        <Tag color="red" key={sector}>
                          {sector}
                        </Tag>
                      ) : (
                        <Tag color="blue" key={sector}>
                          {sector}
                        </Tag>
                      )}
                    </div>
                  ) : (
                    <Tag color="blue" key={sector.name}>
                      {sector.name}
                    </Tag>
                  )}
                </div>
              );
            })}
          </span>
        );
      },
    },
    {
      title: "Community Technical Stack",
      dataIndex: "categories",
      key: "categories",
      ...getColumnSearchProps("categories"),
      render: (categories) => {
        return (
          <span>
            {categories.map((skill) => {
              return (
                <div key={skill.name}>
                  {column === "categories" ? (
                    <div>
                      {Compare(skill, searched) === 1 ? (
                        <Tag color="red" key={skill}>
                          {skill}
                        </Tag>
                      ) : (
                        <Tag color="blue" key={skill}>
                          {skill}
                        </Tag>
                      )}
                    </div>
                  ) : (
                    <Tag color="blue" key={skill.name}>
                      {skill.name}
                    </Tag>
                  )}
                </div>
              );
            })}
          </span>
        );
      },
    },
    {
      title: "Creator",
      dataIndex: "creator",
      key: "creator",
      ...getColumnSearchProps("creator"),
    },
    {
      dataIndex: "key",
      key: "key",
      render: (key, missedDeal) => (
        <div style={{ display: "flex" }}>
          <SearchOutlined
            className={classes.icon2}
            onClick={() => getDetail(missedDeal)}
          />
          {user ? (
            <div>
              {isUserAuthorized(user, { MARKETINFO: 2 }) ? (
                <span>
                  {user.email === missedDeal.creator ? (
                    <Link to={`${url}/edit/${key}`}>
                      <EditOutlined className={classes.icon} />
                    </Link>
                  ) : null}
                </span>
              ) : null}
              {isUserAuthorized(user, { MARKETINFO: 4 }) ? (
                <span>
                  <Link to={`${url}/edit/${key}`}>
                    <EditOutlined className={classes.icon} />
                  </Link>
                </span>
              ) : null}
            </div>
          ) : null}
        </div>
      ),
    },
  ];
  return (
    <div style={{ position: "relative", height: "80%" }}>
      {loading ? <LoadingOutlined className={classes.icon3} /> : null}
      {data1 ? (
        <Table
          style={{ position: "relative" }}
          columns={columns}
          dataSource={data1}
          scroll={{ x: `calc(1px + ${size}%)`, y: sizeHeight }}
        />
      ) : null}
    </div>
  );
}

TableMarketInfo.propTypes = {
  marketInfos: PropTypes.arrayOf(PropTypes.object),
  setSelectedKeys: PropTypes.func,
  selectedKeys: PropTypes.string,
  confirm: PropTypes.func,
  clearFilters: PropTypes.func,
  user: PropTypes.shape({
    role: PropTypes.string,
    email: PropTypes.string,
    groups: PropTypes.array,
  }).isRequired,
};
TableMarketInfo.defaultProps = {
  marketInfos: [],
  setSelectedKeys: () => {},
  selectedKeys: "",
  confirm: () => {},
  clearFilters: () => {},
};
export default TableMarketInfo;
