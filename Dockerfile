FROM node:latest

WORKDIR /usr/src/app

COPY package*.json /usr/src/app

RUN yarn

COPY . /usr/src/app

EXPOSE 3000

CMD ["yarn","run","start"]
