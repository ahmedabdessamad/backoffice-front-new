import { gql } from "graphql.macro";

const FILL_TIME_SHEET_MUTATION = gql`
  mutation FillTimesheet(
    $id: ID!
    $selectedDays: [SelectedDaysInput]
    $specialHours: [SpecialHoursInput]
  ) {
    fillTimesheet(
      id: $id
      body: { selectedDays: $selectedDays, specialHours: $specialHours }
    ) {
      id
    }
  }
`;

const UPDATE_TIMESHEET_STATUS_MUTATION = gql`
  mutation UpdateTimesheet($id: ID!, $status: String) {
    updateTimesheet(id: $id, newTimesheet: { status: $status })
  }
`;

export { FILL_TIME_SHEET_MUTATION, UPDATE_TIMESHEET_STATUS_MUTATION };
