const actions = {
  VERIF_TOKEN: "VERIF_TOKEN",
  xeroLogin: (payload) => ({
    type: actions.VERIF_TOKEN,
    payload,
  }),
};
export default actions;
