import React from "react";
import { message, Modal, Form, Input, Button } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/react-hooks";
import { GET_GROUPS } from "./queries";
import { addGroupMutation } from "./mutations";

export default function AddGroup() {
  const [form] = Form.useForm();
  const [visible, setVisible] = React.useState(false);
  const [group, setGroup] = React.useState("");
  const [addGroup] = useMutation(addGroupMutation, {
    onCompleted: () => message.success("Group added"),
    refetchQueries: [{ query: GET_GROUPS }],
  });
  function handleOk() {
    setVisible(false);
    addGroup({
      variables: {
        group,
      },
    });
  }
  return (
    <div>
      <Button
        type="primary"
        icon={<PlusOutlined />}
        onClick={() => setVisible(true)}
      >
        Add Group
      </Button>
      <Modal
        title="ADD GROUP"
        visible={visible}
        onOk={handleOk}
        onCancel={() => setVisible(false)}
      >
        <Form form={form}>
          <Form.Item label="Group Name">
            <Input
              placeholder="enter a group"
              onChange={(e) => setGroup(e.target.value)}
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
