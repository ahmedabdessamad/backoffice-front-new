import { useState } from "react";

const useForm = (callback, initialState = {}) => {
  const [values, setValues] = useState(initialState);

  const handleChange = (event) => {
    event.persist();
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    if (event) event.preventDefault();
    // console.log(values);
    callback();
  };
  return [values, handleChange, handleSubmit];
};
export default useForm;
