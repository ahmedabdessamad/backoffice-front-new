import React from "react";
import { Spin } from "antd";
import classes from "./spinner.module.scss";

export default function Spinner() {
  return (
    <div className={classes.spin}>
      <Spin tip="Loading..." size="large" />
    </div>
  );
}
