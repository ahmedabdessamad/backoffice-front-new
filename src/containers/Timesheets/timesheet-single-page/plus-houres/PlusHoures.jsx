import React from "react";
import * as PropTypes from "prop-types";
import classes from "./PlusHoures.module.scss";
import Line from "./line/Line";

function PlusHoures(props) {
  const { month, year, specialHours } = props;
  return (
    <div className={classes.container}>
      <div className={classes.header}>Heures & Astreintes</div>
      <div className={classes.content}>
        {specialHours.length
          ? specialHours.map((sh) => {
              return (
                <Line
                  key={`special_hour_line_${sh.id}`}
                  specialHour={sh}
                  month={month}
                  year={year}
                />
              );
            })
          : null}
      </div>
    </div>
  );
}

PlusHoures.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
  specialHours: PropTypes.shape.isRequired,
};

export default PlusHoures;
