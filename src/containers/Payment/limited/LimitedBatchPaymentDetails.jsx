import React from "react";
import Details from "../BatchPayment-Components/Details";
// a
export default function LimitedBatchPaymentDetails() {
  return (
    <Details
      title=" A new batch payment has been sent from xero"
      path="/home/limited_logs"
    />
  );
}
