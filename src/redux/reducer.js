import { combineReducers } from "redux";
import AppReducer from "./app/reducer";
import AuthReducer from "./auth/reducer";
import BhEntityReducer from "./bhEntity/reducer";
import UserReducer from "./users/reducer";
import PermissionReducer from "./permissions/reducer";
import SendoutReducer from "./sendout/reducer";
import MissedDealReducer from "./missedDeal/reducer";
import xeroLoginReducer from "./xeroLogin/reducer";
import CheckboxChoicesReducer from "./checkboxChoices/reducer";

export default combineReducers({
  AppReducer,
  AuthReducer,
  BhEntityReducer,
  UserReducer,
  PermissionReducer,
  SendoutReducer,
  MissedDealReducer,
  xeroLoginReducer,
  CheckboxChoicesReducer,
});
