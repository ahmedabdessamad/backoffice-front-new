import { gql } from "graphql.macro";

const UPDATE_MARKETINFO_MUTATION = gql`
  mutation editMarketInfo($marketInfoID: ID!, $input: MarketInfoInput!) {
    editMarketInfo(marketInfoID: $marketInfoID, input: $input) {
      marketInfoID
      companyId
      companyName
      jobTitle
      contact
      contactName
      email
      phone
      industry {
        name
      }
      categories {
        name
      }
      description
      creator {
        email
        userID
      }
    }
  }
`;

const CREATE_MARKETINFO_MUTATION = gql`
  mutation addMarketInfo($input: MarketInfoInput!) {
    addMarketInfo(input: $input) {
      marketInfoID
      companyId
      companyName
      jobTitle
      contact
      contactName
      email
      phone
      industry {
        name
      }
      categories {
        name
      }
      description
      creator {
        email
        userID
      }
    }
  }
`;
export { UPDATE_MARKETINFO_MUTATION, CREATE_MARKETINFO_MUTATION };
