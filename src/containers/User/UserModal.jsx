import React, { useState } from "react";
import {
  Modal,
  Form,
  Input,
  Select,
  message,
  Button,
  Space,
  Switch,
} from "antd";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { useQuery } from "@apollo/react-hooks";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/react-hooks/lib/index";
import classes from "./user.module.scss";
import { GET_PERMISSIONS } from "../Permission/queries";
import {
  addUserMutation as ADD_USER_MUTATION,
  editUserMutation as EDIT_USER_MUTATION,
} from "./mutations";
import { GET_USERS } from "./queries";
import usersAction from "../../redux/users/actions";

const { toggleUserModal } = usersAction;

const { Option } = Select;

export default function () {
  const { modalVisible, user } = useSelector((state) => state.UserReducer);
  const [password, setPassword] = useState(user.password);
  const dispatch = useDispatch();

  const [form] = Form.useForm();

  const {
    loading: loadingPermissions,
    error: errorPermissions,
    data: permissionsData,
  } = useQuery(GET_PERMISSIONS,{fetchPolicy: "network-only"});


  const [addUser] = useMutation(ADD_USER_MUTATION, {
    onCompleted: () => message.success("User Successfully Created!"),
    onError: (error) => {
      if (error.message === "GraphQL error: user already exists") {
        message.error("User Already Exists");
      } else {
        message.error("Oops Something went wrong");
      }
    },
    update: (cache, { data }) => {
      const { users } = cache.readQuery({ query: GET_USERS });
      const newUsers = [data.addUser, ...users];
      cache.writeQuery({ query: GET_USERS, data: { users: newUsers } });
    },
  });

  const [updateUser] = useMutation(EDIT_USER_MUTATION, {
    onCompleted: () => message.success("User Updated!"),
    onError: () => {
      message.error("Oops Something went wrong");
    },
    refetchQueries: [{ query: GET_USERS }],
  });

  const buildOptions = (loading, error, data, filedName) => {
    if (loading || error) return [];
    return data[filedName].map((elm) => (
      <Option key={elm.name}>{elm.name}</Option>
    ));
  };

  /* const getDefaultOptions = (arr) => {
    if (arr) {
      return arr.map((elm) => ({
        role: elm.role.name,
        groupe: elm.groupe.name,
      }));
    }
    return [];
  }; */
  const getDefaultOptions = (arr) => {
    console.log("err", arr);
    if (arr) {
      return arr.map((elm) => {
        console.log("elm.id====>", elm.name);
        return elm.name;
      });
    }
    return [];
  };
  const changePasswordHandler = (checked) => {
    const newPassword = Math.random().toString(36).slice(2);

    if (checked) {
      setPassword(newPassword);
    }
  };

  return (
    <Modal
      visible={modalVisible}
      title={user.userID ? "Update User" : "Create New User"}
      okText={user.userID ? "Update" : "Create"}
      onClose={() => {
        dispatch(toggleUserModal());
        form.resetFields();
      }}
      onCancel={() => {
        dispatch(toggleUserModal());
        form.resetFields();
      }}
      onOk={() => {
        form.validateFields().then((values) => {
          const per = values.permissions.map((p) => {
            const exist = permissionsData.permissions.filter(
              (t) => t.name === p
            );
            if (exist) return exist[0].id;
            return [];
          });
          console.log("user==================================>", user);
          user.userID
            ? updateUser({
                variables: {
                  userID: user.userID,
                  input: {
                    ...values,
                    password,
                    permissions: per,
                  },
                },
              })
            : addUser({
                variables: {
                  input: {
                    ...values,
                    permissions: per,
                  },
                },
              });
          dispatch(toggleUserModal());
          form.resetFields();
        });
      }}
    >

      <Form
        form={form}
        fields={[
          {
            name: ["username"],
            value: user.username,
          },
          {
            name: ["email"],
            value: user.email,
          },
          {
            name: ["password"],
            value: user.password,
          },
          {
            name: ["permissions"],
            value: getDefaultOptions(user.permissions),
          },
        ]}
      >
        <div className={classes.field}>
          <Form.Item label="Username" name="username">
            <Input />
          </Form.Item>
        </div>
        <div className={classes.field}>
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                type: "email",
                message: "please enter a valid email",
              },
            ]}
          >
            <Input disabled={user.userID} />
          </Form.Item>
        </div>
        <div className={classes.field}>
          {user.userID ? (
            <Form.Item label="Generate New Password" name="password">
              <Switch onChange={changePasswordHandler} />
            </Form.Item>
          ) : null}
        </div>
        <Form.List name="permissions">
          {(fields, { add, remove }) => {
            return (
              <div>
                {fields.map((field) => (
                  <Space
                    key={field.key}
                    style={{ display: "flex", marginBottom: 8 }}
                    align="start"
                  >
                    <div className={classes.field}>
                      <Form.Item
                        {...field}
                        label="Permissions"
                        name={field.name}
                        fieldKey={field.fieldKey}
                        rules={[
                          {
                            required: true,
                            message: "please select a permission",
                          },
                        ]}
                      >
                        <Select style={{ width: 200 }}>
                          {buildOptions(
                            loadingPermissions,
                            errorPermissions,
                            permissionsData,
                            "permissions"
                          )}
                        </Select>
                      </Form.Item>
                    </div>
                    {/* <div className={classes.field}>
                      <Form.Item
                        label="Roles"
                        name={[field.name, "role"]}
                        fieldkey={[field.fieldKey, "role"]}
                        rules={[
                          { required: true, message: "please select a role" },
                        ]}
                      >
                        <Select style={{ width: 200 }}>
                          {buildOptions(
                            loadingRoles,
                            errorRoles,
                            rolesData,
                            "roles"
                          )}
                        </Select>
                      </Form.Item>
                    </div> */}
                    <MinusCircleOutlined
                      onClick={() => {
                        remove(field.name);
                      }}
                    />
                  </Space>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => {
                      add();
                    }}
                    block
                  >
                    <PlusOutlined /> Add Permission
                  </Button>
                </Form.Item>
              </div>
            );
          }}
        </Form.List>
      </Form>
    </Modal>
  );
}
