import React, { useEffect, useState } from "react";
import * as PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import {
  Table,
  Tooltip,
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  message,
} from "antd";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { NotificationTwoTone } from "@ant-design/icons";
import moment from "moment";
import ContentContainer from "../../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../../components/ContentWrapper/ContentWrapper";
import { EXCLUDED_EMAILS, GET_SERVICES } from "../query";
import localStateToQuery from "../localStateToQuery";
import { STOP_MAILING, RESUME_MAILING } from "../mutations";

function Excluded() {
  const initialFilters = {
    limit: 1000,
    skip: 0,
    startDate: null,
    endDate: null,
    services:[],
  };

  const [filters, setFilters] = useState({});
  const [selectedItems, setSelectedItems] = useState([]);
  const [email, setEmail] = useState("");
  const [emailToResume, setEmailToResume] = useState("");
  const [errors, setErrors] = useState({});
  const [queryFilters, setQueryFilters] = useState(
    localStateToQuery(initialFilters)
  );

  useEffect(() => {
    if (filters.searchTrigger) setQueryFilters(localStateToQuery(filters));
  }, [filters]);

  useEffect(() => {
    const newState = { ...filters };
    newState.searchTrigger = false;
    setFilters(newState);
  }, [queryFilters]);

  const paginate = (page, pageSize) => {
    const newState = { ...filters };
    newState.skip = (page - 1) * pageSize;
    newState.limit = pageSize;
    newState.searchTrigger = true;
    setFilters(newState);
  };

  const { loading, data, error } = useQuery(EXCLUDED_EMAILS, {
    variables: queryFilters,
  });
  let OPTIONS = [];
  const { loading: loading1, data: data1, error: error1 } = useQuery(
    GET_SERVICES
  );
  const successStop = () => {
    message.success("Mailing service is stopped");
  };

  const successResume = () => {
    message.success("Mailing service is resumed");
  };

  const [stopMailing] = useMutation(STOP_MAILING, {
    variables: {
      services: selectedItems,
      email,
    },
    onCompleted() {
      successStop();
    },
    onError(err) {
      setErrors(err.graphQLErrors[0].message);
    },
  });
  const [resumeMailing] = useMutation(RESUME_MAILING, {
    variables: {
      email: emailToResume,
    },
    refetchQueries: [{ query: EXCLUDED_EMAILS, variables: queryFilters }],
    awaitRefetchQueries: true,
    onCompleted() {
      successResume();
    },
    onError(err) {
      setErrors(err.graphQLErrors[0].message);
    },
  });

  if (data1 && data1.getServices) {
    OPTIONS = data1.getServices;
  }

  let emails = null;
  let total = null;
  if (data && data.excludedEmails && data.excludedEmails.excludes) {
    emails = data.excludedEmails.excludes.map((m) => ({
      email: m.email,
      services: m.services && m.services.map((s) => <li key={s}>{`${s}`}</li>),
      platform: m.platform,
      excludedAt: moment(m.excludedAt).format("DD/MM/YYYY"),
      actions: m.email,
    }));
    total = data.excludedEmails.total;
  }

  const columns = [
    {
      title: "Email",
      width: 100,
      dataIndex: "email",
      key: "email",
      fixed: "left",
    },
    {
      title: "Services",
      width: 100,
      dataIndex: "services",
      key: "services",
    },
    {
      title: "Excluded at",
      dataIndex: "excludedAt",
      key: "excludedAt",
      width: 150,
    },
    {
      title: "Platform",
      dataIndex: "platform",
      key: "platform",
      width: 150,
    },
    {
      title: "Actions",
      dataIndex: "actions",
      key: "actions",
      width: 100,
      render: (m) => (
        <div>
          <Tooltip placement="topLeft" title="edit">
            <NotificationTwoTone
              style={{ color: "#1890ff", padding: "0 3px" }}
              onClick={() => {
                setEmailToResume(m);
                resumeMailing(m);
              }}
              type="edit"
            />
          </Tooltip>
        </div>
      ),
    },
  ];

  const validateMessages = {
    required: "is required!",
    types: {
      email: "is not validate email!",
    },
  };

  const handleSelectedItemsChange = (items) => {
    setSelectedItems(items);
  };

  const handleEmailChange = (value) => {
    setEmail(value);
  };

  const onFinish = () => {
    stopMailing();
  };

  const filteredOptions = OPTIONS.filter((o) => !selectedItems.includes(o));
  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <Form
          name="nest-messages"
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Row>
            <Col span={8}>
              <Form.Item
                name={["user", "email"]}
                label="Email"
                rules={[{ type: "email" }, { required: true }]}
              >
                <Input
                  style={{ width: "90%" }}
                  placeholder="input email"
                  onChange={(e) => handleEmailChange(e.target.value)}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item name={["user", "services"]} label="Services">
                <Select
                  mode="multiple"
                  placeholder="select services"
                  value={selectedItems}
                  onChange={handleSelectedItemsChange}
                  style={{ width: "90%" }}
                >
                  {filteredOptions.map((item) => (
                    <Select.Option key={item} value={item}>
                      {item}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  style={{ width: "90%" }}
                >
                  Stop mailing
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <Table
          columns={columns}
          dataSource={!error && emails}
          size="small"
          sticky
          pagination={{
            onChange: paginate,
            total,
          }}
          loading={loading}
          scroll={{ x: 1500, y: 450 }}
        />
      </ContentContainer>
    </ContentWrapper>
  );
}

Excluded.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
};
export default withRouter(Excluded);
