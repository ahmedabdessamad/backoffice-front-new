import React from "react";
import { Form, Input, Button, Typography, Card, Select, message } from "antd";
import * as PropTypes from "prop-types";
import { FlagOutlined } from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { parse } from "query-string";
import classes from "./AddMarketInfo.module.scss";
import {
  FETCH_MARKETINFO_QUERY,
  GET_INDUSTRY_QUERY,
  GET_COMPANY_QUERY,
  GET_CATEGORIE_QUERY,
  GET_CANDIDATE_EMAIL_PHONE_QUERY,
} from "./utile/queries";
import { CREATE_MARKETINFO_MUTATION } from "./utile/mutations";

const { Title } = Typography;

function AddMarketInfo(props) {
  const { location, history } = props;
  const bhEntityId = parse(location.search).EntityID;
  const success = () => {
    message.success("Market Info added successfully");
  };
  const { loading, data: dataCandidate } = useQuery(
    GET_CANDIDATE_EMAIL_PHONE_QUERY,
    {
      skip: !bhEntityId,
      variables: {
        candidateID: bhEntityId,
      },
    }
  );
  const { data: dataComp } = useQuery(GET_COMPANY_QUERY);
  let companyList = null;
  let tab1 = [];
  if (dataComp) {
    companyList = dataComp.clientCorporations.map((comp) => comp.name);
    tab1 = companyList.map((comp) => {
      return { value: comp };
    });
  }

  const { data: dataI } = useQuery(GET_INDUSTRY_QUERY);
  let industryList = null;
  let tab = [];
  if (dataI) {
    industryList = dataI.businessSectors.map((sector) => sector.name);

    tab = industryList.map((sector) => {
      return { value: sector };
    });
  }

  const { data: dataCatego } = useQuery(GET_CATEGORIE_QUERY);
  let categoriesList = null;
  let tab2 = [];
  if (dataCatego) {
    categoriesList = dataCatego.categories.map((catego) => catego.name);

    tab2 = categoriesList.map((catego) => {
      return { value: catego };
    });
  }
  const valideCompany = (rule, value) => {
    if (value)
      if (value.length < 2 && value.length > 0)
        return Promise.reject(new Error("longeur non valide"));
    return Promise.resolve();
  };
  const valideContact = (rule, value) => {
    if (value)
      if (value.length < 3 && value.length > 0)
        return Promise.reject(new Error("longeur non valide"));
    return Promise.resolve();
  };
  const validejobTitle = (rule, value) => {
    if (value)
      if (value.length < 3 && value.length > 0)
        return Promise.reject(new Error("longeur non valide"));
    return Promise.resolve();
  };
  const validePhone = (rule, value) => {
    if (value)
      if (value.length < 8 && value.length > 0)
        return Promise.reject(new Error("longeur non valide"));
    return Promise.resolve();
  };
  const valideDescription = (rule, value) => {
    if (value)
      if (value.length < 3 && value.length > 0)
        return Promise.reject(new Error("longeur non valide"));
    return Promise.resolve();
  };
  const error = () => {
    message.error("error");
  };
  const [createMarketInfo] = useMutation(CREATE_MARKETINFO_MUTATION, {
    refetchQueries: () => [{ query: FETCH_MARKETINFO_QUERY }],
    onCompleted() {
      history.push("/home/marketInfo");
      success();
    },
    onError() {
      error();
    },
  });
  // if(loading) return null;
  const onFinish = (values) => {
    const tabIdustry = [];
    for (let i = 0; i < values.industry.length; i += 1) {
      tabIdustry.push({ name: values.industry[i] });
    }
    const tabCategories = [];
    for (let i = 0; i < values.skills.length; i += 1) {
      tabCategories.push({ name: values.skills[i] });
    }
    createMarketInfo({
      variables: {
        input: {
          company: values.company[0],
          jobTitle: values.jobTitle,
          contact: values.contact,
          email: values.Email,
          phone: values.phone,
          industry: tabIdustry,
          categories: tabCategories,
          description: values.description,
        },
      },
    });
  };
  return (
    <div>
      <Card className={classes.card} key={loading}>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Add Market Info
          </Title>
          <FlagOutlined className={classes.icon}> </FlagOutlined>
        </div>
        <br />
        <br />
        <Form name="nest-messages" onFinish={onFinish}>
          <Form.Item
            name="company"
            label="Company"
            rules={[
              {
                required: true,
                message: "Please input the company!",
              },
            ]}
          >
            {/* <Select
              showSearch
              style={{ width: "100%" }}
              optionFilterProp="children"
              options={tab1}
              filterOption={(input, options) =>
                options.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            /> */}
            <Select
              mode="tags"
              style={{ width: "100%" }}
              tokenSeparators={[","]}
              options={tab1}
              filterOption={(input, options) =>
                options.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            />
          </Form.Item>
          <Form.Item
            name="industry"
            label="Industry"
            rules={[
              {
                required: true,
                message: "Please input the Industry!",
              },
            ]}
          >
            <Select
              mode="multiple"
              style={{ width: "100%" }}
              optionLabelProp="label"
              options={tab}
            />
          </Form.Item>
          <Form.Item
            name="contact"
            label="Candidate ID"
            initialValue={bhEntityId && bhEntityId}
            rules={[
              {
                required: true,
                message: "Please input the contact!",
              },
              {
                validator: valideContact,
              },
              {
                max: 30,
                message: "Length must be less then 30 characters!",
              },
            ]}
          >
            <Input type="contact" name="contact" />
          </Form.Item>
          <Form.Item
            name="jobTitle"
            label="JobTitle"
            rules={[
              {
                required: true,
                message: "Please input the job title!",
              },
              {
                validator: validejobTitle,
              },
              {
                max: 30,
                message: "Length must be less then 30 characters!",
              },
            ]}
          >
            <Input type="jobTitle" name="jobTitle" />
          </Form.Item>
          <Form.Item
            label="Email"
            name="Email"
            htmlFor="emailinput"
            initialValue={
              dataCandidate &&
              dataCandidate.candidateEmailPHone &&
              dataCandidate.candidateEmailPHone.email
            }
            rules={[
              {
                required: true,
                message: "Please input the Email!",
              },
            ]}
          >
            <Input id="emailinput" />
          </Form.Item>
          <Form.Item
            name="phone"
            label="Phone"
            initialValue={
              dataCandidate &&
              dataCandidate.candidateEmailPHone &&
              dataCandidate.candidateEmailPHone.mobile
            }
            rules={[
              {
                validator: validePhone,
              },
              {
                required: true,
                message: "Please input the phone number!",
              },
              {
                max: 30,
                message: "Length must be less then 30 characters!",
              },
            ]}
          >
            <Input type="phone" name="phone" />
          </Form.Item>
          <Form.Item
            name="skills"
            label="Community/Technical Stack"
            rules={[
              {
                required: true,
                message: "Please input skills!",
              },
            ]}
          >
            <Select
              mode="multiple"
              style={{ width: "100%" }}
              optionLabelProp="label"
              options={tab2}
            />
          </Form.Item>
          <Form.Item
            name="description"
            label="Description"
            rules={[
              {
                required: true,
                message: "Please input the Description!",
              },
              {
                validator: valideDescription,
              },
              {
                max: 3000,
                message: "Length must be less then 3000 characters!",
              },
            ]}
          >
            <Input.TextArea type="description" name="description" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Add
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}

AddMarketInfo.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    search: PropTypes.func,
  }).isRequired,
};
export default withRouter(AddMarketInfo);
