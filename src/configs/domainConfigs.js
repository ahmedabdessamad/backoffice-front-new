import runtimeVars from "./runTimeVars";

// expiration date for the cookie: 1 year
const after1year = new Date();
after1year.setFullYear(after1year.getFullYear() + 1);

const cookieSaveOpt = {
  path: "/",
  domain: runtimeVars.DOMAIN,
  expires: after1year,
};
const cookieRemoveOpt = {
  path: "/",
  domain: runtimeVars.DOMAIN,
};

export { cookieSaveOpt, cookieRemoveOpt };
