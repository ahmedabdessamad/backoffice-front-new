import React from "react";
import Details from "../BatchPayment-Components/Details";

export default function BatchPaymentDetails() {
  return (
    <Details
      title=" A new batch payment has been sent from xero"
      path="/home/sas_logs"
    />
  );
}
