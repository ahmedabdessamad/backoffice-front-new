const actions = {
  TOGGLE_USER_MODAL: "TOGGLE_USER_MODAL",
  toggleUserModal: (payload = {}) => ({
    type: actions.TOGGLE_USER_MODAL,
    payload,
  }),
};

export default actions;
