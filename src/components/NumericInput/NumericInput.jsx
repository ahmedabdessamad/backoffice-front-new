import React from "react";
import { Input, Tooltip } from "antd";
import * as PropTypes from "prop-types";
import formatNumber from "../../helpers/formatNumber";

function NumericInput(props) {
  const { value } = props;
  const title = value ? (
    <span className="numeric-input-title">{formatNumber(value)}</span>
  ) : (
    "Input the client Id"
  );

  const onChange = (e) => {
    const { value } = e.target;
    const reg = /^\d*$/;
    if ((!isNaN(value) && reg.test(value)) || value === "") {
      props.onChange(value);
    }
  };

  // '.' at the end or only '-' in the input box.
  const onBlur = () => {
    const { value, onBlur, onChange } = props;
    let valueTemp = value;
    if (value.charAt(value.length - 1) === "." || value === "-") {
      valueTemp = value.slice(0, -1);
    }
    onChange(valueTemp.replace(/0*(\d+)/, "$1"));
    if (onBlur) {
      onBlur();
    }
  };

  return (
    <Tooltip
      trigger={["focus"]}
      title={title}
      placement="topLeft"
      overlayClassName="numeric-input"
    >
      <Input
        {...props}
        onChange={onChange}
        onBlur={onBlur}
        placeholder="Input the client Id"
        size="large"
        maxLength={25}
      />
    </Tooltip>
  );
}

NumericInput.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  onBlur: PropTypes.func.isRequired,
};

export default NumericInput;
