import React, { useState } from "react";
import {
  Form,
  Input,
  InputNumber,
  Button,
  Typography,
  Card,
  DatePicker,
  Select,
} from "antd";
import * as PropTypes from "prop-types";
import { FlagOutlined } from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import moment from "moment";
import { useQuery } from "@apollo/react-hooks";
import { useSelector } from "react-redux";
import classes from "./AddMissedDeal.module.scss";
import { GET_SKILLS_QUERY, GET_COMPANY_QUERY } from "./utile/queries";
import ModalUpdateCandidate from "../../components/Modal/ModalUpdateCandidate";

const { Title } = Typography;

function AddMissedDeal(props) {
  const [company, setCompany] = useState("");
  const [ITConsultingFirm, setITConsultingFirm] = useState("");
  const [intermedianryAgency, setIntermedianryAgency] = useState("");
  const [missionStart, setMissionStart] = useState(moment());
  const [missionEnd, setMissionEnd] = useState(moment());
  const [skills, setSkills] = useState([{}]);
  const [jobTitle, setJobTitle] = useState("");
  const [dailyRate, setDailyRate] = useState({});
  const [city, setCity] = useState("");
  const [missionProjectDescription, setMissionProjectDescription] = useState(
    ""
  );
  const [candidate, setCandidate] = useState("");
  const { history } = props;
  const [visible, setVisible] = useState(false);
  const [missedDeal, setMissedDeal] = useState({});
  const MutationType = "create";
  const bhEntityId = useSelector(
    (state) => state.BhEntityReducer.bhEntity.entityId
  );
  console.log("bhEntity", bhEntityId);
  if (bhEntityId) {
    if (candidate === "") {
      setCandidate(bhEntityId);
    }
  }
  const { data } = useQuery(GET_SKILLS_QUERY);
  let skillsList = null;
  const tab = [];
  if (data) {
    skillsList = data.skills.map((skill) => skill.name);

    skillsList.map((skill) => {
      tab.push({ value: skill });
      return null;
    });
  }

  const { data: dataComp } = useQuery(GET_COMPANY_QUERY);
  let companyList = null;
  const tab1 = [];
  if (dataComp) {
    companyList = dataComp.clientCorporations.map((comp) => comp.name);

    companyList.map((comp) => {
      tab1.push({ value: comp });
    });
  }

  const valideCompany = (rule, value) => {
    if (value) {
      if (value.length < 2 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };

  const valideItConsultingFirm = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const valideIntermedianryAgency = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const validejobTitle = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const valideCity = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const valideMissionProjectDescription = (rule, value) => {
    if (value) {
      if (value.length < 3 && value.length > 0) {
        return Promise.reject(new Error("longeur non valide"));
      }
    }
    return Promise.resolve();
  };
  const valideMissionEnd = (rule, value) => {
    if (value) {
      if (value < missionStart) {
        return Promise.reject(
          new Error(
            "The mission end date must be after the mission start date!"
          )
        );
      }
    }
    return Promise.resolve();
  };

  const changecompanyHandler = (value) => {
    setCompany(value[0]);
  };
  const changeITConsultingFirmHandler = (event) => {
    setITConsultingFirm(event.target.value);
  };
  const changeintermedianryAgencyHandler = (event) => {
    setIntermedianryAgency(event.target.value);
  };
  const changemissionStartHandler = (date) => {
    setMissionStart(date);
  };
  const changemissionEndHandler = (date) => {
    setMissionEnd(date);
  };
  const changeskillsHandler = (value) => {
    const tabskills = [];
    for (let i = 0; i < value.length; i += 1) {
      tabskills.push({ name: value[i] });
    }
    setSkills(tabskills);
  };

  const changejobTitleHandler = (event) => {
    setJobTitle(event.target.value);
  };
  const changedailyRateHandler = (number) => {
    setDailyRate(number.toString());
  };
  const changecityHandler = (event) => {
    setCity(event.target.value);
  };
  const changemissionProjectDescriptionHandler = (event) => {
    setMissionProjectDescription(event.target.value);
  };
  const changecandidateHandler = (event) => {
    setCandidate(event.target.value);
  };

  const onFinish = () => {
    setMissedDeal({
      company,
      candidate,
      ITConsultingFirm,
      intermedianryAgency,
      missionStart,
      missionEnd,
      skills,
      jobTitle,
      dailyRate,
      city,
      missionProjectDescription,
    });
    setVisible(true);
  };

  return (
    <div>
      <ModalUpdateCandidate
        visible={visible}
        missedDeal={missedDeal}
        setVisible={setVisible}
        history={history}
        MutationType={MutationType}
      />
      <Card className={classes.card}>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Add Missed Deal
          </Title>
          <FlagOutlined className={classes.icon}> </FlagOutlined>
        </div>
        <br />
        <br />
        <Form name="nest-messages" onFinish={onFinish}>
          <Form.Item
            name="company"
            label="Company"
            rules={[
              {
                required: true,
                message: "Please input the company!",
              },
            ]}
          >
            {/* <Select
              showSearch
              style={{ width: "100%" }}
              optionFilterProp="children"
              onChange={changecompanyHandler}
              options={tab1}
              filterOption={(input, options) =>
                options.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            /> */}
            <Select
              mode="tags"
              style={{ width: "100%" }}
              onChange={changecompanyHandler}
              tokenSeparators={[","]}
              options={tab1}
              filterOption={(input, options) =>
                options.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            />
          </Form.Item>
          {bhEntityId ? (
            <Form.Item
              name="candidate"
              label="Candidate ID"
              rules={[
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="candidate"
                onChange={changecandidateHandler}
                name="candidate"
                defaultValue={candidate}
                value={candidate}
              />
            </Form.Item>
          ) : (
            <Form.Item
              name="candidate"
              label="Candidate ID"
              rules={[
                {
                  required: true,
                  message: "Please input the candidate!",
                },
                {
                  max: 30,
                  message: "Length must be less then 30 characters!",
                },
              ]}
            >
              <Input
                type="candidate"
                onChange={changecandidateHandler}
                name="candidate"
                value={candidate}
              />
            </Form.Item>
          )}
          <Form.Item
            name="ITConsultingFirm"
            label="IT Consulting Firm"
            rules={[
              {
                validator: valideItConsultingFirm,
              },
              {
                max: 30,
                message: "Length must be less then 30 characters!",
              },
            ]}
          >
            <Input
              type="ITConsultingFirm"
              onChange={changeITConsultingFirmHandler}
              name="ITConsultingFirm"
              value={ITConsultingFirm}
            />
          </Form.Item>
          <Form.Item
            name="intermedianryAgency"
            label="Intermediary Agency"
            rules={[
              {
                validator: valideIntermedianryAgency,
              },
              {
                max: 30,
                message: "Length must be less then 30 characters!",
              },
            ]}
          >
            <Input
              type="intermedianryAgency"
              onChange={changeintermedianryAgencyHandler}
              name="intermedianryAgency"
              value={intermedianryAgency}
            />
          </Form.Item>
          <Form.Item
            name="missionStart"
            label="Mission Start"
            rules={[
              {
                required: true,
                message: "Please input the Mission Start's date!",
              },
            ]}
          >
            <DatePicker
              initialValues={moment("2020/01/01", "YYYY/MM/DD")}
              format="YYYY/MM/DD"
              onChange={changemissionStartHandler}
              name="missionStart"
              value={missionStart}
            />
          </Form.Item>
          <Form.Item
            name="missionEnd"
            label="Mission End"
            rules={[
              {
                required: true,
                message: "Please input the Mission End's date!",
              },
              {
                validator: valideMissionEnd,
              },
            ]}
          >
            <DatePicker
              initialValues={moment("2020/01/01", "YYYY/MM/DD")}
              format="YYYY/MM/DD"
              onChange={changemissionEndHandler}
              name="missionEnd"
              value={missionEnd}
            />
          </Form.Item>

          <Form.Item
            name="skills"
            label="Skills"
            rules={[
              {
                required: true,
                message: "Please input the skills!",
              },
            ]}
          >
            <Select
              mode="multiple"
              style={{ width: "100%" }}
              onChange={changeskillsHandler}
              optionLabelProp="label"
              options={tab}
            />
          </Form.Item>
          <Form.Item
            name="jobTitle"
            label="JobTitle"
            rules={[
              {
                required: true,
                message: "Please input the job title!",
              },
              {
                validator: validejobTitle,
              },
              {
                max: 30,
                message: "Length must be less then 30 characters!",
              },
            ]}
          >
            <Input
              type="jobTitle"
              onChange={changejobTitleHandler}
              name="jobTitle"
              value={jobTitle}
            />
          </Form.Item>
          <Form.Item
            name="dailyRate"
            label="Daily Rate"
            rules={[
              {
                type: "number",
                min: 0,
                max: 9999,
                required: true,
                message: "the daily rate must be a number!",
              },
            ]}
          >
            <InputNumber
              step={0.1}
              onChange={changedailyRateHandler}
              name="dailyRate"
              value={dailyRate}
            />
          </Form.Item>
          <Form.Item
            name="city"
            label="City"
            rules={[
              {
                required: true,
                message: "Please input the city's name!",
              },
              {
                validator: valideCity,
              },
              {
                max: 30,
                message: "Length must be less then 30 characters!",
              },
            ]}
          >
            <Input
              type="city"
              onChange={changecityHandler}
              name="city"
              value={city}
            />
          </Form.Item>
          <Form.Item
            name="missionProjectDescription"
            label="Mission Project Description"
            rules={[
              {
                required: true,
                message: "Please input the Description!",
              },
              {
                validator: valideMissionProjectDescription,
              },
              {
                max: 3000,
                message: "Length must be less then 3000 characters!",
              },
            ]}
          >
            <Input.TextArea
              type="missionProjectDescription"
              onChange={changemissionProjectDescriptionHandler}
              name="missionProjectDescription"
              value={missionProjectDescription}
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Add
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}

AddMissedDeal.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(AddMissedDeal);
