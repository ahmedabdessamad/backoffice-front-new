const getDate = (timestamp) => {
  // initialize new Date object
  const dateOb = new Date(timestamp);

  // year as 4 digits (YYYY)
  const year = dateOb.getFullYear();

  // month as 2 digits (MM)
  const month = `0${dateOb.getMonth() + 1}`.slice(-2);

  // date as 2 digits (DD)
  const date = `0${dateOb.getDate()}`.slice(-2);

  // hours as 2 digits (hh)
  // const hours = `0${dateOb.getHours()}`.slice(-2);

  // minutes as 2 digits (mm)
  // const minutes = `0${dateOb.getMinutes()}`.slice(-2);

  // seconds as 2 digits (ss)
  // const seconds = `0${dateOb.getSeconds()}`.slice(-2);
  return `${date}/${month}/${year}`;
  // date as YYYY-MM-DD format
  // console.log(`Date as YYYY-MM-DD Format: ${date}/${month}/${year}`);

  // console.log("\r\n");

  // date & time as YYYY-MM-DD hh:mm:ss format:
  /* console.log(
    `Date as YYYY-MM-DD hh:mm:ss Format: ${year}-${month}-${date} ${hours}:${minutes}:${seconds}`
  ); */

  // console.log("\r\n");

  // time as hh:mm format:
  // console.log(`Time as hh:mm Format: ${hours}:${minutes}`);
};

export default getDate;
