import { gql } from "graphql.macro";

const SEND_MESSAGE_TIME_SHEET = gql`
  mutation SendMessageTimesheet(
    $timesheetID: Int!
    $date: Float
    $message: String!
  ) {
    sendMessageTimesheet(
      timesheetID: $timesheetID
      date: $date
      message: $message
    ) {
      id
      client {
        id
        firstName
        lastName
      }
      candidate {
        id
        firstName
        lastName
      }
      timesheetID
      dateAdded
      dateLastUpdate
      messages {
        id
        text
        date
        speaker
      }
    }
  }
`;

const UPDATE_COMMENT = gql`
  mutation UpdateComment($timesheetID: Int!, $comment: String!) {
    updateComment(timesheetID: $timesheetID, comment: $comment) {
      id
      client {
        id
        firstName
        lastName
      }
      candidate {
        id
        firstName
        lastName
      }
      timesheetID
      dateAdded
      dateLastUpdate
      messages {
        id
        text
        date
        speaker
      }
      comment
    }
  }
`;

export { SEND_MESSAGE_TIME_SHEET, UPDATE_COMMENT };
