/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import * as PropTypes from "prop-types";
import { Typography, Menu, Form, Button, message, Card } from "antd";
import { withRouter } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  MinusCircleOutlined,
  AlignLeftOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { useMutation, useQuery } from "@apollo/react-hooks";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import classes from "./EditFormComponent.module.scss";
import InputComponent from "../../components/Form/InputComponent";
import CheckboxComponent from "../../components/Form/CheckboxComponent";
import ModalInput from "../../components/Form/ModalInput";
import EditModalInput from "../../components/Form/EditModalInput";
import checkboxChoicesActions from "../../redux/checkboxChoices/actions";
import { UPDATE_FORM_MUTATION } from "./utile/mutations";
import { FETCH_FORM_QUERY, FETCH_ONE_FORM_QUERY } from "./utile/queries";

const { Title } = Typography;
const { SubMenu } = Menu;
const { setInputList } = checkboxChoicesActions;

function EditFormComponent(props) {
  const dispatch = useDispatch();
  const { inputList } = useSelector((state) => state.CheckboxChoicesReducer);
  const [id, setId] = useState("");
  const { history, match } = props;
  // const [inputList_old, setInputList] = useState([]);
  const [visibleModalInput, setVisibleModalInput] = useState(false);
  const [visibleEditModalInput, setVisibleEditModalInput] = useState(false);
  const [formItem, setFormItem] = useState(null);
  const [formItemIndex, setFormItemIndex] = useState(null);
  const [errors, setErrors] = useState({});
  const [menuKey, setMenuKey] = useState("");
  const [formSubject, setFormSubject] = useState("");

  const formIDfromUrl = match.params.id;

  useEffect(() => {
    for (let i = 0; i < inputList.length; i++) {
      if (inputList[i].key === "FormTitle") {
        setFormSubject(inputList[i].name);
      }
    }
  }, [inputList]);

  const success = () => {
    message.success("Form updated successfully");
  };
  const success2 = () => {
    message.error("Please add a title!");
  };
  const { data } = useQuery(FETCH_ONE_FORM_QUERY, {
    variables: {
      formID: formIDfromUrl,
    },
    fetchPolicy: "network-only",
  });

  if (data) {
    const { formID, structure } = data.form;
    if (!id) {
      setId(formID);
      dispatch(setInputList(structure));
    }
  }

  const [editForm] = useMutation(UPDATE_FORM_MUTATION, {
    variables: {
      formID: id,
      subject: formSubject,
      structure: inputList,
    },

    refetchQueries: () => [{ query: FETCH_FORM_QUERY }],
    onCompleted() {
      history.push("/home/formList");
      success();
      dispatch(setInputList([]));
    },
    onError(err) {
      setErrors(err.graphQLErrors[0].message);
    },
  });

  const handleClickMenu = (e) => {
    setMenuKey(e.key);
    if (e.key === "simpleInput" || e.key === "FormTitle") {
      setVisibleModalInput(true);
    }
    if (e.key === "Checkbox") {
      setVisibleModalInput(true);
    }
    if (e.key === "inputText") {
      setVisibleModalInput(true);
    }
    if (e.key === "inputNumber") {
      setVisibleModalInput(true);
    }
  };

  // handle click event of the Remove button
  const handleRemoveClick = (index) => {
    const list = [...inputList];
    list.splice(index, 1);
    console.log("list1111", list);
    dispatch(setInputList(list));
  };
  const handleEditClick = (index, x) => {
    console.log("xxxxxxxxx", x);
    setMenuKey(x.key);
    setFormItemIndex(index);
    setFormItem(x);
    setVisibleEditModalInput(true);
  };
  // handle click event of the Add button
  const handleOnClick = () => {
    if (formSubject === "") {
      success2();
    } else {
      editForm();
    }
  };

  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <ModalInput
          visibleModalInput={visibleModalInput}
          setVisibleModalInput={setVisibleModalInput}
          inputList={inputList}
          menuKey={menuKey}
        />
        <EditModalInput
          visibleEditModalInput={visibleEditModalInput}
          setVisibleEditModalInput={setVisibleEditModalInput}
          formItem={formItem}
          setFormItem={setFormItem}
          formItemIndex={formItemIndex}
          setFormItemIndex={formItemIndex}
          menuKey={menuKey}
        />
        <div className={classes.content}>
          <Menu
            mode="vertical"
            style={{ width: 256 }}
            onClick={handleClickMenu}
          >
            <Menu.Item key="FormTitle">Form Title</Menu.Item>
            <SubMenu icon={<AlignLeftOutlined />} key="input" title="Input">
              <Menu.Item key="simpleInput">Short answer</Menu.Item>
              <Menu.Item key="inputText">Paragraph</Menu.Item>
              <Menu.Item key="inputNumber">Number</Menu.Item>
            </SubMenu>
            <Menu.Item key="Checkbox">Checkbox</Menu.Item>
            <Menu.Item key="DatePicker">DatePicker</Menu.Item>
            {/*  <Menu.Item key="Radio">Radio</Menu.Item>
            <Menu.Item key="Rate">Rate</Menu.Item>
            <Menu.Item key="Select">Select</Menu.Item>
            <Menu.Item key="TimePicker">TimePicker</Menu.Item>
            <Menu.Item key="TreeSelect">TreeSelect</Menu.Item>
  <Menu.Item key="Switch">Switch</Menu.Item> */}
          </Menu>

          <div className="App">
            <Card className={classes.card}>
              {inputList.map((x, i) => {
                if (x.key === "FormTitle") {
                  return (
                    <Title level={3} className={classes.title}>
                      {x.name}
                    </Title>
                  );
                }
              })}
              <Form layout="vertical">
                {inputList.map((x, i) => {
                  if (x.key === "Checkbox") {
                    console.log("CheckBox input list =", x);
                    return (
                      <div>
                        <Form.Item
                          name={x.name}
                          label={x.name}
                          rules={[
                            {
                              required: x.required,
                              message: "This field is required!",
                            },
                          ]}
                        >
                          <div className="btn-box">
                            <div className={classes.inputWrapper}>
                              <CheckboxComponent
                                name={x.name}
                                choices={x.choices}
                              />
                              {inputList.length !== 1 && (
                                <div style={{ display: "flex" }}>
                                  <EditOutlined
                                    className={classes.icon1}
                                    onClick={() => handleEditClick(i, x)}
                                  />
                                  <MinusCircleOutlined
                                    className={classes.icon}
                                    onClick={() => handleRemoveClick(i)}
                                  />
                                </div>
                              )}
                              {inputList.length - 1 === i}
                            </div>
                          </div>
                        </Form.Item>
                      </div>
                    );
                  }
                  if (
                    x.key === "simpleInput" ||
                    x.key === "inputText" ||
                    x.key === "inputNumber"
                  ) {
                    return (
                      <div>
                        <Form.Item
                          name={x.name}
                          label={x.name}
                          rules={[
                            {
                              required: x.required,
                              message: "This field is required!",
                            },
                          ]}
                        >
                          <div className="btn-box">
                            <div className={classes.inputWrapper}>
                              <InputComponent x={x} i={i} />
                              {inputList.length !== 1 && (
                                <div style={{ display: "flex" }}>
                                  <EditOutlined
                                    className={classes.icon1}
                                    onClick={() => handleEditClick(i, x)}
                                  />
                                  <MinusCircleOutlined
                                    className={classes.icon}
                                    onClick={() => handleRemoveClick(i)}
                                  />
                                </div>
                              )}
                              {inputList.length - 1 === i}
                            </div>
                          </div>
                        </Form.Item>
                      </div>
                    );
                  }
                })}
                {inputList.length > 0 ? (
                  <Button type="primary" disabled>
                    Submit
                  </Button>
                ) : null}
              </Form>
            </Card>
            {inputList.length < 2 ? (
              <Button type="primary" disabled className={classes.createButton}>
                Create
              </Button>
            ) : (
              <Button
                type="primary"
                className={classes.createButton}
                onClick={handleOnClick}
              >
                Save Edit
              </Button>
            )}
            {/* <div style={{ marginTop: 20 }}>{JSON.stringify(inputList)}</div> */}
          </div>
        </div>
      </ContentContainer>
    </ContentWrapper>
  );
}

EditFormComponent.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(EditFormComponent);
