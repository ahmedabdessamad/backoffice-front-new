import React from "react";
import * as PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { Typography, Tabs } from "antd";
import { useSelector } from "react-redux";
import classes from "./Timesheets.module.scss";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import Reports from "./Tabs/Reports";
import Logs from "../Logs/index";
import Mailing from  "../Mailing/Tabs/Sent";
import Placements from "./Tabs/Placements/timesheetable-placements";
import AllTimesheets from "./Tabs/AllTimesheets";
import { isUserAuthorized } from "../../helpers/authorization/permissionsCheckHelpers";

const { Title } = Typography;
const { TabPane } = Tabs;

function Timesheets(props) {
  // set up url
  const { location, history } = props;
  const { pathname } = location;
  const splitted = pathname.split("/");
  const user = useSelector((state) => state.AuthReducer.user);
  let defaultLocation = "timesheets";
  if (splitted.length === 4) {
    defaultLocation = splitted[3];
  } else history.replace("/home/timesheets/timesheets");

  const navigate = (key) => {
    history.push(`/home/timesheets/${key}`);
  };

  return (
    <ContentWrapper style={{ height: "110vh" }}>
      <ContentContainer>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Timesheets
          </Title>
        </div>
        <Tabs defaultActiveKey={defaultLocation} onChange={navigate}>
          {isUserAuthorized(user, {
            TIMESHEETS: 7,
          }) && (
            <TabPane tab="Timesheets" key="timesheets">
              <AllTimesheets />
            </TabPane>
          )}
          {isUserAuthorized(user, {
            REPORTS: 7,
          }) && (
            <TabPane tab="Reports" key="reports">
              <Reports />
            </TabPane>
          )}
          {isUserAuthorized(user, {
            PLACEMENTS: 7,
          }) && (
            <TabPane tab="Placements" key="placements">
              <Placements />
            </TabPane>
          )}
          {isUserAuthorized(user, {
            TIMESHEETLOGS: 7,
          }) && (
            <TabPane tab="Logs" key="logs">
              <Logs />
            </TabPane>
          )}
          {isUserAuthorized(user, {
            TIMESHEETEMAILS: 7,
          }) && (
            <TabPane tab="E-mails" key="mailing">
              <Mailing />
            </TabPane>
          )}
        </Tabs>
      </ContentContainer>
    </ContentWrapper>
  );
}

Timesheets.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
};
export default withRouter(Timesheets);
