import React from "react";
import PropTypes from "prop-types";
import { Button } from "cf-neo-ui";
import { useMutation } from "@apollo/react-hooks";
import { withRouter } from "react-router-dom";
import { message } from "antd";
import { useSelector } from "react-redux";
import classes from "./ConclusionSection.module.scss";
import { UPDATE_TIMESHEET_STATUS_MUTATION } from "../mutations";
import { checkPermissionAndGetRoleHelpers } from "../../../../helpers/authorization";

function ConclusionSection(props) {
  const { status, totalSpecial, match, history } = props;

  const { user } = useSelector((state) => state.AuthReducer);
  const userRole = checkPermissionAndGetRoleHelpers({ TIMESHEETS: 2 }, user);


  function formatTotal() {
    const { total } = props;
    return {
      days: Math.floor(total / 8),
      hours: Math.floor(total % 8),
      minutes: Math.floor(((total % 8) % 1) * 60),
    };
  }

  const success = () => {
    message.success("Timesheet's status updated successfully");
  };

  const total = formatTotal();

  const [update, { data }] = useMutation(UPDATE_TIMESHEET_STATUS_MUTATION, {
    onCompleted() {
      history.push("/home/timesheets");
      success();
    },
    onError(err) {
      console.error(err);
    },
  });

  function changeTimesheetStatus(newStatus) {
    console.log("hereeee timesheet", parseInt(match.params.id, 10), newStatus);
    update({
      variables: {
        id: parseInt(match.params.id, 10),
        status: newStatus,
      },
    });
  }

  const getRightButton = () => {
    let reopenButton = false;
    let rejectedButton = false;

    if (userRole === "Reader") {
      reopenButton = true;
      rejectedButton = true;
    } else if (status === "pending") {
      reopenButton = true;
      rejectedButton = false;
    } else if (
      status === "open" ||
      status === "draft" ||
      status === "rejected"
    ) {
      reopenButton = true;
      rejectedButton = true;
    } else {
      reopenButton = false;
      rejectedButton = true;
    }

    return (
      <div className={classes.actions}>
        <Button
          disabled={rejectedButton}
          className={classes.action}
          variant="primary"
          onClick={() => changeTimesheetStatus("rejected")}
        >
          Rejeter
        </Button>
        <span className={classes.action} />
        <Button
          disabled={reopenButton}
          className={classes.action}
          variant="secondary"
          onClick={() => changeTimesheetStatus("open")}
        >
          Réouvrir
        </Button>
      </div>
    );
  };
  return (
    <div className={classes.container}>
      <div className={classes.conclusionBox}>
        <div className={classes.header}>Totale</div>
        <div className={classes.content}>
          <div className={classes.item}>
            <span>
              <span>{total.days} </span>
              <span>j</span>
              <span>{total.hours} </span>
              <span>h</span>
              {total.minutes ? <span>{total.minutes}</span> : null}
              {total.minutes ? <span>mn</span> : null}
            </span>
            <p>normal</p>
          </div>
          <div className={classes.item}>
            <span>
              <span>{totalSpecial.hours} </span>
              <span>h</span>
              <span>{totalSpecial.minutes} </span>
              <span>mn</span>
            </span>
            <p>special</p>
          </div>
        </div>
      </div>
      <div className={classes.actions}>{getRightButton()}</div>
    </div>
  );
}

ConclusionSection.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  timesheetStore: PropTypes.shape({
    total: PropTypes.number,
    validationSpecialHours: PropTypes.func,
    comment: PropTypes.string,
    timesheet: PropTypes.shape,
    totalSpecial: PropTypes.shape({
      hours: PropTypes.number,
      minutes: PropTypes.number,
    }),
    updateSelectedDays: PropTypes.func,
    specialHours: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        date: PropTypes.number,
        hours: PropTypes.number,
        minutes: PropTypes.number,
        type: PropTypes.string,
      })
    ),
    selectedDays: PropTypes.arrayOf(
      PropTypes.shape({
        day: PropTypes.number,
        hours: PropTypes.number,
      })
    ),
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
  i18n: PropTypes.shape({
    _: PropTypes.func,
  }).isRequired,
  totalSpecial: PropTypes.shape.isRequired,
  total: PropTypes.shape.isRequired,
  status: PropTypes.string.isRequired,
};

export default withRouter(ConclusionSection);
