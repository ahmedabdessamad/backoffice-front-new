import { gql } from "graphql.macro";

const REPORTS_QUERY = gql`
  query Reports(
    $limit: Int
    $skip: Int
    $periodStart: Float
    $periodEnd: Float
    $placementIDs: [Int]
    $timesheetIDs: [Int]
  ) {
    reports(
      filter: {
        limit: $limit
        skip: $skip
        periodStart: $periodStart
        periodEnd: $periodEnd
        timesheetIDs: $timesheetIDs
        placementIDs: $placementIDs
      }
    ) {
      _id
      dateLastUpdate
      placementID
      timesheetID
      frFile
      enFile
      madeBy {
        id
        name
      }
      total
    }
  }
`;

export default REPORTS_QUERY;
