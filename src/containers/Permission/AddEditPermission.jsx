import React, { useEffect, useState } from "react";
import {
  Modal,
  Form,
  Input,
  Select,
  message,
  Button,
  Space,
  Checkbox,
} from "antd";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/react-hooks/lib/index";
import classes from "./Permission.module.scss";
import { addPermissionMutation as ADD_PERMISSION_MUTATION } from "./mutations";
import { GET_PERMISSIONS } from "./queries";
import permissionsAction from "../../redux/permissions/actions";
import { TAB_NAMES, SUBTABS_NAMES } from "../../configs/constant";
import "./style.css";
import { FETCH_MARKETINFO_QUERY } from "../MarketInfo/utile/queries";

const { togglePermissionModal } = permissionsAction;

const { Option } = Select;

const PARENTS_DISPLAY_NAME = Object.keys(SUBTABS_NAMES);

export default function AddEditPermission() {
  const { modalVisible, permission } = useSelector(
    (state) => state.PermissionReducer
  );
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const [fieldsTabs, setFieldsTabs] = useState({});
  const [tabsOptions, setTabsOptions] = useState(PARENTS_DISPLAY_NAME);
  const [subTabsPermission, setSubTabsPermission] = useState([]);

  const [addPermission] = useMutation(ADD_PERMISSION_MUTATION, {
    onCompleted: () => message.success("Permission Successfully Created!"),
    onError: () => {
      message.error("Oops Something went wrong");
    },
    refetchQueries: () => [{ query: GET_PERMISSIONS }],
  });

  const tabSelectHandler = (fieldName, tabName) => {
    // map field to its tab
    setFieldsTabs((prevState) => ({ ...prevState, [fieldName]: tabName }));
  };

  // add/remove selected tab from tabs options on fieldsTabs update
  useEffect(() => {
    const newOptions = PARENTS_DISPLAY_NAME.filter(
      (t) => !Object.values(fieldsTabs).includes(t)
    );
    setTabsOptions(newOptions);
  }, [fieldsTabs]);

  const exist = (tabsPermission, subTname) => {
    let i = 0;
    for (i = 0; i < tabsPermission.length; i += 1) {
      if (subTname === tabsPermission[i].name) {
        return i;
      }
    }
    return -1;
  };

  // prepare permission to be sent in mutatuion if the tab has subtabs
  const permissionToBinary = async (checkedValues, subtab, tab) => {
    let perm = 0;
    checkedValues.map((val) => {
      if (val === "Reader") perm += 1;
      if (val === "Editor") perm += 2;
      if (val === "Admin") perm += 4;
    });
    // changing the subtab name to its forma in our code exemple: "Missed Deal" to "MISSEDDEAL"
    const subTname = Object.keys(TAB_NAMES).find(
      (key) => TAB_NAMES[key] === subtab
    );
    const tname = Object.keys(TAB_NAMES).find((key) => TAB_NAMES[key] === tab);
    const ob = {
      name: subTname,
      permission: perm,
      parent: tname,
    };
    // setSubTabsPermission((prevState) => [...prevState, ob]);
    const isExist = exist(subTabsPermission, subTname);
    const isExist2 = exist(subTabsPermission, tname);
    if (subTabsPermission.length === 0 || isExist === -1) {
      setSubTabsPermission([...subTabsPermission, ob]);
      if (isExist2 === -1 || subTabsPermission.length === 0) {
        const obParent = {
          name: tname,
          permission: perm,
        };
        setSubTabsPermission([...subTabsPermission, obParent, ob]);
      } else {
        setSubTabsPermission([...subTabsPermission, ob]);
      }
      // if the sub tab is unchekecked after bin checked => delete it from subTabsPermission
    } else if (perm === 0) {
      const newArr1 = [...subTabsPermission];
      newArr1.splice(isExist, 1);
      setSubTabsPermission(newArr1);
    } else {
      const newArr = [...subTabsPermission];
      newArr[isExist] = ob;
      setSubTabsPermission(newArr);
    }
  };
  // prepare permission to be sent in mutatuion if the tab has no subtabs
  const permissionToBinary2 = (checkedValues, tab) => {
    let perm2 = 0;
    checkedValues.map((val) => {
      if (val === "Reader") perm2 += 1;
      if (val === "Editor") perm2 += 2;
      if (val === "Admin") perm2 += 4;
    });
    // changing the subtab name to its forma in our code exemple: "Missed Deal" to "MISSEDDEAL"
    const tname = Object.keys(TAB_NAMES).find((key) => TAB_NAMES[key] === tab);
    const ob = {
      name: tname,
      permission: perm2,
    };
    // setSubTabsPermission((prevState) => [...prevState, ob]);
    const isExist = exist(subTabsPermission, tname);
    if (subTabsPermission.length === 0 || isExist === -1) {
      setSubTabsPermission([...subTabsPermission, ob]);
    } else {
      const newArr = [...subTabsPermission];
      newArr[isExist] = ob;
      setSubTabsPermission(newArr);
    }
  };

  const renderSubTabs = (tab) => {
    //  console.log("Field name in render sub tabs: ", tab)
    if (Object.keys(SUBTABS_NAMES[tab]).length !== 0) {
      return Object.values(SUBTABS_NAMES[tab]).map((subtab) => {
        const onChange = (checkedValue) => {
          permissionToBinary(checkedValue, subtab, tab);
        };
        return (
          <div style={{ display: "flex", height: "20px" }} key={subtab}>
            <div className={classes.permissionLabel}>
              <Form.Item name="name" className={classes.subtabName}>
                {subtab}
              </Form.Item>
            </div>
            <div className="checkboxList">
              <Checkbox.Group
                onChange={onChange}
                options={["Admin", "Editor", "Reader"]}
              />
            </div>
          </div>
        );
      });
    }
    const onChange2 = (checkedValue) => {
      permissionToBinary2(checkedValue, tab);
    };
    return (
      <div className="checkboxListNoSubTabs" key={tab}>
        <Checkbox.Group
          onChange={onChange2}
          options={["Admin", "Editor", "Reader"]}
        />
      </div>
    );
  };

  const removeFieldHandler = (fieldName) => {
    const tab = fieldsTabs[fieldName];
    // remove tab and subtab from subTabsPermission
    const tabName = Object.keys(TAB_NAMES).find(
      (key) => TAB_NAMES[key] === tab
    );
    const newsubTbs = subTabsPermission.filter((p) => {
      return p.name !== tabName && p.parent !== tabName;
    });
    setSubTabsPermission(newsubTbs);
    // remove field from fieldsTabs state
    setFieldsTabs((prevState) => ({ ...prevState, [fieldName]: null }));
    // add tab of the fieldName to tab options
    //setTabsOptions((prevState) => [tab, ...prevState]);
  };

  return (
    <Modal
      visible={modalVisible}
      width="50%"
      title="Create New Permission"
      okText="Create"
      onClose={() => {
        dispatch(togglePermissionModal());
        form.resetFields();
        setSubTabsPermission([]);
      }}
      onCancel={() => {
        dispatch(togglePermissionModal());
        form.resetFields();
        // reset state too
        setFieldsTabs({});
        setTabsOptions(Object.keys(SUBTABS_NAMES));
        setSubTabsPermission([]);
      }}
      onOk={() => {
        form.validateFields().then((values) => {
          addPermission({
            variables: {
              input: {
                ...values,
                tabs: subTabsPermission,
              },
            },
          });
          dispatch(togglePermissionModal());
          form.resetFields();
          // reset state too
          setFieldsTabs({});
          setTabsOptions(Object.keys(SUBTABS_NAMES));
        });
      }}
    >
      <Form
        form={form}
        fields={[
          {
            name: ["name"],
            value: permission.name,
          },
        ]}
      >
        <div className={classes.field}>
          <Form.Item
            label="Name"
            name="name"
            rules={[
              {
                required: true,
                message: "please enter a name",
              },
            ]}
          >
            <Input />
          </Form.Item>
        </div>
        <Form.List name="tabs">
          {(fields, { add, remove }) => {
            // fields = [{name: 0, key: 0, isListField: true, fieldKey: 0}];
            return (
              <div>
                {fields.map((field) => {
                  // console.log("field = ", field);
                  return (
                    <Space
                      key={field.key}
                      style={{
                        display: "flex",
                        marginBottom: 8,
                        width: "100%",
                      }}
                      align="start"
                      className="permissions-space"
                    >
                      <div>
                        <Form.Item
                          {...field}
                          label="Tab"
                          name={field.name}
                          fieldKey={field.fieldKey}
                          className="permission_add_label"
                        >
                          <Select
                            style={{ width: 200 }}
                            // defaultValue={getDefaultOptions(permission)}
                            onSelect={(value) =>
                              tabSelectHandler(field.key, value)
                            }
                            placeholder="Select tab..."
                          >
                            {tabsOptions.map((t) => (
                              <Option key={t} value={t}>
                                {t}
                              </Option>
                            ))}
                          </Select>
                          {fieldsTabs[field.key]
                            ? renderSubTabs(fieldsTabs[field.key])
                            : null}
                        </Form.Item>
                      </div>
                      <MinusCircleOutlined
                        onClick={() => {
                          remove(field.name);
                          removeFieldHandler(field.key);
                        }}
                      />
                    </Space>
                  );
                })}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => {
                      add();
                    }}
                    block
                  >
                    <PlusOutlined /> Add Tab
                  </Button>
                </Form.Item>
              </div>
            );
          }}
        </Form.List>
      </Form>
    </Modal>
  );
}
