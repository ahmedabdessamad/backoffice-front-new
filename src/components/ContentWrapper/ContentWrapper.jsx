/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from "react";
import classes from "./contentWrapper.module.scss";

export default (props) => (
  <div
    className={
      props.className != null
        ? `${props.className} ${classes.contentWrapper}`
        : classes.contentWrapper
    }
    {...props}
  >
    {props.children}
  </div>
);
