import React from "react";
import { useQuery } from "@apollo/react-hooks";
import * as PropTypes from "prop-types";
import { Typography } from "antd";
import { FlagOutlined, LoadingOutlined } from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import { useSelector } from "react-redux";
import { FETCH_FORM_QUERY } from "./utile/queries";
import TableForm from "../../components/Form/TableForm";
import classes from "./FormListComponent.module.scss";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";

const { Title } = Typography;

function FormListComponent() {
  const user = useSelector((state) => state.AuthReducer.user);

  const { loading, data } = useQuery(FETCH_FORM_QUERY);

  return (
    <ContentWrapper style={{ height: "100vh" }}>
      <ContentContainer>
        <div className={classes.header}>
          <Title level={3} className={classes.title}>
            Form List
          </Title>
          <FlagOutlined className={classes.icon}> </FlagOutlined>
        </div>

        <br />
        {data ? (
          <TableForm
            forms={data.forms}
            setSelectedKeys={() => {}}
            selectedKeys=""
            confirm={() => {}}
            clearFilters={() => {}}
            user={user}
          >
            {" "}
          </TableForm>
        ) : null}
        {loading ? <LoadingOutlined className={classes.icon2} /> : null}
      </ContentContainer>
    </ContentWrapper>
  );
}

FormListComponent.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default withRouter(FormListComponent);
