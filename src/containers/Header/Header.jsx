import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Layout } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import appActions from "../../redux/app/actions";
import classes from "./header.module.scss";
import UserMenu from "./UserMenu";
import IsError from "../../helpers/IsError";
import { isUserAuthorized } from "../../helpers/authorization/permissionsCheckHelpers";

const { Header: AntHeader } = Layout;

const { toggleCollapse } = appActions;

export default function Header() {
  const { user } = useSelector((state) => state.AuthReducer);
  const dispatch = useDispatch();

  const toggleHandler = () => dispatch(toggleCollapse());

  const { collapsed } = useSelector((state) => state.AppReducer);

  return (
    <AntHeader
      className={`${classes.headerWrapper} ${collapsed && classes.collapsed}`}
    >
      <div className={classes.btn}>
        {React.createElement(
          collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
          {
            onClick: toggleHandler,
          }
        )}
      </div>
      {isUserAuthorized(user, { XEROLOGIN: 4 }) && <IsError />}

      <div className={classes.logoutBtn}>
        <UserMenu />
      </div>
    </AntHeader>
  );
}
