import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, message } from "antd";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { useParams, useHistory } from "react-router-dom";
import sendoutAction from "../../redux/sendout/actions";
import { SENDOUT_DETAILS, SENDOUTS } from "./queries";
import { VALIDATE_SENDOUT, REJECT_SENDOUT } from "./mutations";
import ContentWrapper from "../../components/ContentWrapper/ContentWrapper";
import ContentContainer from "../../components/ContentContainer/ContentContainer";
import CandidateName from "./CandidateName";
import CandidateBrief from "./CandidateBrief";
import PayRate from "./PayRate";
import CandidateReferences from "./CandidateReferences";
import CandidateFiles from "./CandidateFiles";

export default function SendoutDetails() {
  const { sendoutId } = useParams();
  const history = useHistory();
  const {
    sendout,
    displayName,
    displayTJM,
    selectedReferences,
    selectedFile,
  } = useSelector((state) => state.SendoutReducer);
  const dispatch = useDispatch();

  const [validateSendout, { loading: mutationLoading }] = useMutation(
    VALIDATE_SENDOUT,
    {
      onCompleted: () => message.success("Sendout Validated"),
      onError: () => message.error("Oops! Something went wrong"),
      refetchQueries: [
        { query: SENDOUTS, variables: { validationStatus: ["pending"] } },
      ],
    }
  );

  const [rejectSendout, { loading: rejectionLoading }] = useMutation(
    REJECT_SENDOUT,
    {
      variables: { id: sendoutId },
      onCompleted: () => {
        history.push("/home/pending_sendouts");
      },
      onError: () => message.error("Oops! Something went wrong"),
      refetchQueries: [
        { query: SENDOUTS, variables: { validationStatus: ["pending"] } },
      ],
    }
  );

  const { loading, error, data } = useQuery(SENDOUT_DETAILS, {
    variables: { id: sendoutId },
  });

  useEffect(() => {
    if (data && data.sendout) {
      dispatch(sendoutAction.initSendout(data.sendout));
    }
  }, [data, dispatch]);

  if (loading) return <p>Loading...</p>;
  // if (error) return <p>Error</p>;
  if (error) {
    if (error.message === "GraphQL error: NOT_AUTHORIZED") {
      history.push("/home/pending_sendouts");
    }
    return <p>Error</p>;
  }

  const filterFile = (fileID, files) => {
    const file = files.filter((item) => item.id === fileID)[0];

    const { __typename, ...rest } = file;
    return rest;
  };

  const filterReferences = (refIds, refs) =>
    refs
      .filter((ref) => refIds.includes(ref.id))
      .map((ref) => {
        const { __typename, ...rest } = ref;
        return rest;
      });

  const getMutationVariables = () => {
    const { __typename, ...restAddress } = sendout.candidateAddress;

    const input = {
      jobSubmissionID: Number.parseInt(sendout.id, 10),
      jobOrderID: sendout.jobOrderID,
      jobOrderTitle: sendout.jobOrderTitle,
      candidateID: sendout.candidateID,
      candidateFirstName: displayName ? sendout.candidateFirstName : "",
      candidateLastName: displayName ? sendout.candidateLastName : "",
      candidateOccupation: sendout.candidateOccupation,
      candidateAvailableDate: sendout.candidateAvailableDate,
      candidateAddress: restAddress,
      candidateFile: filterFile(selectedFile[0], sendout.candidateFiles),
      comment: sendout.comments,
      payRate: (displayTJM && parseFloat(sendout.payRate)) || null,
      reference: filterReferences(selectedReferences, sendout.reference),
    };

    return { input };
  };

  const validateMutationSubmit = () => !!selectedFile.length;

  return (
    <ContentWrapper>
      <ContentContainer>
        <div
          style={{
            borderBottom: "1px dashed rgb(216, 216, 216)",
            paddingBottom: "25px",
          }}
        >
          <h3>Candidate:</h3>
          <CandidateName />
          <PayRate />
          <CandidateBrief />
        </div>
        <div
          style={{
            borderBottom: "1px dashed rgb(216, 216, 216)",
            paddingTop: "25px",
            paddingBottom: "25px",
          }}
        >
          <h3>Candidate Files:</h3>
          <CandidateFiles />
        </div>
        <div
          style={{
            paddingTop: "25px",
          }}
        >
          <h3 style={{ marginBottom: "16px" }}>Candidate References:</h3>
          <CandidateReferences />
        </div>
        <div
          style={{
            paddingTop: "25px",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Button
            type="primary"
            onClick={() => {
              if (validateMutationSubmit())
                validateSendout({ variables: getMutationVariables() });
              else message.warning("Please select a cv file");
              validateMutationSubmit();
            }}
            loading={mutationLoading}
          >
            <span>
              {sendout.status === "pending"
                ? "Send Candidate"
                : "Update Candidate"}
            </span>
          </Button>
          {sendout.status === "pending" && (
            <Button danger onClick={rejectSendout} loading={rejectionLoading}>
              <span>Reject Candidate</span>
            </Button>
          )}
        </div>
      </ContentContainer>
    </ContentWrapper>
  );
}
