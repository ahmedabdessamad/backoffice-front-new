import React from "react";
import * as PropTypes from "prop-types";
import { Form, Input, Button, Row, Col } from "antd";
import { withRouter } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import UPDATE_SIGNATAIRE from "./mutation";

function AddSignatoryModal(props) {
  const { placement, setModalVisible } = props;

  const [update, { loading }] = useMutation(UPDATE_SIGNATAIRE, {
    refetchQueries: ["TimesheetablePlacements"],
  });

  const onFinish = (values) => {
    const { email } = values;
    update({
      variables: {
        placementID: placement.id,
        secSigEmail: email,
      },
    });
    setModalVisible({
      visible: false,
      type: null,
      placement: null,
    });
  };

  return (
    <Form name="validate_other" onFinish={onFinish}>
      <Form.Item
        name="email"
        label="Email"
        rules={[
          {
            type: "email",
            required: true,
            message: "Please input the email!",
          },
        ]}
      >
        <Input
          type="email"
          name="email"
          placeholder="ajouter l'email du nouveau signataire.."
        />
      </Form.Item>
      <Row>
        <Col span={4}>
          <Form.Item>
            {loading ? (
              <Button type="primary" htmlType="submit">
                loading
              </Button>
            ) : (
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            )}
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
}

AddSignatoryModal.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  placement: PropTypes.shape({
    id: PropTypes.number,
  }).isRequired,
  setModalVisible: PropTypes.func.isRequired,
};
export default withRouter(AddSignatoryModal);
