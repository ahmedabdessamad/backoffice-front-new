/**
 *
 * @param obj
 * @returns {{}}
 */
const localStateToQuery = (obj) => {
  const res = {};
  if (!obj || !Object.keys(obj).length) return {};

  if (obj.limit || obj.limit === 0) res.limit = Number.parseInt(obj.limit, 10);
  else res.limit = 10;
  if (obj.skip || obj.skip === 0) res.skip = Number.parseInt(obj.skip, 10);
  else res.skip = 0;
  if (obj.services.length > 0) res.services = obj.services;

  return res;
};

export default localStateToQuery;
