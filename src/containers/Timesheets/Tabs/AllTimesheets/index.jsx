import React from "react";
import * as PropTypes from "prop-types";
import { Tabs, Card, Col, Row, Button } from "antd";
import { NotificationOutlined } from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import { parse } from "query-string";

import TimesheetTable from "./display-timesheets";

const { TabPane } = Tabs;

function AllTimesheets(props) {
  const { location, history } = props;
  const defaultTab = parse(location.search).tab || "all";

  const setUrl = (key) => {
    history.push({
      search: `?tab=${key}`,
    });
  };

  const cardClick = () => {
    console.log("card button clicked");
  };

  return (
    <div>
      <Card style={{ height: 70, background: "#DFF1FF" }}>
        <Row>
          <Col span={8}>
            <p>
              <NotificationOutlined style={{ color: "red", marginRight: 10 }} />
              message to display
            </p>
          </Col>
          <Col span={8} offset={8}>
            <Button
              type="link"
              style={{ position: "absolute", right: 0 }}
              onClick={cardClick}
            >
              Voir le timesheet
            </Button>
          </Col>
        </Row>
      </Card>
      <br />
      <Tabs defaultActiveKey={defaultTab} onChange={setUrl}>
        <TabPane tab="All" key="all">
          <TimesheetTable status={null} />
        </TabPane>
        <TabPane tab="Open" key="open">
          <TimesheetTable status="open" />
        </TabPane>
        <TabPane tab="Pending" key="pending">
          <TimesheetTable status="pending" />
        </TabPane>
        <TabPane tab="Approved" key="approved">
          <TimesheetTable status="approved" />
        </TabPane>
        <TabPane tab="Rejected" key="rejected">
          <TimesheetTable status="rejected" />
        </TabPane>
      </Tabs>
    </div>
  );
}

AllTimesheets.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
};

export default withRouter(AllTimesheets);
