/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import { Modal, Typography, Form, Input, Card, Button } from "antd";
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import classes from "./ModalForm.module.scss";
import InputComponent from "./InputComponent";
import CheckboxComponent from "./CheckboxComponent";

const { Title } = Typography;

function ModalForm(props) {
  const { visibleModalForm, setVisibleModalForm, form } = props;
  //console.log("formModaaal", form);

  const [choicesInputs, setChoicesInputs] = useState([]);
  const [numbCheckbox, setNumbCheckbox] = useState(0);
  const [inputChoicesValues, setInputChoicesValues] = useState({});

  const inputList = useSelector(
    (state) => state.CheckboxChoicesReducer.inputList
  );
  useEffect(() => {
    // console.log("inputChoicesValues44444444", inputChoicesValues);
    //console.log("inputListsafa", inputList);
    //console.log("inputChoicesValues", inputChoicesValues);
  }, [inputChoicesValues, inputList]);

  const handleChange = (e, i) => {
    setInputChoicesValues((prevState) => ({
      ...prevState,
      [i]: e.target.value,
    }));
  };

  const renderForLoop = () => {
    const arr = [];
    for (let i = 1; i <= numbCheckbox; i += 1) {
      arr.push(
        <Form.Item
          name={`choice${i}`}
          label={`choice ${i}`}
          validateTrigger={["onChange", "onBlur"]}
          rules={[
            {
              required: true,
              whitespace: true,
              message: "Please input the choice.",
            },
          ]}
        >
          <Input name={`choice${i}`} onChange={(e) => handleChange(e, i)} />
        </Form.Item>
      );
    }
    return arr;
  };

  const choices = useSelector((state) => state.CheckboxChoicesReducer.choices);

  useEffect(() => {
    if (numbCheckbox) {
      const tab = renderForLoop();
      setChoicesInputs(tab);
    }
    if (choices) {
     // console.log("choicesssssssssssssssssssss", choices);
    }
  }, [numbCheckbox, choices]);

  const handleOk = (e) => {
    setVisibleModalForm(false);
  };
  const handleCancel = (e) => {
    setVisibleModalForm(false);
  };

  return (
    <div>
      <Modal
        title="Form Details"
        visible={visibleModalForm}
        onOk={handleOk}
        onCancel={handleCancel}
        width="40%"
      >
        <div>
          <Card>
            {form ? (
              <div>
                {form.structure.map((x, i) => {
                  if (x.key === "FormTitle") {
                    return (
                      <Title level={3} className={classes.title}>
                        {x.name}
                      </Title>
                    );
                  }
                })}
                <Form layout="vertical">
                  {form.structure.map((x, i) => {
                    if (x.key === "Checkbox") {
                     // console.log("CheckBox input list =", x);
                      return (
                        <div>
                          <Form.Item
                            name={x.name}
                            label={x.name}
                            rules={[
                              {
                                required: x.required,
                                message: "This field is required!",
                              },
                            ]}
                          >
                            <div className="btn-box">
                              <div style={{ display: "flex" }}>
                                <CheckboxComponent
                                  name={x.name}
                                  choices={x.choices}
                                />

                                {inputList.length - 1 === i}
                              </div>
                            </div>
                          </Form.Item>
                        </div>
                      );
                    }
                    if (
                      x.key === "simpleInput" ||
                      x.key === "inputText" ||
                      x.key === "inputNumber"
                    ) {
                      return (
                        <div>
                          <Form.Item
                            name={x.name}
                            label={x.name}
                            rules={[
                              {
                                required: x.required,
                                message: "This field is required!",
                              },
                            ]}
                          >
                            <div className="btn-box">
                              <div style={{ display: "flex" }}>
                                <InputComponent x={x} i={i} />

                                {inputList.length - 1 === i}
                              </div>
                            </div>
                          </Form.Item>
                        </div>
                      );
                    }
                  })}
                </Form>
              </div>
            ) : null}
            <Button type="primary" disabled>
              Submit
            </Button>
          </Card>
        </div>
      </Modal>
    </div>
  );
}

export default ModalForm;
