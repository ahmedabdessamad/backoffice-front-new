import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { Table, Button, Input, Space, Tooltip, Modal } from "antd";
import { SelectOutlined, EditFilled, SearchOutlined } from "@ant-design/icons";
import { useQuery } from "@apollo/react-hooks";
import moment from "moment";
import TIMESHEETABLE_PLACEMENTS from "./query";
import localStateToQuery from "./localStateToQuery";

import ViewModal from "../modals/PlacementDetails";
import EditModal from "../modals/AddSignatoryModal";

function ReportsComponent() {
  const initialFilters = {
    limit: 10,
    skip: 0,
    ids: null,
    periodStart: null,
    periodEnd: null,
    clientID: null,
    candidateID: null,
  };

  const [filters, setFilters] = useState(initialFilters);
  const [queryFilters, setQueryFilters] = useState(
    localStateToQuery(initialFilters)
  );
  const [modalVisible, setModalVisible] = useState({
    visible: 0,
    type: null,
    placement: null,
  });

  const searchQuery = () => {
    setFilters({ ...filters, searchTrigger: true });
  };
  const clearSearchQuery = () => {
    setFilters({ ...initialFilters, searchTrigger: true });
  };
  const clearSearchQueryItem = (dataIndex) => {
    const newState = { ...filters };
    newState[dataIndex] = null;
    newState.searchTrigger = true;
    setFilters(newState);
  };
  const filterChangeHandler = (e, dataIndex) => {
    const newState = { ...filters };
    newState[dataIndex] = e.target.value;
    setFilters(newState);
  };

  useEffect(() => {
    if (filters.searchTrigger) setQueryFilters(localStateToQuery(filters));
  }, [filters]);

  useEffect(() => {
    const newState = { ...filters };
    newState.searchTrigger = false;
    setFilters(newState);
  }, [queryFilters]);

  const paginate = (page) => {
    const newState = { ...filters };
    newState.skip = (page - 1) * 10;
    newState.searchTrigger = true;
    setFilters(newState);
  };

  const { loading, data, error } = useQuery(TIMESHEETABLE_PLACEMENTS, {
    variables: queryFilters,
  });
  let placements = null;
  let total = null;
  if (data && data.timesheetablePlacements) {
    placements = data.timesheetablePlacements.map((pl) => ({
      id: pl.id,
      jobOrderID: pl.jobOrder && pl.jobOrder.id,
      kam: `${pl.owner && pl.owner.firstName} ${pl.owner && pl.owner.lastName}`,
      client: `${pl.clientContact && pl.clientContact.firstName} ${
        pl.clientContact && pl.clientContact.lastName
      }`,
      clientID: pl.clientContact && pl.clientContact.id,
      project: pl.project,
      start: moment(pl.dateBegin).format("DD/MM/YYYY"),
      end: moment(pl.dateEnd).format("DD/MM/YYYY"),

      candidate: `${pl.candidate && pl.candidate.firstName} ${
        pl.candidate && pl.candidate.lastName
      }`,
      candidateID: pl.candidate && pl.candidate.id,
      signataire1:
        pl.signataire &&
        pl.signataire &&
        pl.signataire.principalSignataire &&
        `${pl.signataire.principalSignataire.firstName} ${pl.signataire.principalSignataire.lastName}`,
      signataire1ID:
        pl.signataire &&
        pl.signataire &&
        pl.signataire.principalSignataire &&
        pl.signataire.principalSignataire.id,
      signataire2:
        pl.signataire &&
        pl.signataire &&
        pl.signataire.secondarySignataires &&
        pl.signataire.secondarySignataires.map((ss) => (
          <li key={ss.id}>{`${ss.firstName || ""} ${ss.lastName || ""}`}</li>
        )),
      signataire2ID:
        pl.signataire &&
        pl.signataire &&
        pl.signataire.secondarySignataires &&
        pl.signataire.secondarySignataires.map((ss) => (
          <li key={ss.id}>{ss.id}</li>
        )),
      actions: pl.id,
    }));
    total =
      data.timesheetablePlacements &&
      data.timesheetablePlacements[0] &&
      data.timesheetablePlacements[0].total;
  }

  const innerSearch = (dataIndex) => ({
    filterDropdown: () => {
      return (
        <div style={{ padding: 8 }}>
          <Input
            placeholder={`Search by ${dataIndex}`}
            value={filters[dataIndex]}
            onChange={(e) => filterChangeHandler(e, dataIndex)}
            onPressEnter={searchQuery}
            style={{ marginBottom: 10, display: "flex" }}
          />
          <Space>
            <Button
              type="primary"
              onClick={searchQuery}
              icon={<SearchOutlined />}
              size="small"
              style={{ width: 80 }}
            >
              Search
            </Button>
            <Button
              onClick={() => clearSearchQueryItem(dataIndex)}
              size="small"
              style={{ width: 80 }}
            >
              Reset
            </Button>
            <Button
              onClick={() => clearSearchQuery()}
              size="small"
              style={{ width: 80 }}
            >
              Reset All
            </Button>
          </Space>
        </div>
      );
    },
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    render: (text) => text,
  });

  const viewClickHandler = (id) => {
    setModalVisible({
      visible: true,
      type: "view",
      placement:
        data &&
        data.timesheetablePlacements &&
        data.timesheetablePlacements.find((x) => x.id === id),
    });
  };

  const editClickHandler = (id) => {
    setModalVisible({
      visible: true,
      type: "edit",
      placement:
        data &&
        data.timesheetablePlacements &&
        data.timesheetablePlacements.find((x) => x.id === id),
    });
  };

  const closeModal = () => {
    setModalVisible({
      visible: false,
      type: null,
      placement: null,
    });
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      fixed: "left",
      ...innerSearch("id"),
    },
    {
      title: "JobOrder ID",
      dataIndex: "jobOrderID",
      key: "jobOrderID",
      fixed: "left",
    },
    {
      title: "KAM",
      dataIndex: "kam",
      key: "kam",
    },
    {
      title: "Client",
      dataIndex: "client",
      key: "client",
    },
    {
      title: "Client ID",
      dataIndex: "clientID",
      key: "clientID",
      ...innerSearch("candidateID"),
    },
    {
      title: "Project",
      dataIndex: "project",
      key: "project",
    },
    {
      title: "Start",
      dataIndex: "start",
      key: "start",
    },
    {
      title: "end",
      dataIndex: "end",
      key: "end",
    },
    {
      title: "Candidate",
      dataIndex: "candidate",
      key: "candidate",
    },
    {
      title: "Candidate ID",
      dataIndex: "candidateID",
      key: "candidateId",
      ...innerSearch("candidateID"),
    },
    {
      title: "Signataire 1",
      dataIndex: "signataire1",
      key: "signataire1",
    },
    {
      title: "Signataire 1 ID",
      dataIndex: "signataire1ID",
      key: "signataire1ID",
    },
    {
      title: "Signataire 2",
      dataIndex: "signataire2",
      key: "signataire2",
    },
    {
      title: "Signataire 2 ID",
      dataIndex: "signataire2ID",
      key: "signataire2ID",
    },
    {
      title: "Actions",
      dataIndex: "actions",
      key: "actions",
      fixed: "right",
      render: (id) => (
        <div>
          <Tooltip placement="topLeft" title="view">
            <SelectOutlined
              style={{ color: "#1890ff", padding: "0 3px" }}
              onClick={() => viewClickHandler(id)}
              type="view"
              twoToneColor="#eb2f96"
            />
          </Tooltip>
          <Tooltip placement="topLeft" title="edit">
            <EditFilled
              style={{ color: "#1890ff", padding: "0 3px" }}
              onClick={() => editClickHandler(id)}
              type="edit"
            />
          </Tooltip>
        </div>
      ),
    },
  ];

  return (
    <div>
      <Table
        columns={columns}
        dataSource={!error && placements}
        scroll
        size="small"
        pagination={{
          onChange: paginate,
          total,
        }}
        loading={loading}
      />
      {modalVisible.visible ? (
        <Modal
          title={`${modalVisible.type}placement${modalVisible.placement.id}`}
          centered
          visible={modalVisible.visible}
          onCancel={closeModal}
          footer={[
            <Button key="cancel" onClick={closeModal}>
              cancel
            </Button>,
          ]}
          width={700}
        >
          {modalVisible.type === "view" ? (
            <ViewModal
              placement={modalVisible.placement}
              setModalVisible={setModalVisible}
            />
          ) : null}
          {modalVisible.type === "edit" ? (
            <EditModal
              placement={modalVisible.placement}
              setModalVisible={setModalVisible}
            />
          ) : null}
        </Modal>
      ) : null}
    </div>
  );
}

export default withRouter(ReportsComponent);
