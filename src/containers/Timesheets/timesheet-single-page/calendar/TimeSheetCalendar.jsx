import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { MonthlyTimesheet } from "cf-neo-ui";
import { useQuery } from "@apollo/react-hooks";
import classes from "./TimeSheetCalendar.module.scss";
import runtimeVars from "../../../../configs/runTimeVars";
import { HOLIDAYS } from "../../queries";

function TimeSheetCalendar(props) {
  const { month, year, selectedDays } = props;
  let country = "EN";
  if (runtimeVars.APP_LANG === "fr") country = "FR";

  const { data } = useQuery(HOLIDAYS, {
    variables: {
      month,
      year,
      country,
    },
    fetchPolicy: "network-only",
  });

  return (
    <div className={classes.calenderContainer}>
      <MonthlyTimesheet
        month={month}
        year={year}
        defaultSelectedDays={selectedDays}
        iconTooltip="Cliquez içi préciser le nombre d’heures travaillés"
        holidays={data && data.holidays.length ? data.holidays[0].holidays : []}
        editable={false}
        local="gb"
        writingFormat="long"
      />
    </div>
  );
}

TimeSheetCalendar.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
  selectedDays: PropTypes.shape.isRequired,
  i18n: PropTypes.shape({
    _: PropTypes.func,
  }).isRequired,
};

export default withRouter(TimeSheetCalendar);
