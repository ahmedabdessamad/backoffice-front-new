import { gql } from "graphql.macro";

export const addUserMutation = gql`
  mutation addUser($input: NewUserInput!) {
    addUser(input: $input) {
      userID
      username
      email
      permissions {
        name
        tabs {
          name
          permission
          parent
        }
      }
      bhID
    }
  }
`;
export const editUserMutation = gql`
  mutation editUser($userID: ID!, $input: UserInput!) {
    editUser(userID: $userID, input: $input) {
      userID
      username
      email
      permissions {
        name
        tabs {
          name
          permission
          parent
        }
      }
      bhID
    }
  }
`;
export const deleteUserMutation = gql`
  mutation deleteUser($userID: ID!) {
    deleteUser(userID: $userID)
  }
`;
