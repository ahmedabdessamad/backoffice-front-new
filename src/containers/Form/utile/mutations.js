import { gql } from "graphql.macro";

const UPDATE_FORM_MUTATION = gql`
  mutation editForm($formID: ID!, $subject: String!, $structure: [formItem]) {
    editForm(formID: $formID, subject: $subject, structure: $structure) {
      formID
      subject
      structure {
        name
        key
        type
        placeholder
        defaultValue
        required
        maxLength
      }
      creator {
        email
      }
    }
  }
`;

const CREATE_FORM_MUTATION = gql`
  mutation addForm($subject: String!, $structure: [formItem]) {
    addForm(subject: $subject, structure: $structure) {
      formID
      subject
      structure {
        name
        key
        type
        placeholder
        defaultValue
        required
        maxLength
      }
      creator {
        email
      }
    }
  }
`;

const SEND_FORM_MUTATION = gql`
  mutation sendForm($formID: ID!, $email: String!) {
    sendForm(formID: $formID, email: $email) {
      formID
      subject
      structure {
        name
        key
        type
        placeholder
        defaultValue
        required
        maxLength
      }
      creator {
        email
      }
    }
  }
`;
export { UPDATE_FORM_MUTATION, CREATE_FORM_MUTATION, SEND_FORM_MUTATION };
