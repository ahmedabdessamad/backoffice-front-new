import { Modal, Form, Input, Card, Select, Radio, InputNumber } from "antd";
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import checkboxChoicesActions from "../../redux/checkboxChoices/actions";

const { Option, OptGroup } = Select;
const { addInputList, setInputList } = checkboxChoicesActions;

function ModalInput(props) {
  const { visibleModalInput, setVisibleModalInput, menuKey } = props;

  const [name, setName] = useState("");
  const [type, setType] = useState("");

  const [choicesInputs, setChoicesInputs] = useState([]);
  const [numbCheckbox, setNumbCheckbox] = useState(0);
  const [placeholder, setPlaceholder] = useState("");
  const [defaultValue, setDefaultValue] = useState("");
  const [maxLength, setMaxLength] = useState();
  const [radioValue, setRadioValue] = useState(false);
  const [inputChoicesValues, setInputChoicesValues] = useState({});

  const inputList = useSelector(
    (state) => state.CheckboxChoicesReducer.inputList
  );
  const dispatch = useDispatch();
  useEffect(() => {
    // console.log("inputChoicesValues44444444", inputChoicesValues);
   // console.log("inputListsafa", inputList);
    //console.log("inputChoicesValues", inputChoicesValues);
  }, [inputChoicesValues, inputList]);

  const handleChange = (e, i) => {
    setInputChoicesValues((prevState) => ({
      ...prevState,
      [i]: e.target.value,
    }));
  };

  const renderForLoop = () => {
    const arr = [];
    for (let i = 1; i <= numbCheckbox; i += 1) {
      arr.push(
        <Form.Item
          name={`choice${i}`}
          label={`choice ${i}`}
          validateTrigger={["onChange", "onBlur"]}
          rules={[
            {
              required: true,
              whitespace: true,
              message: "Please input the choice.",
            },
          ]}
        >
          <Input name={`choice${i}`} onChange={(e) => handleChange(e, i)} />
        </Form.Item>
      );
    }
    return arr;
  };

  const choices = useSelector((state) => state.CheckboxChoicesReducer.choices);

  useEffect(() => {
    if (numbCheckbox) {
      const tab = renderForLoop();
      setChoicesInputs(tab);
    }
    if (choices) {
     // console.log("choicesssssssssssssssssssss", choices);
    }
  }, [numbCheckbox, choices]);

  const exist = (inputList) => {
    let i = 0;
    for (i = 0; i < inputList.length; i += 1) {
      if (inputList[i].key === "FormTitle") {
        return i;
      }
    }
    return -1;
  };

  const handleOk = (e) => {
   // console.log("in okkkkkkkkk", inputChoicesValues);
    if (menuKey === "FormTitle") {
      const index = exist(inputList);
      if (index !== -1) {
        const list = [...inputList];
        list[index].name = name;
        dispatch(setInputList(list));
      } else {
        dispatch(
          addInputList({
            name,
            key: menuKey,
          })
        );
      }

      setVisibleModalInput(false);
    } else if (
      menuKey === "simpleInput" ||
      menuKey === "inputText" ||
      menuKey === "inputNumber"
    ) {
      dispatch(
        addInputList({
          name,
          type,
          placeholder,
          key: menuKey,
          defaultValue,
          maxLength,
          required: radioValue,
        })
      );

      setVisibleModalInput(false);
    } else if (menuKey === "Checkbox") {
      // const tabChoices = Object.values(inputChoicesValues);
      // setChoices(tabChoices);
      // dispatch(setChoices({name, tabChoices}));
      setInputChoicesValues([]);
      /* setInputList([
        ...inputList,
        {
          name,
          type,
          key: menuKey,
          required: radioValue,
          choices,
        },
      ]); */

      /* setInputList((prevState) => [
        ...prevState,
        {
          name,
          type,
          key: menuKey,
          required: radioValue,
          choices,
        },
      ]); */

      dispatch(
        addInputList({
          name,
          type,
          key: menuKey,
          required: radioValue,
          choices: Object.values(inputChoicesValues),
        })
      );

      setVisibleModalInput(false);
    }
  };
  const handleCancel = (e) => {
    setVisibleModalInput(false);
  };
  const changenameHandler = (event) => {
    setName(event.target.value);
  };
  const changetypeHandler = (value) => {
    setType(value);
  };
  const changeplaceholderHandler = (event) => {
    setPlaceholder(event.target.value);
  };
  const changedefaultValueHandler = (event) => {
    setDefaultValue(event.target.value);
  };
  const changemaxLengthHandler = (event) => {
    setMaxLength(event.target.value);
  };

  const changeNumbCheckboxHandler = (value) => setNumbCheckbox(value);

  const onChangeRadio = (e) => {
    //console.log("radio checked", e.target.value);
    if (e.target.value === "Required") {
      setRadioValue(true);
    } else {
      setRadioValue(false);
    }
  };

  return (
    <div>
      <Modal
        title="Configuration"
        visible={visibleModalInput}
        onOk={handleOk}
        onCancel={handleCancel}
        width="40%"
      >
        <div>
          <Card>
            <Form name="nest-messages">
              <Form.Item
                name="name"
                label="Name"
                rules={[
                  {
                    required: true,
                    message: "Please input the name!",
                  },
                ]}
              >
                <Input
                  type="name"
                  onChange={changenameHandler}
                  name="name"
                  value={name}
                />
              </Form.Item>
              {menuKey === "simpleInput" ? (
                <Form.Item
                  name="type"
                  label="Type"
                  rules={[
                    {
                      required: true,
                      message: "Please input the type!",
                    },
                    {
                      max: 30,
                      message: "Length must be less then 30 characters!",
                    },
                  ]}
                >
                  <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="Select a type"
                    optionFilterProp="children"
                    onChange={changetypeHandler}
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    <Option value="text">Text</Option>
                    <Option value="email">Email</Option>
                    <Option value="password">Password</Option>
                  </Select>
                </Form.Item>
              ) : null}
              {menuKey === "inputNumber" ? (
                <Form.Item
                  name="type"
                  label="Type"
                  rules={[
                    {
                      required: true,
                      message: "Please input the type!",
                    },
                    {
                      max: 30,
                      message: "Length must be less then 30 characters!",
                    },
                  ]}
                >
                  <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="Select a type"
                    optionFilterProp="children"
                    onChange={changetypeHandler}
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    <Option value="Number">Number</Option>
                    <Option value="Decimal">Decimal</Option>
                    <Option value="Percentage">Percentage</Option>
                    <OptGroup label="Currency">
                      <Option value="Dollar">Dollar</Option>
                      <Option value="Euro">Euro</Option>
                      <Option value="Pound">Pound</Option>
                    </OptGroup>
                  </Select>
                </Form.Item>
              ) : null}
              {!(menuKey === "Checkbox") && !(menuKey === "FormTitle") ? (
                <Form.Item
                  name="placeholder"
                  label="placeholder"
                  rules={[
                    {
                      max: 30,
                      message: "Length must be less then 30 characters!",
                    },
                  ]}
                >
                  <Input
                    type="placeholder"
                    onChange={changeplaceholderHandler}
                    name="placeholder"
                    value={placeholder}
                  />
                </Form.Item>
              ) : null}
              {!(menuKey === "FormTitle") ? (
                <Form.Item name="required" label="required">
                  <Radio.Group
                    defaultValue="Optional"
                    onChange={onChangeRadio}
                    value={radioValue}
                  >
                    <Radio value="Required">Required</Radio>
                    <Radio value="Optional">Optional</Radio>
                  </Radio.Group>
                </Form.Item>
              ) : null}
              {!(type === "password") &&
              !(menuKey === "Checkbox") &&
              !(menuKey === "FormTitle") ? (
                <div>
                  {!(type === "email") ? (
                    <Form.Item name="defaultValue" label="Default value">
                      <Input
                        type="defaultValue"
                        onChange={changedefaultValueHandler}
                        name="defaultValue"
                        value={defaultValue}
                      />
                    </Form.Item>
                  ) : (
                    <Form.Item name="defaultValue" label="Default value">
                      <Input
                        type="defaultValue"
                        onChange={changedefaultValueHandler}
                        name="defaultValue"
                        value={defaultValue}
                      />
                    </Form.Item>
                  )}
                </div>
              ) : null}
              {!(menuKey === "inputNumber") &&
              !(menuKey === "Checkbox") &&
              !(menuKey === "FormTitle") ? (
                <Form.Item name="maxLength" label="maxLength">
                  <Input
                    type="maxLength"
                    onChange={changemaxLengthHandler}
                    name="maxLength"
                    value={maxLength}
                    defaultValue="30"
                  />
                </Form.Item>
              ) : null}
              {menuKey === "Checkbox" ? (
                <Form.Item name="numbCheckbox" label="numbre of choices">
                  <InputNumber
                    onChange={changeNumbCheckboxHandler}
                    name="numbCheckbox"
                    value={numbCheckbox}
                    min="1"
                    max="10"
                  />
                </Form.Item>
              ) : null}
              {numbCheckbox && menuKey === "Checkbox" ? choicesInputs : null}
            </Form>
          </Card>
        </div>
      </Modal>
    </div>
  );
}

export default ModalInput;
