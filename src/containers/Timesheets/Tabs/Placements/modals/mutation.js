import { gql } from "graphql.macro";

const UPDATE_SIGNATAIRE = gql`
  mutation UpdatePLSignataire($placementID: ID!, $secSigEmail: String!) {
    updatePLSignataire(placementID: $placementID, secSigEmail: $secSigEmail)
  }
`;

export default UPDATE_SIGNATAIRE;
