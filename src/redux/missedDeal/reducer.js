import actions from "./actions";

const initState = {
  missedDeal: null,
};

export default function appReducer(state = initState, action) {
  if (action.type === actions.SET_MISSEDDEAL) {
    return {
      ...state,
      missedDeal: action.payload,
    };
  }
  return state;
}
